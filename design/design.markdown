# Unifying Principles

- Free-form content nodes: no content types, only nodes; a node can serve as a
  blueprint for other nodes.
- Field values are just JSON objects or scalars; plugins and templates can
  provide meaning. No other intrinsic meaning is implied.
- Field types are dynamic; scalar fields and lists are stored as-is, while
  objects can optionally have a `"_type"` property that the backend picks up to
  present the field differently in the editor UI.
- Besides the dynamic fields, records have a few properties that are hard
  wired: creation and modification timestamps, revision information (for
  versioning-aware storage backends), authorship, and whether this record is a
  blueprint or not.

# Core Values

- Correctness. Never lie about your data.
- Security is a primary design concern, not an afterthought. "Develop a feature
  within a secure framework", not "Develop a feature, then figure out how to
  make it secure".
- Simplicity, a.k.a. Do One Thing Right. More advanced stuff can be left to
  plugins.
- Hard core, soft peripherals: The core should be an immutable program,
  interpreting a user-defined site setup.
- Programmer friendly core: write code in a powerful, scalable, pleasant,
  honest, state-of-the-art language (Haskell), write clean code, document well.
- Web Developer friendly tooling: use or emulate existing languages and tools
  as much as possible: JavaScript for plugin scripting, SASS for CSS templates,
  something close to Jinja/Twig for templates, JSON and YAML for data
  representation and configuration files, SQL-inspired query language.
- Editor-friendly backend: provide a graphical ("click and drool") interface
  for all editing tasks, usability > technicalities > design.
- Compatibility with many platforms, within reason:
  - Modular backends, supporting diverse storage mechanisms (various SQL
    databases, file-based, in-memory, NoSQL, HTTP/REST, ...)
  - Support several deployment modes: CGI, SCGI, FastCGI (?), built-in web
    server.
  - Release binaries for all popular server architectures; at least 32-bit and
    64-bit x86 Linux; possibly also FreeBSD, OpenBSD, Windows?; also, ARM.

# Components

## Core

Takes care of:

- Front-facing web server
  - Public website
  - Management/editing UI ("backend")
    - Editing nodes
      - Plain HTML exposes node as JSON in a textarea [done]
      - JavaScript to turn the JSON textarea into a structural editor
- Loading configuration (YAML)
- Instantiating storage providers
- Loading & interfacing plugins
- Firing hooks

## Configuration

- Configure server
- Define routes
  - A route is defined by:
    - A *pattern*, e.g. `/nodes/{node_id}`
    - A dictionary of *queries* into the storage
      - Entries in this dictionary define what to fetch from storage and under
        which key to expose it to the template
    - A template
- Select and configure storage module
- Select and configure plugins

## Plugins

Can provide functionality for:

- Display (views)
- Extending the backend (extra field types)

Plugins can be implemented as either:

- A **native plugin**, written in Haskell and compiled into Core
- **Hosted JavaScript**, running in a sandboxed JS interpreter over the
  suitable monad.
- **External binary**, running as a subprocess and communicating using pipes.
  Obviously this means that at least one subprocess per plugin and request is
  going to be run.
- **JSON/HTTP service**, running in a separate web server. Core sends HTTP
  requests for every hook. This means that a lot of internal HTTP traffic is
  going to be generated, but it also makes for a more scalable application, as
  each plugin can run on a dedicated host if needed.

All but the native plugins themselves are going to be run through native
"wrapper" plugins.

# TO DO

- Tests (HUnit? QuickCheck?)
- Refactor Handler into a `ReaderT HandlerContext IO`, where `data
  HandlerContext :: HandlerContext app rq respond`
- FastCGI server
- More storage plugins
  - SQL backends
    - SQLite
    - PostgreSQL
  - File-based (one file per record, plus a bunch of indexes)
- Frontend routing
  - Facilitate "canonical" route for a node.
    - config:
      `canonicals: [ { nodes: <QUERY_WHERE>, pattern: <ROUTE-TEMPLATE> }, ... ]`
    - recordCanonical :: Record -> Maybe Canonical
    - recordCanonicalURL :: Record -> Maybe Text -- not all records need a
      canonical!
    - In backend, use canonical route to display a link to the node in the
      frontend
    - Use canonical route and actual routes to render a live preview of the
      node being edited
- Add login/logout functionality and a user management UI
  - GET `/admin/login`
  - POST `/admin/logout`
  - POST `/admin/logout`
  - GET `/admin/users`
  - GET/POST `/admin/users/new`
  - GET/POST `/admin/users/{user_id}`
  - DELETE `/admin/users/{user_id}`
- Frontend templates
  - A "template" is simply an output converter: JSON goes in, response comes
    out. `data FrontendResponse = FrontendResponse ContentType LBS.ByteString
- Plugins
  - Hosted JS plugin wrapper
    - check out ecma262 interpreter, or finish run-js
  - Figure out how to do two-way communication (plugin calling back into Core)
    with external binaries and JSON-HTTP
  - External-binary plugin wrapper
  - JSON-HTTP plugin wrapper
