{-#LANGUAGE TypeSynonymInstances #-}
{-#LANGUAGE FlexibleInstances #-}
{-#LANGUAGE FlexibleContexts #-}
module System.LoadFile
where

import Data.Text as Text
import qualified Data.Text.Lazy as LText
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import System.IO
import System.IO.Error
import Text.StringConvert
import Data.Monoid

class LoadFile a where
    loadFile :: FilePath -> IO a

instance LoadFile String where
    loadFile fn = do
        openFile fn ReadMode >>= hGetContents

instance LoadFile LBS.ByteString where
    loadFile fn = openFile fn ReadMode >>= LBS.hGetContents

instance LoadFile ByteString where
    loadFile fn = openFile fn ReadMode >>= BS.hGetContents

loadFileMay :: (LoadFile a) => FilePath -> IO (Maybe a)
loadFileMay fn = do
    tryIOError (loadFile fn) >>= \e ->
         case e of
            Right contents -> do
                return . Just $ contents
            Left err -> do
                hPutStrLn stderr $ show err
                return Nothing


