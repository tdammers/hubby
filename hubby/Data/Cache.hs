{-#LANGUAGE ScopedTypeVariables #-}

-- | A generic interface for caches.
module Data.Cache
( Cache (..)
, newMemCache
, MemCacheOptions (..)
, defMemCacheOptions
)
where

import Control.Applicative
import Data.Monoid
import Data.Hashable
import Data.Maybe (isJust)
import qualified Data.HashTable.IO as HT

-- | Cache k v represents a cache with keys k and values v.
data Cache k v =
    Cache
        { set :: k -> v -> IO () -- ^ add a cached value
        , has :: k -> IO Bool -- ^ ask whether a value exists for the given key
        , get :: k -> IO (Maybe v) -- ^ get the cached value under a key, if any
        , delete :: k -> IO () -- ^ unset and delete the value under the key (ignored if no value cached)
        }

-- | A dummy cache that never actually caches anything. 'set' and 'delete' are
-- no-ops, 'has' always returns 'False', 'get' always returns 'Nothing'.
nullCache :: Cache k v
nullCache =
    Cache
        { set = const . const . return $ () -- never do anything
        , has = const . return $ False -- no caching, no value is ever present
        , get = const . return $ Nothing -- never return anything
        , delete = const . return $ ()
        }

-- | Chain two caches such that insertions populate both caches, and queries
-- use the second cache as a fall-through for failures from the first. When the
-- first cache lookup fails but the second one succeeds, the first cache gets
-- populated from the lookup result.
chain :: Cache k v -> Cache k v -> Cache k v
chain (Cache set1 has1 get1 delete1) (Cache set2 has2 get2 delete2) =
    Cache
        { set = \k v -> set1 k v >> set2 k v
        , delete = \k -> delete1 k >> delete2 k
        , has = \k -> (||) <$> has1 k <*> has2 k
        , get = \k -> do
            a <- get1 k
            case a of
                Just x -> return a
                Nothing -> do
                    vm <- get2 k
                    case vm of
                        Just v -> set1 k v
                    return vm
        }

instance Monoid (Cache k v) where
    mappend = chain
    mempty = nullCache

data MemCacheOptions =
        MemCacheOptions
            { memcacheSizeLimit :: Maybe Int
            }

defMemCacheOptions =
    MemCacheOptions
        { memcacheSizeLimit = Nothing
        }

-- | An STM-based RAM cache.
newMemCache :: (Eq k, Hashable k) => IO (Cache k v)
newMemCache = do
    ht <- mkHashTable
    return $ Cache (mcSet ht) (mcHas ht) (mcGet ht) (mcDelete ht)
    where
        mkHashTable :: IO (HT.BasicHashTable k v)
        mkHashTable = HT.new
        mcSet = HT.insert
        mcHas ht k = isJust <$> HT.lookup ht k
        mcGet = HT.lookup
        mcDelete = HT.delete
