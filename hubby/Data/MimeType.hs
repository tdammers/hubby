{-#LANGUAGE OverloadedStrings #-}
module Data.MimeType
where

import Data.ByteString (ByteString)
import Data.Char (toLower)
import Data.Maybe (fromMaybe)
import System.FilePath

detectMimeType :: FilePath -> Maybe ByteString
detectMimeType fp =
    case extension of
        "css" -> Just "text/css"
        "js" -> Just "text/javascript"
        "html" -> Just "text/html"
        "jpg" -> Just "image/jpeg"
        "png" -> Just "image/png"
        "gif" -> Just "image/gif"
        "txt" -> Just "text/plain"
        _ -> Nothing
    where
        extension :: String
        extension = map toLower . drop 1 . takeExtension $ fp

detectMimeTypeDef :: ByteString -> FilePath -> ByteString
detectMimeTypeDef def = fromMaybe def . detectMimeType
