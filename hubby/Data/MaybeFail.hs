module Data.MaybeFail where

-- | Translate from 'Maybe' to any other monad; 'Nothing' values become
-- failures, 'Just' values are 'return'ed into the target monad.
maybeFail :: Monad m => String -> Maybe a -> m a
maybeFail msg Nothing = fail msg
maybeFail _ (Just x) = return x
