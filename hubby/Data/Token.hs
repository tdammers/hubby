module Data.Token
( mkToken
, mkTokenIO
)
where

import System.Random

randomPick :: (RandomGen g) => g -> [a] -> (a, g)
randomPick g items =
    (items !! i, g')
    where
        i :: Int
        (i, g') = randomR (0, length items - 1) g

-- | 'mkToken gen alphabet length' generates a random token containing 'length'
-- items from the 'alphabet', using the RNG 'gen' and returning the token and
-- the updated RNG.
mkToken :: RandomGen g => g -> [a] -> Int -> ([a], g)
mkToken g _ 0 = ([], g)
mkToken g alphabet n =
    (c:cs, g'')
    where
        (c, g') = randomPick g alphabet
        (cs, g'') = mkToken g' alphabet (n - 1)

-- | Variant of @mkToken@ that runs in @IO@ and uses the global system RNG.
mkTokenIO :: [a] -> Int -> IO [a]
mkTokenIO alphabet n = do
    g <- newStdGen
    let (x, _) = mkToken g alphabet n
    return x
