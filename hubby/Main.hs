{-#LANGUAGE OverloadedStrings #-}
module Main
where

import Control.Exception
import Control.Applicative

import Data.Default
import System.Environment (getArgs, getEnv)
import System.IO (openFile, IOMode (ReadMode), hPutStrLn, stderr )
import System.IO.Error (catchIOError)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import Data.Aeson.ParseUtils
import qualified Data.Yaml as YAML
import Data.Cache (Cache, newMemCache)
import Data.MaybeFail
import Safe
import Data.Maybe

import Hubby.Application
import Hubby.AppConfig
import Hubby.AppContext
import Hubby.Servers
import Hubby.Storage
import Hubby.FileStore
import Hubby.SessionProvider
import Hubby.Authentication
import Hubby.Tests
import Hubby.CLI
import Hubby.Logging
import Hubby.MainOptions

main :: IO ()
main = do
    argv <- getArgs
    let optionsE = parseMainOptions argv
    options <- case optionsE of
        Left err -> do
            hPutStrLn stderr "Invalid options."
            return $ defMainOptions { moCommand = PrintHelp }
        Right o -> return o
    let loadConfigs = loadConfig (moConfigFiles options)
    case moCommand options of
        MakeConfig -> makeConfig
        RunTests AllTests -> runAllTests
        RunUserCommand subcmd -> loadConfigs >>= runUsersCommand subcmd
        RunWarp warpOptions -> loadConfigs >>= serveWarp warpOptions
        RunCGI cgiOptions -> loadConfigs >>= serveCGI
        RunSCGI scgiOptions -> loadConfigs >>= serveSCGI
        PrintHelp -> printHelp

loadConfig :: [FilePath] -> IO AppConfig
loadConfig xs = go xs def
    where
        go :: [FilePath] -> AppConfig -> IO AppConfig
        go [] defConf = return defConf
        go (x:xs) defConf = go xs =<< goLoad x defConf
        goLoad filename defConf =
            inner `catchIOError` handle
            where
                inner = do
                    fc <- BS.hGetContents =<< openFile filename ReadMode
                    let configMay = decodeWithDefaults defConf fc
                    case configMay of
                        Nothing -> fail ("Invalid YAML in " ++ filename)
                        Just config -> return config
                handle err = do
                    print err
                    return defConf

runUsersCommand :: UserCommand -> AppConfig -> IO ()
runUsersCommand (UserCreate Nothing _) _ =
    fail "No username provided."
runUsersCommand (UserCreate (Just username) passwordMay) config = do
    let password = fromMaybe "" passwordMay
    withAppContext config $ createUser username password

makeConfig :: IO ()
makeConfig = do
    let config :: AppConfig
        config = def
    BS.putStr $ YAML.encode config

lookupEnv :: String -> IO (Maybe String)
lookupEnv key = (Just <$> getEnv key) `catchIOError` (const $ return Nothing)

serveWarp :: WarpOptions -> AppConfig -> IO ()
serveWarp (WarpOptions port) config =
    withAppContext config $ runWarp port . application

serveCGI :: AppConfig -> IO ()
serveCGI config = withAppContext config $ runCGI . application

serveSCGI :: AppConfig -> IO ()
serveSCGI config = withAppContext config $ runSCGI . application

--         _ -> do
--             -- Attempt to detect server gateway interface
--             gateway <- lookupEnv "GATEWAY_INTERFACE"
--             case gateway of
--                 -- CGI/1.* - use CGI
--                 Just ('C':'G':'I':'/':'1':'.':_) -> return runCGI
--                 -- Not set, or something we don't recognize - assume SCGI
--                 _ -> return runSCGI
-- 
--     withAppContext (run . application)

withAppContext :: AppConfig -> (AppContext -> IO a) -> IO a
withAppContext config action = do
    let storageConf = storage config
    storageProvider <- createStorage
                            (storageProviderName storageConf)
                            (storageProviderConfig storageConf)

    let sessionsConf = sessions config
    sessionProviderE <- createSessionProvider
                            (sessionsProviderName sessionsConf)
                            (sessionsProviderConfig sessionsConf)

    let authConf = auth config
    sessionProvider <- either fail return sessionProviderE
    authProviderE <- createAuthProvider
                        (authProviderName authConf)
                        (authProviderConfig authConf)
    authProvider <- either fail return authProviderE

    let fileConf = fileStore config
    fileProvider <- createFileStore
                        (fileStoreName fileConf)
                        (fileStoreConfig fileConf)

    thumbCacheProvider <- newMemCache

    let cleanup = do
            dispose storageProvider
            authDispose authProvider
            cleanupSessions sessionProvider

    mapM (writeLog . show) =<< listFiles fileProvider

    flip finally cleanup $ do
        let context :: AppContext
            context = AppContext
                { appConfig = config
                , appStorage = storageProvider
                , appSessions = sessionProvider
                , appAuth = authProvider
                , appFileStore = fileProvider
                , appThumbCache = thumbCacheProvider
                }
        action context
