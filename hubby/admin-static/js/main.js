requirejs.config({
    baseUrl: '/admin/static/js/lib',
    paths: {
        app: '../app',
        underscore: 'underscore-min',
        zepto: 'zepto.min'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        zepto: {
            exports: 'Zepto'
        }
    }
});
requirejs(['app/app'], function(app){
    app.run();
});
