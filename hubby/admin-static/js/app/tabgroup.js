define(
['zepto', 'underscore'],
function($, _) {
    var getPanel = function(selector) {
        var panelID = $(selector).data('tab-panel');
        return $('#' + panelID);
    };

    var tabify = function() {
        $('.tab-selector').on('click', function(e) {
            var panel = getPanel(this);
            var siblings = $(this).siblings();
            console.log(this);
            console.log(siblings);
            siblings.each(function(){
                var sibPanel = getPanel(this);
                sibPanel.removeClass('tab-selected');
                $(this).removeClass('tab-selected');
            });
            $(this).addClass('tab-selected');
            panel.addClass('tab-selected');
            e.preventDefault();
        });
    };

    var TabGroup = {
        attach: tabify
    };

    return TabGroup;
});
