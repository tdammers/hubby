define(
[ 'zepto', 'underscore' ],
function($, _) {
    var ConfirmSubmits = {
        attach: function() {
            $('.confirm-submit').on('submit', function(e){
                if (!confirm('Are you sure?')) {
                    e.preventDefault();
                }
            });
        }
    };

    return ConfirmSubmits;
});
