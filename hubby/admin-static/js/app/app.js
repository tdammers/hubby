define(
['app/record-editor', 'app/tabgroup', 'app/confirm-submits', 'app/query-editor'],
function(RecordEditor, TabGroup, ConfirmSubmits, QueryEditor){
    return {
        'run': function() {
            RecordEditor.attach();
            TabGroup.attach();
            ConfirmSubmits.attach();
            QueryEditor.attach();
        }
    };
});
