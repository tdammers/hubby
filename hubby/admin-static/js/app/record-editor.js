define(
[ 'zepto', 'underscore', 'app/widgets/factory' ],
function($, _, Widgets) {
    RecordEditor = {
        attach: function() {
            $(document).ready(function(){
                $('.record-editor').each(function(){
                    var editor = Widgets.create('field-list');
                    var jsonElem = $('.record-json', $(this));
                    var jsonData;
                    try {
                        jsonData = JSON.parse(jsonElem.val());
                    }
                    catch (e) {
                        console.log(e, jsonElem.val());
                        jsonData = [];
                    }
                    editor.attachTo($(this));
                    editor.dataBind(jsonData);
                    editor.on('change', function(src, data){
                        jsonElem.val(JSON.stringify(data));
                    });
                    var container = this;
                });
            });
        }
    };

    return RecordEditor;
});
