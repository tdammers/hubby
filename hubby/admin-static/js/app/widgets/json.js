/**
 * The "I give up" option: Just give the user a textarea to edit raw JSON
 * values.
 */
define(
['zepto', 'underscore', 'inherit', 'app/widgets/base'],
function($, _, inherit, BaseWidget) {
    var JSONWidget = inherit(BaseWidget);

    JSONWidget.prototype.getName = function() { return 'JSON widget'; };

    JSONWidget.prototype.createElem = function() {
        this.textBox = $('<textarea/>').addClass('widget-json');
        var me = this;
        this.textBox.on('keyup', function() {
            me.raise('change', me.getData());
        });
        return this.textBox;
    };

    JSONWidget.prototype.dataBind = function(value) {
        this.textBox.val(JSON.stringify(value));
    };

    JSONWidget.prototype.getData = function() {
        try {
            // Little trick here to support parsing scalars (which the JSON
            // standard does not specify as valid JSON documents): wrap the
            // source in a single-element list construct, then get the first
            // (and thus, only) element from the parsed list.
            return JSON.parse('[' + this.textBox.val() + ']')[0];
        }
        catch (e) {
            // console.log("Invalid JSON in JSON widget: ", this.textBox.val());
            return null;
        }
    };

    return JSONWidget;
});
