/**
 * A text field that runs its input through pandoc for rendering.
 */
define(
['zepto', 'underscore', 'inherit', 'app/widgets/base'],
function($, _, inherit, BaseWidget) {
    var cheatsheetURLs = {
        'markdown': 'http://warpedvisions.org/projects/markdown-cheat-sheet/',
        'textile': 'http://warpedvisions.org/projects/textile-cheat-sheet/',
        'rst': 'https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst'
    };
    var PandocWidget = inherit(BaseWidget);

    PandocWidget.prototype.getName = function() { return 'Pandoc widget'; };

    PandocWidget.prototype.createElem = function() {
        var inputFormat = this.options.format || 'markdown';
        var cheatsheetURL = cheatsheetURLs[inputFormat];
        var container = $('<div/>').addClass('widget-pandoc');
        this.textBox = $('<textarea/>').addClass('widget-pandoc-src').appendTo(container);
        this.previewBox = $('<div/>').addClass('widget-pandoc-dst').appendTo(container);
        if (cheatsheetURL) {
            this.cheatsheetLink = $('<a/>')
                                        .addClass('widget-pandoc-cheatsheet-link')
                                        .attr('href', cheatsheetURL)
                                        .attr('target', 'syntax_help')
                                        .text('syntax help...')
                                        .appendTo(container);
        }
        else {
            this.cheatsheetLink = $('<span/>')
                                        .addClass('widget-pandoc-cheatsheet-link')
                                        .text('no syntax help available')
                                        .appendTo(container);
        }
        var me = this;
        this.pendingRequest = null;
        this.sending = false;
        this.textBox.on('keyup', function() {
            me.sendRequest(me.getData());
            me.raise('change', me.getData());
        });
        this.textBox.on('scroll', this.adjustScrollPosition.bind(this));
        return container;
    };

    PandocWidget.prototype.adjustScrollPosition = function() {
        var relScroll = this.textBox.scrollTop() / this.textBox[0].scrollHeight;
        var dstScroll = Math.round(this.previewBox[0].scrollHeight * relScroll);
        console.log(dstScroll);
        this.previewBox.scrollTop(dstScroll);
    };

    PandocWidget.prototype.getInputFormat = function() {
        var format = this.options.format || 'markdown';
        if (format === 'markdown') {
            switch (this.options.subformat) {
                case 'github':
                case 'strict':
                case 'mmd':
                case 'phpextra':
                    return 'markdown_' + this.options.subformat;
                default:
                    return 'markdown';
            }
        }
        if (_.contains(['docbook', 'html', 'latex', 'mediawiki', 'org', 'rst', 'textile'], format)) {
            return format;
        }
        else {
            return 'markdown';
        }
    };

    PandocWidget.prototype.sendRequest = function(value) {
        var url = '/admin/pandoc/' + this.getInputFormat() + '/html';
        var data = { 'body': value };
        var me = this;

        if (this.sending) {
            this.pendingRequest = value;
        }
        else {
            this.sending = true;
            $.post(url, data, function(response){
                me.previewBox.html(response);
                me.adjustScrollPosition();
                me.sending = false;
                if (me.pendingRequest) {
                    var nextRequest = me.pendingRequest;
                    me.pendingRequest = null;
                    me.sendRequest(nextRequest);
                }
            });
        }
    };

    PandocWidget.prototype.dataBind = function(value) {
        this.textBox.val(value);
        this.sendRequest(value);
    };

    PandocWidget.prototype.getData = function() {
        return this.textBox.val();
    };

    return PandocWidget;
});
