/**
 * A field list widget, implementing a list of fields (app/widgets/field.js).
 * Supports adding, modifying, and removing fields.
 * Field values are extracted from an array or object upon data binding, and
 * returned as an array through getData.
 */
define(
['zepto', 'underscore', 'require', 'inherit', 'app/widgets/base'],
function($, _, require, inherit, BaseWidget) {
    var FieldListWidget = inherit(BaseWidget, function() {
        BaseWidget.call(this);
        this.fields = [];
    });

    FieldListWidget.prototype.getName = function() { return 'FieldList widget'; };

    FieldListWidget.prototype.createElem = function() {
        this.innerWrapper = null;
        this.wrapper = $('<div/>').addClass('widget-field-list');
        this.fields = [];
        return this.wrapper;
    };

    FieldListWidget.prototype.dataBind = function(value) {
        var me = this;
        var Widgets = require("app/widgets/factory");

        this.innerWrapper = $('<div/>').addClass('widget-field-list-inner');
        this.wrapper.empty().append(this.innerWrapper);
        this.fields = [];

        // Convert non-list/non-object values into something that makes sense
        // as a list.
        if (typeof(value) !== 'object') {
            if (typeof(value) === 'string') {
                value = value.split(/,\s*/);
            }
            else {
                value = [value];
            }
        }

        // We need to keep track of the iteration index to create a good
        // identifier for each widget.
        var ix = 0;
        var addField = function(fieldValue, unlock) {
            var field = Widgets.create('field', {
                buttons: [
                    { label: '⚙', eventName: 'toggleLock' },
                    { label: '↑', eventName: 'moveUp' },
                    { label: '↓', eventName: 'moveDown' },
                    { label: '✖', eventName: 'delete' }
                ],
                identifier: ix++
            });
            me.fields.push(field);
            var fieldElem = field.createElem();
            me.innerWrapper.append(fieldElem);
            field.dataBind(fieldValue);

            // Wire up events
            field.on('change', function(){
                me.raise('change', me.getData());
            });
            field.on('delete', function(){
                // This is a bit of a kludge - we need to remove the
                // appropriate items from both the `fields` property *and* the
                // DOM.
                me.fields = _.reject(me.fields, function(f) {
                    return f.options.identifier === field.options.identifier;
                });
                fieldElem.remove();

                // And obviously, we need to signal a data change.
                me.raise('change', me.getData());
            });
            field.on('moveUp', function(){
                // First, move the field itself
                var identifiers = _.pluck(_.pluck(me.fields, 'options'), 'identifier');
                var index = identifiers.indexOf(field.options.identifier);
                if (index < 1) {
                    return;
                }
                var before = me.fields.slice(0, index);
                var pred = before.pop();
                var after = me.fields.slice(index + 1);
                me.fields = before.concat(field, pred, after);

                // Then move the DOM element.
                var predElem = $(fieldElem).prev();
                fieldElem.remove();
                fieldElem.insertBefore(predElem);
                me.raise('change', me.getData());
            });
            field.on('moveDown', function(){
                // First, move the field itself
                var identifiers = _.pluck(_.pluck(me.fields, 'options'), 'identifier');
                var index = identifiers.indexOf(field.options.identifier);
                if (index >= me.fields.length - 1 || index < 0) {
                    return;
                }
                var before = me.fields.slice(0, index);
                var after = me.fields.slice(index + 1);
                var succ = after.shift();
                me.fields = before.concat(succ, field, after);

                // Then move the DOM element.
                var succElem = $(fieldElem).next();
                fieldElem.remove();
                fieldElem.insertAfter(succElem);
                me.raise('change', me.getData());
            });
            if (unlock) {
                field.raise('toggleLock');
            }
        }

        _(value).each(function(x){ addField(x, false); });

        // The "Add a field" button:
        var addFieldButton =
                $('<button type="button"/>')
                    .text('Add Field...')
                    .on('click', function(){
                        addField('', true);
                    });
        this.wrapper.append(addFieldButton);
    };

    FieldListWidget.prototype.getData = function() {
        return _(this.fields).map(function(field){
            return field.getData();
        });
    };

    return FieldListWidget;
});
