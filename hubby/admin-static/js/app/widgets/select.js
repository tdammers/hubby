/**
 * A dropdown box, optionally equipped with an "other" option, displayed as an
 * additional text box to allow selecting arbitrary other options.
 *
 * Supported options:
 * - items: a list of objects specifying the items to display in the dropdown
 *   list. Each object must have a .value property, and may have a .label
 *   property to override the displayed text.
 * - otherOption: If specified, an object with a .label property to add to the
 *   dropdown box. If unspecified, no such option is added, and the "other"
 *   textbox will not be created.
 */
define(
['zepto', 'underscore', 'inherit', 'app/widgets/base'],
function($, _, inherit, BaseWidget) {
    var SelectWidget = inherit(BaseWidget);

    SelectWidget.prototype.getName = function() { return 'Select widget'; };

    SelectWidget.prototype.createElem = function() {
        var me = this;
        this.dropdown = $('<select/>').addClass('widget-select-dropdown');
        _.each(this.options.items, function(item) {
            me.dropdown.append(
                $('<option/>')
                    .attr('value', item.value)
                    .text(item.label || item.value));
        });
        if (this.options.otherOption) {
            this.dropdown.append(
                $('<option/>')
                    .attr('value', '_other')
                    .text(this.options.otherOption.label));
        }
        var me = this;
        me.dropdown.on('click keyup', function() {
            if (me.dropdown.val() === '_other') {
                me.showOtherText();
            }
            else {
                me.hideOtherText();
            }
            me.raise('change', me.getData());
        });
        var elem = $('<span/>').addClass('widget-select')
            .append(this.dropdown);
        if (this.options.otherOption) {
            this.otherText =
                $('<input type="text"/>')
                    .addClass('widget-select-other-text')
                    .on('keyup', function() {
                        me.raise('change', me.getData());
                    });
            elem.append(this.otherText);
        }
        else {
            this.otherText = null;
        }
        return elem;
    };

    SelectWidget.prototype.dataBind = function(value) {
        var selectValue = null;
        var me = this;
        if (this.options.otherOption) {
            this.showOtherText();
            selectValue = '_other';
        }
        _.each(this.options.items, function(item) {
            if (item.value === value) {
                selectValue = value;
                if (me.options.otherOption) {
                    me.hideOtherText();
                }
            }
        });
        this.dropdown.val(selectValue);
        if (this.options.otherOption) {
            this.otherText.val(value);
        }
    };

    SelectWidget.prototype.getData = function() {
        var selectValue = this.dropdown.val();
        if (selectValue === '_other') {
            return this.otherText.val();
        }
        else {
            return selectValue;
        }
    };

    SelectWidget.prototype.showOtherText = function() {
        if (this.otherText) this.otherText.show();
    },

    SelectWidget.prototype.hideOtherText = function() {
        if (this.otherText) this.otherText.hide();
    }

    return SelectWidget;
});
