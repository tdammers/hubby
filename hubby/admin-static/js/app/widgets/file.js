/**
 * A table widget, implementing a 2D table. Each column defines a field type
 * and a name, while the cells contain the field values.
 * Binding expects an object containing the keys "columns" (a list of { type,
 * name } objects) and "rows" (a list of lists of fields).
 */
define(
['zepto', 'underscore', 'require', 'inherit', 'app/widgets/base', 'app/widgets/field'],
function($, _, require, inherit, BaseWidget, FieldWidget) {
    var FileWidget = inherit(BaseWidget, function() {
        BaseWidget.call(this);
        this.filename = null;
    });

    FileWidget.prototype.getTitle = function() { return 'File widget'; };

    FileWidget.prototype.createElem = function() {
        this.imgWrapper = $('<div/>').addClass('widget-file-img-wrapper');
        this.img = $('<img/>').appendTo(this.imgWrapper);
        this.imgPlaceholder =
                $('<div/>')
                    .addClass('widget-file-placeholder')
                    .appendTo(this.imgWrapper)
                    .text('?');
        this.spanFilename = $('<span/>').text('---');
        this.wrapper = $('<div/>')
            .addClass('widget-file');
        var item =
                $('<div/>')
                    .addClass('widget-file-item')
                    .append(this.imgWrapper)
                    .append(this.spanFilename)
                    .appendTo(this.wrapper);
        item.on('click', this.openPicker.bind(this));
        return this.wrapper;
    };

    FileWidget.prototype.dataBind = function(value) {
        var me = this;
        var Widgets = require("app/widgets/factory");

        if (value.filename) {
            this.img.attr('src', '/admin/thumb/120x120/' + value.filename).show();
            this.imgPlaceholder.hide();
            this.spanFilename.text(value.filename);
        }
        else {
            this.img.hide();
            this.imgPlaceholder.show();
            this.spanFilename.text('(no file selected)');
        }
        this.filename = value.filename;
    };

    FileWidget.prototype.getData = function() {
        return { 'filename': this.filename };
    };

    FileWidget.prototype.openPicker = function() {
        var widget = this;
        var overlay = $('<div class="modal-overlay"/>').appendTo($('body'));
        var dialog = $('<div class="modal" />').appendTo(overlay);
        var container = $('<div class="widget-file-grid"/>').appendTo(dialog);
        var closeDialog = function() {
            overlay.remove();
        };
        var handleClick = function(filename) {
            var data = { 'filename': filename };
            widget.dataBind(data);
            widget.raise('change', data);
            closeDialog(); 
        };
        overlay.on('click', function() { closeDialog(); });
        $.get('/admin/files.json', function(data) {
            Array.sort(data);
            _.each(data, function(filename){
                container.append(
                    $('<div/>')
                        .addClass("widget-file-item")
                        .addClass("widget-file-grid-item")
                        .append(
                            $('<img/>')
                                .attr('src', '/admin/thumb/120x120/' + filename))
                        .append(
                            $('<span/>')
                                .text(filename))
                        .on('click', function(){handleClick(filename);}));
            });
        });
    };

    return FileWidget;
});

