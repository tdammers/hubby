/**
 * Widget factory.
 *
 * The .create() method creates a widget based on a type name and an object of
 * options.
 *
 * Currently, the following type names are supported:
 * - string (a one-line text field, widgets/string.js)
 * - text (a longer block of flow text, widgets/string.js)
 * - password (a password field, widgets/string.js)
 * - number (a one-line numeric field, widgets/string.js)
 * - select (a drop-down box, widgets/select.js)
 * - field (a record field, with a name, title, and value, widgets/field.js)
 * - field-list (a list of record fields, widgets/field_list.js)
 * - json (a textarea to edit raw JSON, widgets/json.js)
 */
define(
[ 'require'
, 'app/widgets/string'
, 'app/widgets/select'
, 'app/widgets/file'
, 'app/widgets/field'
, 'app/widgets/field_list'
, 'app/widgets/json'
, 'app/widgets/table'
, 'app/widgets/pandoc'
],
function(require){
    var getWidgetRecipe = function(widgetType) {
        switch (widgetType) {
            case 'string':
                return { ctor: require('app/widgets/string'), options: { style: 'string' } };
            case 'text':
                return { ctor: require('app/widgets/string'), options: { style: 'text' } };
            case 'markdown':
            case 'rst':
            case 'textile':
            case 'html':
            case 'mediawiki':
            case 'docbook':
            case 'latex':
                return { ctor: require('app/widgets/pandoc'), options: { format: widgetType } };
            case 'password':
                return { ctor: require('app/widgets/string'), options: { style: 'password' } };
            case 'number':
                return { ctor: require('app/widgets/string'), options: { style: 'number' } };
            case 'select':
                return { ctor: require('app/widgets/select'), options: { } };
            case 'file':
                return { ctor: require('app/widgets/file'), options: { } };
            case 'field':
                return { ctor: require('app/widgets/field'), options: { } };
            case 'field-list':
                return { ctor: require('app/widgets/field_list'), options: { } };
            case 'table':
                return { ctor: require('app/widgets/table'), options: { } };
            case 'json':
            default:
                return { ctor: require('app/widgets/json'), options: {} };
        }
    };

    var Widgets = {
        create: function(widgetTypeName, options) {
            var widget;
            if (typeof(options) === "undefined") {
                options = {};
            }
            recipe = getWidgetRecipe(widgetTypeName);
            widget = new recipe.ctor();
            widget.init(_.extend(options, recipe.options));
            return widget;
        }
    };

    return Widgets;
});
