/**
 * A text widget. Supports several styles, rendering different DOM elements and
 * producing slightly different semantics.
 *
 * Supported options:
 * - style: The style for the text widget. Possible values:
 *   - 'string' (the default): A one-line text box. Produces a JSON string.
 *   - 'text': like 'string', but renders as a textarea.
 *   - 'number': like 'string', but produces a JSON number if the input is
 *               numeric.
 *   - 'password': like 'string', but renders as a password field.
 */
define(
['zepto', 'underscore', 'inherit', 'app/widgets/base'],
function($, _, inherit, BaseWidget) {
    var StringWidget = inherit(BaseWidget);

    StringWidget.prototype.getName = function() { return 'String widget'; };

    StringWidget.prototype.createElem = function() {
        switch (this.options.style) {
            case 'text':
                this.textBox = $('<textarea/>').addClass('widget-text');
                break;
            case 'password':
                this.textBox = $('<input type="password"/>').addClass('widget-password');
                break;
            case 'number':
                this.textBox = $('<input type="text"/>').addClass('widget-number');
                break;
            case 'string':
            default:
                this.textBox = $('<input type="text"/>').addClass('widget-string');
                break;
        }
        var me = this;
        this.textBox.on('keyup', function() {
            me.raise('change', me.getData());
        });
        _.each(this.options.extraClasses, function(c) {
            me.textBox.addClass(c);
        });
        return this.textBox;
    };

    StringWidget.prototype.dataBind = function(value) {
        if (value) {
            // Make sure we're using a String here
            this.textBox.val(String(value));
        }
        else {
            this.textBox.val('');
        }
        this.raise('dataBound', this.getData());
    };

    StringWidget.prototype.getData = function() {
        var val = this.textBox.val();
        if (this.options.style === 'number') {
            // is-numeric test: if casting to Number yields NaN, it's not a
            // number.
            var numVal = Number(val);
            if (Number.isNaN(numVal)) {
                return val;
            }
            else {
                return numVal;
            }
        }
        else {
            return val;
        }
    };

    return StringWidget;
});
