/**
 * A record field.
 * Consists of three sub-field: a *title* text field, a *type* selector, and
 * a *value* widget. The value widget is (re-)instantiated based on the value
 * of the type selector.
 *
 * .dataBind() expects either a "plain" value, or a "decorated" value, which is
 * an object providing at least one of ._value, ._type, ._name. A plain value
 * is automatically decorated internally by mapping its type to the most
 * suitable generic type: strings become StringWidgets, numbers become numeric
 * StringWidgets, arrays become field lists, anything else becomes raw JSON.
 *
 * .getData() returns a "plain" value when this does not lead to loss of
 * information, i.e., feeding the plain value to .dataBind() would produce the
 * exact same value. When lossless conversion to a plain value is not possible,
 * a decorated value is returned instead.
 *
 * Supported options:
 * - 'buttons': an array of objects defining extra buttons to display in the
 *   top row of the widget. Each button definition must have two fields:
 *   'eventName', defining the event that the button raises when clicked, and
 *   'label', the text to display on the button.
 */
define(
['zepto', 'underscore', 'require', 'inherit', 'app/widgets/base', 'app/widgets/factory'],
function($, _, require, inherit, BaseWidget) {
    var FieldWidget = inherit(BaseWidget);

    var widgetTypes =
        [ { value: 'string', label: 'Short text' }
        , { value: 'text', label: 'Long text' }
        , { value: 'markdown', label: 'Markdown' }
        , { value: 'rst', label: 'ReStructured Text' }
        , { value: 'textile', label: 'Textile' }
        , { value: 'html', label: 'HTML' }
        , { value: 'mediawiki', label: 'MediaWiki' }
        , { value: 'docbook', label: 'DocBook' }
        , { value: 'latex', label: 'LaTeX' }
        , { value: 'number', label: 'Numeric' }
        , { value: 'file', label: 'File' }
        , { value: 'field-list', label: 'Field list' }
        , { value: 'table', label: 'Table' }
        , { value: 'json', label: 'JSON' }
        ];

    FieldWidget.getWidgetTypes = function() { return widgetTypes; };

    FieldWidget.createTitleField = function() {
        var Widgets = require('app/widgets/factory');
        return Widgets.create(
                    'string',
                    { extraClasses: [ 'widget-field-title' ] });
    };

    FieldWidget.createTypeField = function() {
        var Widgets = require('app/widgets/factory');
        return Widgets.create('select', {
                    // Do use the "other" option
                    otherOption: {
                        label: 'other:'
                    },
                    items:
                        _.map(widgetTypes,
                            function(v) {
                                return {
                                    value: v.value,
                                    label: v.label
                                };
                            })
                });
    };

    FieldWidget.prototype.toggleLock = function() { this.wrapper.toggleClass('widget-locked'); };
    FieldWidget.prototype.lock = function() { this.wrapper.addClass('widget-locked'); };
    FieldWidget.prototype.unlock = function() { this.wrapper.removeClass('widget-locked'); };

    FieldWidget.prototype.getName = function() { return 'Field widget'; };

    FieldWidget.prototype.createElem = function() {
        var me = this;
        var Widgets = require('app/widgets/factory');

        // Prepare individual components: extra buttons, title, type, and a
        // container to hold the value field.
        this.buttonsContainer = null;
        if (this.options && this.options.buttons) {
            this.buttonsContainer = $('<div/>').addClass('widget-field-buttons');
            _.each(this.options.buttons, function(b) {
                me.buttonsContainer
                    .append(
                        $('<button/>')
                            .text(b.label)
                            .attr('type', 'button')
                            .addClass('widget-field-button')
                            .on('click', function() { me.raise(b.eventName); }));
            });
        }

        this.titleField = FieldWidget.createTitleField();
        this.typeField = FieldWidget.createTypeField();
        this.valueContainer = $('<div/>').addClass('widget-field-value');
        this.valueField = null;
        var titleStatic = $('<span/>').addClass('widget-field-title');
        this.titleField.on(['change', 'dataBound'], function(tf){
            titleStatic.text(tf.getData());
        });
        this.wrapper =
            $('<div/>').addClass('widget-field')
                .append(this.buttonsContainer)
                .append(
                    $('<div/>')
                        .addClass('widget-field-locked-invisible')
                        .append($('<label/>').text('name'))
                        .append(this.titleField.createElem()))
                .append(
                    $('<div/>')
                        .addClass('widget-field-locked-only')
                        .append(titleStatic))
                .append(
                    $('<div/>')
                        .addClass('widget-field-locked-invisible')
                        .append($('<label/>').text('type'))
                        .append(this.typeField.createElem()))
                .append(this.valueContainer);

        // Changes to the "title" field raise the "change" event.
        this.titleField.on('change', function(){
            me.raise('change', me.getData());
        });

        // Changes to the "type" field rebind the value widget and raise the
        // "change" event.
        this.typeField.on('change', function(){
            me.rebindValueContainer();
            me.raise('change', me.getData());
        });

        this.on('lock', this.lock.bind(this));
        this.on('unlock', this.unlock.bind(this));
        this.on('toggleLock', this.toggleLock.bind(this));

        this.lock();

        // NB; changes to the value field itself also raise the "change" event,
        // but because the value field gets recreated on data binding and type
        // changes, we wire up the events in the dataBind method, not here.

        return this.wrapper;
    };

    FieldWidget.prototype.dataBind = function(data) {
        var value, type, name;

        // Check if we are binding against a "decorated" value or a plain one
        if (data && (data._type || data._name || data._value)) {
            value = data._value || '';
            type = data._type || '';
            name = data._name || '';
        }
        else {
            value = data;
            type = '';
            name = '';
        }

        // When binding against a "plain" value, determine the most appropriate
        // type:
        if (!type) {
            switch (typeof(value)) {
                case 'string':
                case 'number':
                    type = typeof(value);
                    break;
                case 'object':
                    if (Array.isArray(value)) {
                        type = 'field-list';
                    }
                    else {
                        type = 'json';
                    }
                default:
                    type = 'json';
                    break;
            }
        }
        this.titleField.dataBind(name);
        this.typeField.dataBind(type);
        this.bindValueContainer(value);
    };

    FieldWidget.prototype.getData = function() {
        // Create a decorated value
        var value = {
            _name: this.titleField.getData(),
            _type: this.typeField.getData(),
            _value: this.valueField.getData()
        };
        // Check if it can be returned as a plain value
        if (!value._name) {
            if (!value._type ||
                value._type === 'string' ||
                value._type === 'number' ||
                value._type === 'field-list') {
                return value._value;
            }
            delete(value._name);
        }
        return value;
    };

    /**
     * Bind the actual value by creating a suitable sub-widget and wiring it
     * up.
     */
    FieldWidget.prototype.bindValueContainer = function(value) {
        var Widgets = require('app/widgets/factory');
        var me = this;
        var type = this.typeField.getData();

        this.valueField = Widgets.create(type, {});
        this.valueContainer.empty();
        this.valueField.attachTo(this.valueContainer);
        this.valueField.dataBind(value);
        this.valueField.on('change', function(){
            me.raise('change', me.getData());
        });
    };

    /**
     * Re-bind the value sub-widget to the appropriate type, keeping the raw
     * value intact as much as possible.
     */
    FieldWidget.prototype.rebindValueContainer = function() {
        var value;
        if (this.valueField) {
            value = this.valueField.getData();
        }
        else {
            value = '';
        }
        this.bindValueContainer(value);
    };

    return FieldWidget;
});
