/**
 * A table widget, implementing a 2D table. Each column defines a field type
 * and a name, while the cells contain the field values.
 * Binding expects an object containing the keys "columns" (a list of { type,
 * name } objects) and "rows" (a list of lists of fields).
 */
define(
['zepto', 'underscore', 'require', 'inherit', 'app/widgets/base', 'app/widgets/field'],
function($, _, require, inherit, BaseWidget, FieldWidget) {
    var TableWidget = inherit(BaseWidget, function() {
        BaseWidget.call(this);
        this.fields = [];
    });

    TableWidget.prototype.getTitle = function() { return 'Table widget'; };

    TableWidget.prototype.createElem = function() {
        this.innerWrapper = null;
        this.wrapper = $('<div/>').addClass('widget-table');
        this.columns = [];
        this.rows = [];
        return this.wrapper;
    };

    TableWidget.prototype.dataBind = function(value) {
        var me = this;
        var Widgets = require("app/widgets/factory");

        this.innerWrapper = $('<table/>').addClass('widget-table-inner');
        this.wrapper.empty().append(this.innerWrapper);

        // TODO: Convert non-list/non-object values into something that makes
        // sense as a list.

        var me = this;

        value = value || {"rows":[], "columns":[]};
        var columns = value.columns || [];
        var rows = value.rows || [];

        var headerTypeField;
        var headerTitleField;
        var tableCell;

        var tableRow = $('<tr/>').addClass('widget-table-header');
        tableRow.append($('<th/>')
            .text('+')
            .addClass('widget-table-column-insert')
            .attr('title', 'Insert column')
            .on('click', _.partial(me.insertColumn.bind(me), 0)));
        // tableRow.append($('<td/>'));
        me.columns = [];
        me.rows = [];
        _.each(columns, function(col, index){
            tableRow.append($('<th/>')
                .text('-')
                .addClass('widget-table-column-delete')
                .attr('title', 'Delete column')
                .on('click', _.partial(me.deleteColumn.bind(me), index)));
            headerTypeField = FieldWidget.createTypeField();
            headerTitleField = FieldWidget.createTitleField();
            tableCell = $('<th/>');
            tableCell.append(
                $('<span/>')
                    .append(headerTitleField.createElem()));
            tableCell.append(headerTypeField.createElem());
            tableRow.append(tableCell);
            me.columns.push({"type":headerTypeField, "name":headerTitleField});
            headerTypeField.dataBind(col.type);
            headerTitleField.dataBind(col.name);
            headerTypeField.on('change', function(){
                var data = me.getData();
                me.dataBindRows(data);
                me.raise('change', data);
            });
            headerTitleField.on('change', function(){
                me.raise('change', me.getData());
            });
            tableRow.append($('<th/>')
                .text('+')
                .addClass('widget-table-column-insert')
                .attr('title', 'Insert column')
                .on('click', _.partial(me.insertColumn.bind(me), index + 1)));
        });
        this.innerWrapper.append($('<thead/>').append(tableRow));
        this.tbody = $('<tbody/>');
        this.innerWrapper.append(this.tbody);
        this.dataBindRows(value);
    };

    TableWidget.prototype.dataBindRows = function(data) {
        var me = this;
        var columnCount = data.columns.length || 1;
        var Widgets = require("app/widgets/factory");
        var selectedElem = null;
        // selectedRow = selectedRow || null;
        // selectedColumn = selectedColumn || null;
        me.rows = [];
        me.tbody.empty();
        _.each(data.rows, function(row, rowIndex){
            var tableRow = $('<tr/>').addClass('widget-table-row');
            tableRow.append(
                $('<th/>')
                    .text('-')
                    .addClass('widget-table-row-delete')
                    .attr('title', 'Delete row')
                    .on('click', _.partial(me.deleteRow.bind(me), rowIndex)));
            me.rows.push([]);
            _.each(row, function(item, index) {
                var headerCell = me.columns[index];
                var field;
                var type = "string";
                if (headerCell) {
                    type = headerCell.type.getData();
                }
                var tableCell = $('<td/>');
                field = Widgets.create(type);
                var elem = field.createElem();
                tableCell.append(elem);
                tableCell.attr('colspan', 3);
                tableRow.append(tableCell);
                field.dataBind(item);
                field.on('change', function(){
                    me.raise('change', me.getData());
                });
                me.rows[rowIndex][index] = field;
            });
            me.tbody.append(
                $('<tr/>')
                    .append(
                        $('<td/>')
                            .text('+')
                            .addClass('widget-table-row-insert')
                            .attr('colspan', columnCount * 3 + 2)
                            .attr('title', 'Insert row')
                            .on('click', _.partial(me.insertRow.bind(me), rowIndex))));
            me.tbody.append(tableRow);
        });
        me.tbody.append(
            $('<tr/>')
                .append(
                    $('<td/>')
                        .text('+')
                        .addClass('widget-table-row-insert')
                        .attr('colspan', columnCount * 3 + 2)
                        .attr('title', 'Insert row')
                        .on('click', _.partial(me.insertRow.bind(me), me.rows.length))));
    };

    TableWidget.prototype.getData = function() {
        var data = { "columns": [], "rows": [] };
        _.each(this.columns, function(colWidgets){
            data.columns.push({
                "type":colWidgets.type.getData(),
                "name":colWidgets.name.getData()
                });
        });
        _.each(this.rows, function(rowWidgets){
            var row = [];
            _.each(rowWidgets, function(field) {
                row.push(field.getData());
            });
            data.rows.push(row);
        });
        return data;
    };

    TableWidget.prototype.insertRow = function(position) {
        var data = this.getData();
        var rowsBefore = _.first(data.rows, position) || [];
        var rowsAfter = _.rest(data.rows, position) || [];
        var newRow = _.map(data.columns, function(){return null});
        data.rows = rowsBefore.concat([newRow], rowsAfter);
        this.dataBind(data);
        this.raise('change', data);
    };

    TableWidget.prototype.deleteRow = function(position) {
        var data = this.getData();
        var rowsBefore = _.first(data.rows, position);
        var rowsAfter = _.rest(data.rows, position + 1);
        if (!Array.isArray(rowsBefore)) {
            rowsBefore = [];
        }
        if (!Array.isArray(rowsAfter)) {
            rowsAfter = [];
        }
        data.rows = rowsBefore.concat(rowsAfter);
        this.dataBind(data);
        this.raise('change', data);
    };

    TableWidget.prototype.insertColumn = function(position) {
        var data = this.getData();
        var columnsBefore = _.first(data.columns, position) || [];
        var columnsAfter = _.rest(data.columns, position) || [];
        var newColumn = {"type":"string"};
        data.columns = columnsBefore.concat([newColumn], columnsAfter);
        data.rows = data.rows || [];
        data.rows = _.map(data.rows, function(row){
            var cellsBefore = _.first(row, position) || [];
            var cellsAfter = _.rest(row, position) || [];
            var newCell = "";
            return cellsBefore.concat([newCell], cellsAfter);
        });
        this.dataBind(data);
        this.raise('change', data);
    };

    TableWidget.prototype.deleteColumn = function(position) {
        var data = this.getData();
        var columnsBefore = _.first(data.columns, position) || [];
        var columnsAfter = _.rest(data.columns, position + 1) || [];
        var newColumn = {"type":"string"};
        data.columns = columnsBefore.concat(columnsAfter);
        data.rows = data.rows || [];
        data.rows = _.map(data.rows, function(row){
            var cellsBefore = _.first(row, position) || [];
            var cellsAfter = _.rest(row, position + 1) || [];
            return cellsBefore.concat(cellsAfter);
        });
        this.dataBind(data);
        this.raise('change', data);
    };


    return TableWidget;
});
