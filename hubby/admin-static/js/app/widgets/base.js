/**
 * Base "class" for all widgets.
 *
 * Basic widget usage:
 *
 * 1. Create a widget; either directly, or through the widget factory
 *    (widgets/factory.js).
 * 2. Call the widget's .init() method with appropriate options. The factory
 *    will do this for you if you use it, so you don't need to bother.
 * 3. Hook the widget into the page DOM. You can do this explicitly by calling
 *    .createElem() and inserting the return value into the DOM yourself (e.g.:
 *    var e = widget.createElem(); $('#my_form').append(e);), or you can use
 *    the .attachTo() method, which appends the element into its first
 *    argument.
 * 4. Use the .on() method to tap into the widget's events. Most widgets
 *    support at least the 'change' event, which fires whenever the widget's
 *    data has changed.
 * 5. Data-bind the widget by calling its .dataBind() method.
 * 6. In order to read out the widget's raw data, use the .getData() method, or
 *    capture the second argument of the 'change' event.
 */
define(
['zepto', 'underscore'],
function($, _) {
    var BaseWidget = function() {
        this.handlers = {};
        this.options = {};
    };

    BaseWidget.prototype.init = function(options) {
        this.handlers = {};
        this.options = options;
    };

    BaseWidget.prototype.createElem = function() {
        return $('<div>(base widget: no controls)</div>');
    };

    BaseWidget.prototype.attachTo = function(parentElem) {
        parentElem.append(this.createElem());
    };

    BaseWidget.prototype.dataBind = function(value) {
    };

    BaseWidget.prototype.getData = function() {
        return null;
    };

    BaseWidget.prototype.getName = function() { return 'Base widget'; };

    BaseWidget.prototype.on = function(eventName, handler) {
        if (Array.isArray(eventName)) {
            // bind to multiple events at once
            _.each(eventName, (function(e) {
                this.on(e, handler);
            }).bind(this));
        }
        else {
            if (!Array.isArray(this.handlers[eventName])) {
                this.handlers[eventName] = [ handler ];
            }
            else {
                this.handlers[eventName].push(handler);
            }
        }
    };

    BaseWidget.prototype.raise = function(eventName, data) {
        var me = this;
        var handlers = this.handlers[eventName] || [];
        _.each(handlers, function(handler) {
            handler(me, data);
        });
    };

    return BaseWidget;
});
