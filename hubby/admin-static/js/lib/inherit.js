define(
function(){
    var inherit = function(Blueprint, Inheritee) {
        if (!Inheritee) {
            Inheritee = function() {
                Blueprint.call(this);
            }
        }
        Inheritee.prototype = Object.create(Blueprint.prototype);
        Inheritee.prototype.constructor = Inheritee;
        return Inheritee;
    };
    return inherit;
});
