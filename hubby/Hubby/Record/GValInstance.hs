{-#LANGUAGE FlexibleInstances #-}
{-#LANGUAGE MultiParamTypeClasses #-}
{-#LANGUAGE OverloadedStrings #-}
module Hubby.Record.GValInstance
where

import Prelude hiding (length)
import qualified Prelude
import Text.Ginger
import Text.Ginger.Html
import Text.Ginger.GVal as GVal
import Hubby.Record
import Data.Aeson as JSON
import qualified Data.Text as Text
import Data.Text (Text)
import Control.Applicative
import Data.Maybe (fromMaybe, catMaybes)
import Data.List ( intersperse )
import Data.Monoid
import Data.Default
import Text.StringConvert
import Hubby.Pandoc
import Safe

instance ToGVal m Record where
    toGVal = recordToGVal

recordToGVal :: Record -> GVal m
recordToGVal rec =
    let base :: GVal m
        base = fieldsToGVal . recordFields $ rec
    in
        base
            { asLookup = Just (\key -> case s key :: String of
                                    "_json" -> Just $ jsonGVal rec
                                    "_fields" -> Just base
                                    "id" -> Just . toGVal . recordID $ rec
                                    _ -> lookupKey key base)
            }

fieldsToGVal :: RecordFields -> GVal m
fieldsToGVal rfields = def
    { asLookup = Just (\key -> case s key :: String of
                            "_json" -> Just $ jsonGVal fields
                            _ -> fieldToGVal <$> lookupField key rfields)
    , asDictItems = Just [(fromMaybe "" $ fieldName field, fieldToGVal field) | field <- fields ]
    , asList = Just (map fieldToGVal $ fields)
    , asHtml = mconcat
                [ mconcat
                    [ unsafeRawHtml "<section><h1>"
                    , html (fromMaybe "" $ fieldName field)
                    , unsafeRawHtml "</h1>"
                    , asHtml (fieldToGVal field)
                    , unsafeRawHtml "</section>"
                    ]
                | field <- fields ]
    , asText =
        mconcat
            [ (fromMaybe "<unnamed>" $ fieldName field) <> ": " <> asText (fieldToGVal field) <> "; "
            | field <- fields ]
    , length = Just $ Prelude.length fields
    , isNull = False
    }
    where
        fields = unRecordFields rfields

tableToGVal :: RecordTable -> GVal m
tableToGVal rtable = def
    { asLookup = Just (\key -> case s key :: String of
                                   _ -> Nothing)
    -- , asDictItems = Just [(fromMaybe "" $ fieldName field, fieldToGVal field) | field <- fields ]
    , asList = Just (map fieldsToGVal rows)
    , asHtml = mconcat
                [ unsafeRawHtml "<table>"
                , unsafeRawHtml "<tr>"
                , mconcat . map ((<> unsafeRawHtml "</th>") . (unsafeRawHtml "<th>" <>) . html . fromMaybe "??" . columnName) $ columns
                , unsafeRawHtml "</tr>"
                , mconcat [
                        mconcat
                            [ unsafeRawHtml "<tr>"
                            , mconcat
                                [ ((<> unsafeRawHtml "</td>") . (unsafeRawHtml "<td>" <>) . asHtml . fieldToGVal) $ field
                                | field <- unRecordFields row ]
                            , unsafeRawHtml "</tr>"
                            ]
                        | row <- rows ]
                , unsafeRawHtml "</table>"
                ]
    , asText = mconcat . intersperse ("; ") $
                [
                    mconcat . intersperse (", ") $
                        [ fromMaybe "" ((<> ": ") <$> fieldName field) <> (asText . toGVal . fieldValue) field
                        | field <- unRecordFields row
                        ]
                | row <- rows
                ]
    , length = Just $ Prelude.length rowsRaw
    , isNull = False
    }
    where
        rowsRaw = rtableRows rtable
        rows = map (recordTableRow rtable) rowsRaw
        columns = rtableColumns rtable

instance ToGVal m RecordField where
    toGVal = fieldToGVal

fieldToGVal :: RecordField -> GVal m
fieldToGVal field =
    base
        { asLookup = Just $ \key ->
            let mine = case s key :: String of
                            "_type" -> Just . toGVal . effectiveFieldType $ field
                            "_name" -> Just . toGVal . fieldName $ field
                            "_value" -> Just . toGVal . fieldValue $ field
                            "_json" -> Just . jsonGVal $ field
                            _ -> Nothing
                baseLookup = fromMaybe (const Nothing) (asLookup base)
                theirs = baseLookup key
            in mine <|> theirs
        , asText = case pandocInputFormatMay of
            Nothing -> asText base
            Just fmt -> s . pandoc fmt WritePlain . s . asText $ base
        , asHtml = case pandocInputFormatMay of
            Nothing -> asHtml base
            Just fmt -> unsafeRawHtml . s . pandoc fmt WriteHtml . s . asText $ base
        }
    where
        fieldTypeStringMay :: Maybe String
        fieldTypeStringMay = s <$> fieldType field

        fieldTypeString :: String
        fieldTypeString = fromMaybe "" fieldTypeStringMay

        pandocInputFormatMay :: Maybe PandocReaderFormat
        pandocInputFormatMay = readMay fieldTypeString

        base =
            case s <$> fieldType field :: Maybe String of
                Just "field-list" ->
                        case (fromJSON $ fieldValue field :: Result RecordFields) of
                            Error str -> def
                            Success xs -> fieldsToGVal $ xs
                Just "table" ->
                        case (fromJSON $ fieldValue field :: Result RecordTable) of
                            Error str -> def { asHtml = html "oy vey: " <> html (s str) }
                            Success xs -> tableToGVal $ xs
                _ -> case fieldValue field of
                        x -> toGVal x

jsonGVal :: ToJSON a => a -> GVal m
jsonGVal x =
    toGVal t
    where
        t :: Text
        t = s . JSON.encode $ x
