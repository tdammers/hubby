{-#LANGUAGE TemplateHaskell #-}
{-#LANGUAGE OverloadedStrings #-}
module Hubby.Storage.Providers.InProcess
( createInProcessStorage
, InProcessStorageConfig
)
where

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.Hashable (Hashable)
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Vector as Vector
import Data.Text (Text)
import Data.Aeson as JSON
import Data.Aeson.TH as JSON
import Data.Aeson.DeriveUtils
import Data.Aeson.ParseUtils
import Data.Maybe
import Data.List
import Data.Monoid
import Data.Default
import Text.StringConvert
import Data.Token

import Control.Concurrent
import Control.Concurrent.STM
import Control.Concurrent.QSem
import Control.Exception
import Control.Applicative
import Control.Monad

import Hubby.Logging
import Hubby.Storage.Type
import Hubby.Record
import Hubby.RecordQuery

import System.Posix.Files
import System.IO
import System.Lock.FLock
import System.IO.Error
import System.Random

type IPDBVar = TVar IPDB

data IPDB =
    IPDB
        { ipdbItems :: HashMap RecordID Record
        , ipdbGen :: StdGen
        }

ipdbInsert :: Record -> IPDB -> IPDB
ipdbInsert record db =
    db { ipdbItems = HashMap.insert (recordID record) record (ipdbItems db) }

ipdbDelete :: RecordID -> IPDB -> IPDB
ipdbDelete recordID db =
    db { ipdbItems = HashMap.delete recordID (ipdbItems db) }

hashMapBy :: (Eq k, Hashable k) => (a -> k) -> [a] -> HashMap k a
hashMapBy f xs = HashMap.fromList $ zip (map f xs) xs

data PersistenceMode
        = ReadWrite -- ^ Read on startup, write on shutdown.
        | ReadOnly -- ^ Read on startup, discard on shutdown.
        | Volatile -- ^ Start with a clean slate, discard on shutdown (no persistence).
        deriving (Show, Eq, Enum, Bounded, Ord)

$(deriveJSON
    defaultOptions { constructorTagModifier = camelToDashed '-' }
    ''PersistenceMode)

instance Default PersistenceMode where
    def = ReadWrite

data InProcessStorageConfig =
    InProcessStorageConfig
        { ipscDatabaseFilename :: Maybe FilePath
        , ipscLockFilename :: Maybe FilePath
        , ipscPersistenceMode :: PersistenceMode
        , ipscPersistInterval :: Maybe Int -- ^ Seconds between persistence runs
        }
$(deriveJSON
    defaultOptions { fieldLabelModifier = camelToDashed '-' . drop (length ("ipsc" :: String)) }
    ''InProcessStorageConfig)

instance Default InProcessStorageConfig where
    def = InProcessStorageConfig Nothing Nothing def Nothing

createInProcessStorage :: Value -> IO Storage
createInProcessStorage confJSON = do
    let configE = fromJSONWithDefaults def confJSON
    config <- case configE of
        Error x -> fail x
        Success x -> return x
    let persistenceMode = ipscPersistenceMode config
    when
        ( (persistenceMode /= Volatile) &&
          (isNothing $ ipscDatabaseFilename config)
        )
        (fail "In-Process Storage: No database file configured.")
    when
        ( (persistenceMode == ReadWrite) &&
          (isNothing $ ipscLockFilename config) &&
          (isJust $ ipscDatabaseFilename config)
        )
        (fail "In-Process Storage: No lockfile configured.")

    let makeLock :: Maybe FilePath -> IO (Maybe Lock)
        makeLock (Just filename) = Just <$> lock filename Exclusive Block
        makeLock Nothing = return Nothing
    lk <- makeLock (ipscLockFilename config)
    writeSem <- newQSem 1

    dbJSON <- case (persistenceMode, ipscDatabaseFilename config) of
                (Volatile, _) -> return "[]"
                (_, Nothing) -> return "[]"
                (_, Just fn) -> catchIOError
                                (openFile fn ReadMode >>= LBS.hGetContents)
                                (const $ return "[]")

    let dbData = fromMaybe HashMap.empty $
            -- | try the newer, compact format:
            (hashMapBy recordID <$> JSON.decode' dbJSON) <|>
            -- | gracefully accept a fully-serialized intMap (we'll
            -- automatically upgrade upon saving)
            JSON.decode' dbJSON
    rng <- newStdGen
    let db = IPDB { ipdbItems = dbData, ipdbGen = rng }
    dbv <- dbData `seq` (atomically $ newTVar db)

    let persistFilename =
            if persistenceMode == ReadWrite
                then ipscDatabaseFilename config
                else Nothing

    -- a signal for the persistence worker thread
    finished <- atomically $ newTVar False

    let persistInterval = ipscPersistInterval config
    case (persistenceMode, persistInterval) of
        (ReadWrite, Just interval) -> do
            writeLog $ "Will persist every " ++ show interval ++ " seconds."
            let persistT :: IO ()
                persistT = do
                    threadDelay (interval * 1000000)
                    ipPersist dbv persistFilename writeSem
                    writeLog "Persistence run completed"
                    -- keep going until "finish" signal is raised
                    isFinished <- atomically $ readTVar finished
                    when (not isFinished) persistT
            forkIO persistT
            return ()
        _ -> return ()

    return $ Storage
        (ipInsert dbv)
        (ipUpdate dbv)
        (ipDelete dbv)
        (ipGet dbv)
        (ipFind dbv)
        (ipSelect dbv)
        (ipCount dbv)
        (ipDispose dbv persistFilename lk finished writeSem)

-- @maybeM_ a x@ applies monadic action @a@ to @Just@ the value in @x@, or
-- @mzero@ if @x@ is Nothing.
maybeM_ :: Monad m => (a -> m ()) -> Maybe a -> m ()
maybeM_ f (Just x) = f x
maybeM_ _ Nothing = return ()

ipDispose :: IPDBVar -> Maybe FilePath -> Maybe Lock -> TVar Bool -> QSem -> IO ()
ipDispose db dbFilename lk finished writeSem = do
    atomically $ modifyTVar' finished (const True)
    ipPersist db dbFilename writeSem
    maybeM_ unlock lk

ipPersist :: IPDBVar -> Maybe FilePath -> QSem -> IO ()
ipPersist db dbFilename writeSem = do
    bracket_ (waitQSem writeSem) (signalQSem writeSem) $ do
        dbData <- atomically $ HashMap.elems . ipdbItems <$> readTVar db
        flip maybeM_ dbFilename $ \fn -> do
                h <- openFile fn WriteMode
                LBS.hPutStr h (JSON.encode dbData)
                hFlush h
                hClose h
        return ()

-- Generate a new unique random ID
ipGenID :: IPDBVar -> STM Text
ipGenID dbv = do
    db <- readTVar dbv
    let gen = ipdbGen db
        (candidate', gen') = mkToken gen alphabet 16
        candidate :: Text
        candidate = s candidate'
    modifyTVar' dbv (\db -> db { ipdbGen = gen' })
    if HashMap.member candidate (ipdbItems db)
        then ipGenID dbv -- key already exists, try again
        else return candidate -- key doesn't exists, we're fine
    where
        alphabet = ['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9'] ++ "_"

ipInsert :: IPDBVar -> RecordFields -> IO Record -- | insert a new record
ipInsert db fields = atomically $ do
    newID <- ipGenID db
    let record = Record { recordID = newID, recordFields = fields }
    modifyTVar' db $ ipdbInsert record
    return record

ipUpdate :: IPDBVar -> Record -> IO Record -- | replace one existing record with another (or create a new version)
ipUpdate db record = atomically $ do
    modifyTVar' db $ ipdbInsert record
    return record

ipDelete :: IPDBVar -> RecordID -> IO () -- | delete one record (or mark record as deleted)
ipDelete db recID = atomically $ do
    modifyTVar' db $ ipdbDelete recID
    return ()

ipGet :: IPDBVar -> RecordID -> IO (Maybe Record) -- | get one record by ID
ipGet db recID = atomically $
    HashMap.lookup recID . ipdbItems <$> readTVar db

ipFind :: IPDBVar -> QueryParams -> RecordQuery -> IO [RecordID] -- | find record IDs based on a query
ipFind db qp q = map recordID <$> ipSelect db qp q

ipCount :: IPDBVar -> QueryParams -> RecordQuery -> IO Int -- | count records based on a query
ipCount db qp q = length <$> ipSelect db qp q

ipSelect :: IPDBVar -> QueryParams -> RecordQuery -> IO [Record] -- | get records based on a query
ipSelect db qp (RecordQuery qMatch qOrder qLimit) = do
    records <- atomically $ HashMap.elems . ipdbItems <$> readTVar db
    let filterF = ipqFilter qp qMatch
        sortF = ipqSorts qOrder
        limitF = ipqLimit qLimit
    return (limitF . sortBy sortF . filter filterF $ records)

ipqFilter :: QueryParams -> Match -> Record -> Bool
ipqFilter qp MatchAll rec = True
ipqFilter qp MatchNone rec = False
ipqFilter qp (MatchID rid) rec = recordID rec == rid
ipqFilter qp (MatchBinary op a b) rec =
    let lhs = ipqGetCompVal qp a rec
        rhs = ipqGetCompVal qp b rec
    in ipqBinary op lhs rhs
ipqFilter qp (MatchTernary op a b c) rec =
    let lhs = ipqGetCompVal qp a rec
        mhs = ipqGetCompVal qp b rec
        rhs = ipqGetCompVal qp c rec
    in ipqTernary op lhs mhs rhs
ipqFilter qp (MatchEither a b) rec = ipqFilter qp a rec || ipqFilter qp b rec
ipqFilter qp (MatchBoth a b) rec = ipqFilter qp a rec && ipqFilter qp b rec
ipqFilter qp (MatchNot a) rec = not $ ipqFilter qp a rec

ipqGetCompVal :: QueryParams -> Operand -> Record -> Maybe Value
ipqGetCompVal _ (FieldOperand "id") rec = Just . String . recordID $ rec
ipqGetCompVal _ (FieldOperand fn) rec = fieldValue <$> lookupField fn (recordFields rec)
ipqGetCompVal _ (LiteralOperand v) _ = Just v
ipqGetCompVal qp (ParamOperand p) rec = qp p

looseEquals :: Value -> Value -> Bool
looseEquals (Number a) (Number b) = a == b
looseEquals (Number a) b = a == numericValue b
looseEquals (String a) b = s a == toString b
looseEquals b (Number a) = a == numericValue b
looseEquals b (String a) = s a == toString b
looseEquals (Array a) (Array b) = Vector.all id $ Vector.zipWith looseEquals a b
-- looseEquals (Object a) (Object b) = TODO
looseEquals a b = (isFalsy a && isFalsy b) || a == b

isFalsy :: Value -> Bool
isFalsy Null = True
isFalsy (Bool False) = True
isFalsy (Number 0) = True
isFalsy (String "") = True
isFalsy (Array a) = Vector.null a
isFalsy (Object a) = HashMap.null a
isFalsy _ = False

ipqBinary :: BinaryOperator -> Maybe Value -> Maybe Value -> Bool
ipqBinary _ Nothing _ = False -- null compares false against anything
ipqBinary _ _ Nothing = False -- null compares false against anything
ipqBinary OpEq (Just a) (Just b) = a `looseEquals` b
ipqBinary OpNEq (Just a) (Just b) = not $ a `looseEquals` b
ipqBinary OpLT (Just a) (Just b) = numericValue a < numericValue b
ipqBinary OpGT (Just a) (Just b) = numericValue a > numericValue b
ipqBinary OpLTE (Just a) (Just b) = numericValue a <= numericValue b
ipqBinary OpGTE (Just a) (Just b) = numericValue a >= numericValue b
ipqBinary OpLTStr (Just a) (Just b) = toString a < toString b
ipqBinary OpGTStr (Just a) (Just b) = toString a > toString b
ipqBinary OpLTEStr (Just a) (Just b) = toString a <= toString b
ipqBinary OpGTEStr (Just a) (Just b) = toString a >= toString b
ipqBinary OpInList (Just a) (Just (Object o)) = a `elem` HashMap.elems o
ipqBinary OpInList (Just a) (Just (Array v)) = Vector.elem a v
ipqBinary OpInList _ _ = False

ipqTernary :: TernaryOperator -> Maybe Value -> Maybe Value -> Maybe Value -> Bool
ipqTernary _ Nothing _ _ = False
ipqTernary _ _ Nothing _ = False
ipqTernary _ _ _ Nothing = False
ipqTernary OpBetween (Just a) (Just b) (Just c) =
    let a' = numericValue a
        b' = numericValue b
        c' = numericValue c
    in (a' <= b') && (b' <= c')

ipqSorts :: [Order] -> Record -> Record -> Ordering
ipqSorts orders a b = mconcat $ [ ipqSort o a b | o <- orders ]

ipqSort :: Order -> Record -> Record -> Ordering
ipqSort (Order x Descending) = flip $ ipqSort (Order x Ascending)
ipqSort (Order OrderID Ascending) = compareRecords recordID
ipqSort (Order (OrderFieldNum fname) Ascending) = compareFields numericValue fname
ipqSort (Order (OrderFieldAlpha fname) Ascending) = compareFields toString fname

compareRecords :: Ord a => (Record -> a) -> Record -> Record -> Ordering
compareRecords f a b = compare (f a) (f b)

compareFields :: Ord a => (Value -> a) -> FieldName -> Record -> Record -> Ordering
compareFields f fname = compareRecords (\rec -> f . fieldValue <$> lookupField fname (recordFields rec))

ipqLimit :: Limit -> [a] -> [a]
ipqLimit Unlimited = id
ipqLimit (LimitCount n) = take n
ipqLimit (LimitOffsetCount o n) = take n . drop o
