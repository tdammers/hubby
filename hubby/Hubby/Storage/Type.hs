{-#LANGUAGE OverloadedStrings #-}
module Hubby.Storage.Type where

import Data.Default
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Aeson (Value)

import Control.Applicative

import Hubby.Record
import Hubby.RecordQuery

type QueryParams = Text -> Maybe Value

data Storage =
    Storage
        { insert :: RecordFields -> IO Record -- | insert a new record
        , update :: Record -> IO Record -- | replace one existing record with another (or create a new version)
        , delete :: RecordID -> IO () -- | delete one record (or mark record as deleted)
        , get :: RecordID -> IO (Maybe Record) -- | get one record by ID
        , find :: QueryParams -> RecordQuery -> IO [RecordID] -- | find record IDs based on a query
        , select :: QueryParams -> RecordQuery -> IO [Record] -- | get records based on a query
        , count :: QueryParams -> RecordQuery -> IO Int -- | count records based on a query
        , dispose :: IO ()
        }

select' :: Storage -> RecordQuery -> IO [Record]
select' s = select s (const Nothing)

find' :: Storage -> RecordQuery -> IO [RecordID]
find' s = find s (const Nothing)

count' :: Storage -> RecordQuery -> IO Int
count' s = count s (const Nothing)

-- | A dummy implementation that throws away all input and never stores
-- anything.
blackHoleStorage :: Storage
blackHoleStorage =
    Storage
        { insert = \fields -> return $ Record "0" fields
        , update = return
        , delete = const $ return ()
        , get = const $ return Nothing
        , find = const . const $ return []
        , select = const . const $ return []
        , count = const . const $ return 0
        , dispose = return ()
        }

instance Default Storage where
    def = blackHoleStorage

-- | Useful default implementation for storage backends where a dedicated
-- 'select' method would not yield a performance benefit over using 'find' and
-- multiple 'get' in concert.
defaultSelect :: Storage -> QueryParams -> RecordQuery -> IO [Record]
defaultSelect storage qp query = catMaybes <$> (find storage qp query >>= mapM (get storage))

-- | Useful default implementation for storage backends where there is no
-- performance benefit from loading full records and then reducing them to IDs
-- (i.e. running 'select' and then extracting the 'recordID' fields).
defaultFind :: Storage -> QueryParams -> RecordQuery -> IO [RecordID]
defaultFind storage qp query = map recordID <$> select storage qp query

-- | A default implementation for 'get', based on 'select'. Useful for backends
-- where a full 'select' is equivalent to a specific get-by-ID operation, e.g.
-- SQL backends.
defaultGet :: Storage -> RecordID -> IO (Maybe Record)
defaultGet storage recid = do
    records <- select storage (const Nothing) (RecordQuery (MatchID recid) [] (LimitCount 1))
    case records of
        (x:_) -> return (Just x)
        [] -> return Nothing
