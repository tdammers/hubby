{-#LANGUAGE TemplateHaskell #-}
module Hubby.Session
where

import Data.Text (Text)
import qualified Data.Text as Text
import Data.Aeson.TH
import Data.Aeson.DeriveUtils

data Session
    = Session
        { sessionUserID :: Maybe Text
        , sessionCSRFToken :: Text
        }
        deriving (Show)
$(deriveLensJSON ''Session)
