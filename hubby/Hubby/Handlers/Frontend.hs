{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE FlexibleInstances #-}
module Hubby.Handlers.Frontend
where

import Data.Text (Text)
import qualified Data.Text as Text
import Data.Aeson as JSON
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.Maybe
import Data.List
import qualified Data.Vector as Vector
import Control.Monad
import Data.Yaml as YAML
import Text.StringConvert
import Data.Monoid
import Control.Applicative
import Control.Monad.Trans.Except
import Control.Monad.Trans
import Control.Monad.IO.Class
import Data.Default
import System.LoadFile
import System.FilePath
import Data.MimeType

import Text.Ginger

import Network.Wai
import Network.HTTP.Types

import Hubby.Handlers.Common
import Hubby.FrontendRoute
import Hubby.FrontendTemplate
import Hubby.RecordQuery
import Hubby.FieldType
import Hubby.RecordQuery.Print
import Hubby.Record
import Hubby.Record.GValInstance
import Hubby.Storage
import Hubby.AppContext
import Hubby.AppConfig
import qualified Hubby.FileStore as FS
import Hubby.Logging

tvNull :: TemplateValue
tvNull = TemplateValue JSON.Null

frontendH :: [Text] -> Handler
frontendH path = do
    feRoutes <- routes <$> getConfig
    frontendRoutesH feRoutes path

frontendRoutesH :: [FrontendRoute] -> [Text] -> Handler
frontendRoutesH routes path = do
    case matchFrontendRoutes routes path of
        Just (captures, route) -> frontendRouteH captures route
        Nothing -> frontend404H

expandPathWithCaptures :: HashMap Text Value -> Text -> Text
expandPathWithCaptures captures path =
    let pathParts = Text.splitOn "/" path
    in mconcat . intersperse "/" . doParts $ pathParts
    where
        doParts :: [Text] -> [Text]
        doParts = map doPart
        doPart :: Text -> Text
        doPart p =
            if "$" `Text.isPrefixOf` p
                then
                    let key = Text.drop 1 p
                        val = fromMaybe Null $ HashMap.lookup key captures
                    in flattenValue val
                else p
        flattenValue :: Value -> Text
        flattenValue (String t) = t
        flattenValue (Array xs) = mconcat . intersperse "/" . map flattenValue . Vector.toList $ xs
        flattenValue _ = ""

frontendRouteH :: HashMap Text Value -> FrontendRoute -> Handler
frontendRouteH captures route = do
    either handle return =<< runExceptT go
    where
        params key = HashMap.lookup key captures
        go :: ExceptT Text HandlerM ResponseReceived
        go = do
            storage <- lift getStorage
            context <- forM (ferContext route) $ \(ContextItem key cte) ->
                case cte of
                    StaticContextElem val -> return (key, TemplateValue val)
                    StaticFileContextElem path -> do
                        let fn = "static"
                                 </> ( s
                                     . expandPathWithCaptures captures
                                     . s
                                     $ path
                                     )
                        bodyMay <- liftIO $ loadFileMay fn
                        body <- case bodyMay of
                                    Nothing -> throwE $ "File not found: " <> s fn
                                    Just b -> return b
                        let fi = FileInfo
                                    { fileName = fn
                                    , fileMimeType = detectMimeTypeDef "text/plain" fn
                                    , fileContents = body
                                    }
                        return (key, TemplateFile fi)
                    UploadedFileContextElem fileID -> do
                        let fid = read
                                . s
                                . expandPathWithCaptures captures
                                $ fileID
                        store <- lift getFileStore
                        bodyMay <- liftIO $ do
                                        exists <- FS.fileExists store fid
                                        if exists
                                            then Just <$> FS.getFile store fid
                                            else return Nothing
                        body <- case bodyMay of
                                    Nothing -> throwE $ "File not found: " <> s fileID
                                    Just b -> return b
                        let fi = FileInfo
                                    { fileName = show fid
                                    , fileMimeType = detectMimeTypeDef "application/octet-stream" (s $ show fid)
                                    , fileContents = body
                                    }
                        return (key, TemplateFile fi)
                    QueryContextElem (ContextQuery query mode) -> do
                        result <- liftIO $ select storage params query
                        resultValue <- case mode of
                            RequireOne ->
                                case result :: [Record] of
                                    [] -> throwE $ "Not found: " <> s key <> "\n" <> s (printQuery query) <> "\nwith\n" <> s (JSON.encode captures)
                                    x:_ -> return $ TemplateRecord x
                            MaybeOne ->
                                case result of
                                    [] -> return tvNull
                                    x:_ -> return $ TemplateRecord x
                            Some ->
                                return $ TemplateRecords result
                            Many ->
                                case result of
                                    [] -> throwE $ "Not found: " <> s key <> "\n" <> s (printQuery query) <> "\nwith\n" <> s (JSON.encode captures)
                                    xs -> return $ TemplateRecords xs
                        return (key, resultValue)
            lift $ serveFrontendTemplate status200 (ferTemplate route) (("_captures", TemplateValue . JSON.Object $ captures):context)
        handle :: Text -> Handler
        handle e = do
            liftIO . writeLog . s $ e
            frontend404H

serveFrontendTemplate :: Status -> Text -> [(Text, TemplateValue)] -> Handler
serveFrontendTemplate status template context = do
    ftc <- FrontendTemplateContext <$> (appThumbCache <$> getApp)
    (contentType, body) <- liftIO $ applyFrontendTemplate ftc template context
    setResponseHeader "Cache-control" "max-age=60"
    respondH status contentType body

frontend404H :: Handler
frontend404H = do
    template <- e404Template <$> getConfig
    serveFrontendTemplate status404 template []
