{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE TemplateHaskell #-}
{-#LANGUAGE TupleSections #-}
module Hubby.Handlers.Admin
where

import Control.Applicative
import Control.Monad.IO.Class
import Data.Text (Text)
import qualified Data.Text as Text
import Text.StringConvert (s)
import Data.Monoid
import Data.Default
import Data.String
import Data.Maybe (fromMaybe)
import Data.Char
import qualified Data.HashMap.Strict as HashMap
import Data.HashMap.Strict (HashMap)
import qualified Data.ByteString.Lazy as LBS
import Safe (headMay, readMay)

import Data.FileEmbed
import Data.MimeType
import Data.MaybeFail

import qualified Data.Aeson as JSON
import qualified Data.Yaml as YAML

import System.FilePath (takeExtension)

import Network.Wai
import Network.Wai.Parse
import Network.HTTP.Types

import Hubby.AppConfig
import Hubby.AppContext
import Hubby.Authentication
import Hubby.Cookies
import Hubby.FileStore
import Hubby.HTTP
import Hubby.Thumbnails
import Hubby.Handlers.Common
import Hubby.Logging
import Hubby.Record
import Hubby.RecordQuery
import Hubby.RecordQuery.Parse as RecordQuery (parseQuery)
import Hubby.Session
import Hubby.Storage
import qualified Hubby.FileStore as FS
import Hubby.Templates.Admin
import Hubby.Templates.Master
import Hubby.Templates.Type
import Hubby.Pandoc (pandoc)

import Text.Tamper ( (!) )
import qualified Text.Tamper as H
import qualified Text.Tamper.Tags.Html as H
import qualified Text.Tamper.Tags.Html.Attributes as H hiding (form)

adminH :: [Text] -> Handler
adminH route =
    case route of
        [] -> requireLogin $ okTemplateH adminDashboardT
        "login":[] -> loginH
        "logout":[] -> logoutH
        "node":tail -> requireLogin $ nodeEditH tail
        "nodes":[] -> requireLogin $ nodeListH "All Nodes" def def
        "nodes":"search":[] -> requireLogin $ nodeSearchH
        "static":tail -> adminStaticH tail
        "files.json":[] -> requireLoginStealth $ filesH
        "json":"files":[] -> requireLoginStealth $ filesH
        "file":path -> requireLoginStealth $ fileH path
        "thumb":geometry:path -> requireLoginStealth $ thumbH geometry path
        "debug":tail -> adminDebugH tail
        "pandoc":from:to:[] -> requireLogin $ pandocH (s from) (s to)
        _ -> error404H

getAdminStaticFile :: Text -> Maybe LBS.ByteString
getAdminStaticFile path =
    HashMap.lookup path asfList
    where
        asfList :: HashMap Text LBS.ByteString
        asfList =
            HashMap.fromList [ (s fp, s content) | (fp, content) <- files ]
        files = $(embedDir "admin-static")


adminStaticH :: [Text] -> Handler
adminStaticH pathParts = do
    assertMethod GET $ do
        asfConfig <- getsConfig adminAssets
        case asfConfig of
            AdminAssetsDynamic fp -> staticFileH (Text.splitOn "/" (s fp) ++ pathParts)
            AdminAssetsBuiltin -> embeddedFileH pathParts getAdminStaticFile

adminDebugH :: [Text] -> Handler
adminDebugH pathRemainder = do
    cookies <- getCookies
    session <- getCurrentSession
    okTextH $ show cookies ++ "\n" ++ show session

loginH :: Handler
loginH =
    dispatchMethods
        [ (GET, okTemplateH (adminLoginT Nothing []))
        , (POST, loginPOST) -- Login does not require CSRF protection...
        ]
        error405H
    where
        loginPOST = do
            username <- s . fromMaybe "" <$> getPostVar "username"
            password <- fromMaybe "" <$> getPostVar "password"
            authProvider <- appAuth <$> getApp
            useridE <- liftIO $ authCheck authProvider (s username) (s password)
            case useridE of
                Nothing -> do
                    okTemplateH $ adminLoginT (Just username) [ "Invalid username / password" ]
                Just userid -> do
                    sessionLogin (s userid)
                    redirect302 =<< mkAdminURL []

logoutH :: Handler
logoutH =
    dispatchMethods
        [ (GET, requireLogin $ okTemplateH adminLogoutT)
        , (POST, requireFormToken logoutPOST)
        ]
        error405H
    where
        logoutPOST = do
            sessionLogout
            redirect302 =<< mkAdminURL []

-- | Decorate a handler that expects a session such that it is only
-- run if a session exists; otherwise, issue a redirect to the login
-- page.
withLogin :: (Session -> Handler) -> Handler
withLogin = withLoginOrElse $ redirect302 =<< mkAdminURL [ "login" ]

-- | Decorate a plain handler such that it only proceeds if a valid session
-- exists; otherwise, issue a redirect to the login page.
requireLogin :: Handler -> Handler
requireLogin h = withLogin (const h)

requireLoginOrElse :: Handler -> Handler -> Handler
requireLoginOrElse nopeH yesH = withLoginOrElse nopeH (const yesH)

-- | Decorate a plain handler such that when no valid session exists, we'll
-- pretend the requested resource doesn't exist (i.e., raise a 404).
requireLoginStealth :: Handler -> Handler
requireLoginStealth = requireLoginOrElse error404H

-- | Decorate a handler that expects a session such that it is only
-- run if a session exists; otherwise, run the provided failure handler.
withLoginOrElse :: Handler -> (Session -> Handler) -> Handler
withLoginOrElse nopeH yesH = do
    sessionMay <- getCurrentSession
    case sessionMay of
        Nothing -> nopeH
        Just session -> yesH session

-- | requireFormTokenOrElse failure body checks for a valid form token. If the
-- token matches, the body is allowed to proceed, else the failure handler is
-- run.
requireFormTokenOrElse :: Handler -> Handler -> Handler
requireFormTokenOrElse failure body = withLogin $ \session -> do
    let expectedToken = sessionCSRFToken session
    actualToken <- getPostVar "_form_token"
    if Just expectedToken == (s <$> actualToken)
        then body
        else failure

-- | Executes the wrapped handler only if the form token check succeeded;
-- otherwise, serve a generic HTTP 400 (Client Error).
requireFormToken :: Handler -> Handler
requireFormToken = requireFormTokenOrElse error400H

filesH :: Handler
filesH = assertMethod GET $ go
    where
        go = do
            fs <- getFileStore
            files <- liftIO $ listFiles fs
            let filenames = map show files
            okJSONH filenames

fileH :: [Text] -> Handler
fileH path = assertMethod GET $ go
    where
        go = do
            fs <- getFileStore
            case FS.makeFileID path of
                Nothing -> error404H
                Just fileID -> do
                    exists <- liftIO $ FS.fileExists fs fileID
                    if not exists
                        then error404H
                        else do
                            body <- liftIO $ FS.getFile fs fileID
                            let contentType = detectMimeTypeDef "application/octet-stream" (s $ show fileID)
                            okRawH contentType body

thumbH :: Text -> [Text] -> Handler
thumbH geometryStr path = assertMethod GET $ go
    where
        go :: Handler
        go = do
            fs <- getFileStore
            case FS.makeFileID path of
                Nothing -> error404H
                Just fileID -> do
                    exists <- liftIO $ FS.fileExists fs fileID
                    if not exists
                        then error404H
                        else do
                            let extension = map toLower . drop 1 . takeExtension $ show fileID
                            (inExtension, body) <- if extension `elem` [ "gif", "jpg", "png" ]
                                                    then
                                                        (extension ,) <$> (liftIO $ FS.getFile fs fileID)
                                                    else
                                                        return
                                                            ( "png"
                                                            , fromMaybe "" $
                                                                getAdminStaticFile ("img/file-icons/" <> s extension <> ".png") <|>
                                                                getAdminStaticFile "img/file-icons/unknown.png")
                            geometry <- maybeFail
                                            ("failed to parse geometry from '" ++ s geometryStr ++ "'")
                                            (parseGeometry $ s geometryStr)
                            thumbBody <- liftIO $ makeThumbnail inExtension geometry body
                            let contentType = "image/png"
                            okRawH contentType thumbBody

nodeSearchH :: Handler
nodeSearchH = do
    assertMethod GET $ go
    where
        go  = do
            qJSONMay <- fromRequest $ param "qj" . queryString
            qStringMay <- fromRequest $ param "q" . queryString
            case (qJSONMay, qStringMay) of
                (_, Just qString) -> goString qString
                (Just qJSON, Nothing) -> goJSON qJSON
                (Nothing, Nothing) -> nodeListH "All Nodes" def def
        goJSON qJSON = do
            case JSON.eitherDecode qJSON of
                Right q -> nodeListH "Search results" RecordQueryFromJSON q
                Left err -> respondTemplateH status400 (queryJSONErrorT err qJSON)
        goString qString = do
            case RecordQuery.parseQuery qString of
                Right q -> do
                    nodeListH "Search results" RecordQueryFromString q
                Left err ->
                    respondTemplateH status400 (querySyntaxErrorT err qString)

nodeListH :: Text -> RecordQueryOriginFormat -> RecordQuery -> Handler
nodeListH queryName qFormat q =
    assertMethod GET $ go
    where
        go  = do
            page <- fromRequest $ paramDef 1 "page" . queryString
            let pageSize = 10
            storage <- getStorage
            nodes <- liftIO $ select' storage (paginate page pageSize q)
            pageCount <- (`div` pageSize) . (+ (pageSize - 1)) <$> liftIO (count' storage q)
            okTemplateH (nodeListT queryName qFormat q nodes page pageCount)

nodeEditH :: [Text] -> Handler
nodeEditH route = do
    case route of
        [] -> error404H
        "new":[] -> nodeCreateH Nothing
        "new":slug:[] -> nodeCreateH (Just slug)
        recID:[] -> nodeUpdateH recID
        _ -> error404H

nodeCreateH :: Maybe Text -> Handler
nodeCreateH slugMay = do
    dispatchMethods
        [ (GET, nodeCreateGET)
        , (PUT, nodeCreatePUT)
        , (POST, requireFormToken nodeCreatePUT)
        ]
        error405H
    where
        nodeCreateGET = do
            blueprints <- blueprints <$> getConfig
            let fields :: RecordFields
                fields = fromMaybe def $ do
                    slug <- slugMay
                    blueprint <- headMay . filter ((== slug) . blueprintSlug) $ blueprints
                    return . blueprintFields $ blueprint
                record :: Record
                record = def { recordFields = fields }
            action <- mkAdminURL [ "node", "new" ]
            okTemplateH $ nodeFormT action record
        nodeCreatePUT  = do
            storage <- getStorage
            fieldsMay <- getPostVar "node"
            case (fieldsMay >>= YAML.decode . s) :: Maybe RecordFields of
                Just fields -> do
                    rec <- liftIO $ insert storage fields
                    redirect302 . s . recordID $ rec
                Nothing ->
                    error400H

nodeUpdateH :: RecordID -> Handler
nodeUpdateH recID = do
    dispatchMethods
        [ (GET, nodeUpdateGET)
        , (POST, requireFormToken nodeUpdatePOST)
        , (DELETE, requireFormToken nodeUpdateDELETE)
        ]
        error405H
    where
        nodeUpdateGET  = do
            storage <- getStorage
            recMay <- liftIO $ get storage recID
            case recMay of
                Nothing -> error404H
                Just rec -> do
                    okTemplateH (nodeFormT recID rec)
        nodeUpdateDELETE = do
            storage <- getStorage
            liftIO $ delete storage recID
            redirect302 =<< mkAdminURL [ "nodes" ]
        nodeUpdatePOST  = do
            storage <- getStorage
            recMay <- liftIO $ get storage recID
            case recMay of
                Nothing -> error404H
                Just rec -> do
                    fieldsMay <- getPostVar "node"
                    case (fieldsMay >>= YAML.decode . s) :: Maybe RecordFields of
                        Just fields -> do
                            let rec' = rec { recordFields = fields }
                            liftIO $ update storage rec'
                            redirect302 . s . recordID $ rec
                        Nothing ->
                            error400H

pandocH :: String -> String -> Handler
pandocH fromStr toStr = do
    body <- s <$> (maybeFail "no body found" =<< getPostVar "body")
    fromFmt <- maybeFail "invalid input format" . readMay $ fromStr
    toFmt <- maybeFail "invalid output format" . readMay $ toStr
    setResponseHeader "Content-type" $
        case toStr of
            "html" -> "text/html"
            _ -> "text/plain"
    okTextH $ pandoc fromFmt toFmt body
