{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE FlexibleContexts #-}
{-#LANGUAGE FlexibleInstances #-}
{-#LANGUAGE TypeSynonymInstances #-}
module Hubby.Handlers.Common
where

import Data.ByteString (ByteString)
import qualified Data.ByteString.Lazy as LBS
import Data.Text (Text)
import Data.List
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap

import Data.Default
import Data.Maybe
import Data.Monoid
import Text.StringConvert (s, StringConvert)
import System.FilePath
import System.Posix.Files
import Data.CaseInsensitive (CI)
import Data.Token
import qualified Data.Aeson as JSON

import Control.Applicative
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Trans.Except
import Control.Monad.IO.Class

import Network.Wai
import Network.Wai.Parse
import Network.Mime
import Network.HTTP.Types

import Hubby.AppContext
import Hubby.AppConfig
import Hubby.Templates.Type hiding (getSession)
import Hubby.Storage.Type
import Hubby.FileStore.Type
import Hubby.Cookies
import Hubby.SessionProvider
import Hubby.Session

type ContentType = ByteString

data HandlerContext =
    HandlerContext
        { hctxAppContext :: AppContext
        , hctxRequest :: Request
        , hctxRespond :: (Response -> IO ResponseReceived)
        }

data HandlerState =
    HandlerState
        { hstResponseHeaders :: [(CI ByteString, ByteString)]
        , hstPost :: Maybe ([(ByteString, ByteString)], [File LBS.ByteString])
        }

instance Default HandlerState where
    def = HandlerState [] Nothing

type HandlerM = StateT HandlerState (ReaderT HandlerContext IO)
type Handler = HandlerM ResponseReceived

runHandler :: Handler
           -> AppContext
           -> Request
           -> (Response -> IO ResponseReceived)
           -> IO ResponseReceived
runHandler h app rq respondCont =
    runReaderT (evalStateT h def) (HandlerContext app rq respondCont)

respond :: Response -> Handler
respond r = do
    respondCont <- asks hctxRespond
    liftIO $ respondCont r

setResponseHeader :: CI ByteString -> ByteString -> HandlerM ()
setResponseHeader name value = modify f
    where
        f :: HandlerState -> HandlerState
        f state = state { hstResponseHeaders = (name,value):hstResponseHeaders state }

getResponseHeaders :: HandlerM [(CI ByteString, ByteString)]
getResponseHeaders = gets hstResponseHeaders

setCookie :: Cookie -> HandlerM ()
setCookie cookie = do
    setResponseHeader "Set-Cookie" $ cookieToValue cookie

getRequest :: HandlerM Request
getRequest = asks hctxRequest

loadPost :: HandlerM ([(ByteString, ByteString)], [File LBS.ByteString])
loadPost = do
    postVars <- getRequest >>= liftIO . parseRequestBody lbsBackEnd
    modify $ \s -> s { hstPost = Just postVars }
    return postVars

getPost :: HandlerM ([(ByteString, ByteString)], [File LBS.ByteString])
getPost = do
    postVarsMay <- gets hstPost
    case postVarsMay of
        Just postVars -> return postVars
        Nothing -> loadPost

getPostVars :: HandlerM [(ByteString, ByteString)]
getPostVars = fst <$> getPost

getPostedFiles :: HandlerM [File LBS.ByteString]
getPostedFiles = snd <$> getPost

getPostVar :: ByteString -> HandlerM (Maybe ByteString)
getPostVar key = lookup key <$> getPostVars

sessionLogin :: Text -> HandlerM ()
sessionLogin userID = do
    let alphabet = ['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9'] ++ "_"
    csrfToken <- liftIO $ mkTokenIO alphabet 16
    let session = Session (Just userID) (s csrfToken)
    sessionProvider <- appSessions <$> getApp
    sessionKey <- liftIO $ newSession sessionProvider session
    either fail setCookie (makeDefCookie "SESSION_ID" . s . unSessionKey $ sessionKey)

sessionLogout :: HandlerM ()
sessionLogout = do
    sessionProvider <- appSessions <$> getApp
    sessionKeyMay <- getCurrentSessionID
    case sessionKeyMay of
        Just sessionKey -> liftIO $ deleteSession sessionProvider sessionKey
        Nothing -> return ()

getCurrentSessionID :: HandlerM (Maybe SessionKey)
getCurrentSessionID = do
    sessionProvider <- appSessions <$> getApp
    cookies <- getCookies
    return $ SessionKey . s <$> lookup "SESSION_ID" cookies

getCurrentSession :: HandlerM (Maybe Session)
getCurrentSession = do
    sessionKeyMay <- getCurrentSessionID
    case sessionKeyMay of
        Nothing -> return Nothing
        Just sessionKey -> do
            sessionProvider <- appSessions <$> getApp
            liftIO $ getSession sessionProvider sessionKey

getRequestBodyLBS :: HandlerM ([Param], [File LBS.ByteString])
getRequestBodyLBS = do
    rq <- getRequest
    liftIO $ parseRequestBody lbsBackEnd rq

getRequestHeaders :: HandlerM [(CI ByteString, ByteString)]
getRequestHeaders = requestHeaders <$> getRequest

getRequestHeader :: CI ByteString -> HandlerM (Maybe ByteString)
getRequestHeader h = lookup h <$> getRequestHeaders

getCookiesRaw :: HandlerM (Maybe ByteString)
getCookiesRaw = getRequestHeader "Cookie"

getCookies :: HandlerM [(ByteString, ByteString)]
getCookies = do
    rawMay <- getCookiesRaw
    case rawMay of
        Nothing -> return []
        Just raw -> return $ parseCookies raw

getApp :: HandlerM AppContext
getApp = asks hctxAppContext

instance MonadConfig HandlerM where
    getConfig = appConfig <$> getApp

getStorage :: HandlerM Storage
getStorage = appStorage <$> getApp

getFileStore :: HandlerM FileStore
getFileStore = appFileStore <$> getApp

getSessionProvider :: HandlerM (SessionProvider Session)
getSessionProvider = appSessions <$> getApp

fromRequest :: (Request -> a) -> HandlerM a
fromRequest f = f <$> getRequest

assertMethod :: StdMethod -> Handler -> Handler
assertMethod m matchedH = matchMethod m matchedH error405H

matchMethod :: StdMethod -> Handler -> Handler -> Handler
matchMethod m matchedH unmatchedH = do
    actualMethod <- fromRequest $ parseMethod . requestMethod
    if actualMethod == Right m
        then matchedH
        else unmatchedH

dispatchMethods :: [(StdMethod, Handler)] -> Handler -> Handler
dispatchMethods matchedHs unmatchedH = do
    actualMethod <- fromRequest $ parseMethod . requestMethod
    case actualMethod of
        Right am -> fromMaybe unmatchedH (lookup am matchedHs)
        Left _ -> unmatchedH

okTemplateH :: Template -> Handler
okTemplateH = respondTemplateH status200

respondTemplateH :: Status -> Template -> Handler
respondTemplateH status dom = do
    s <- getCurrentSession
    rawHtml <- flip (runTemplate' s) dom <$> getConfig
    respondHtmlH status rawHtml

okHtmlH :: StringConvert t LBS.ByteString => t -> Handler
okHtmlH = respondHtmlH status200

okTextH :: StringConvert t LBS.ByteString => t -> Handler
okTextH = respondTextH status200

okJSONH :: JSON.ToJSON a => a -> Handler
okJSONH = respondJSONH status200

okRawH :: ByteString -> LBS.ByteString -> Handler
okRawH contentType body = respondH status200 contentType body

error400H :: Handler
error400H =
    respondTextH status400 ("Client error" :: ByteString)

error404H :: Handler
error404H = do
    path <- fromRequest rawPathInfo
    respondTextH status404 ("Not found: " <> path)

error405H :: Handler
error405H = do
    method <- fromRequest requestMethod
    path<- fromRequest rawPathInfo
    respondTextH status405 ("Method not allowed: " <> method <> " " <> path)

redirect302 :: ByteString -> Handler
redirect302 url = do
    setResponseHeader "Location" url
    respondTextH status302 ("You need to go here: " <> url)

respondTextH :: StringConvert t LBS.ByteString => Status -> t -> Handler
respondTextH status body = respondH status "text/plain;charset=utf8" (s body)

respondHtmlH :: StringConvert t LBS.ByteString => Status -> t -> Handler
respondHtmlH status body = respondH status "text/html;charset=utf8" (s body)

respondJSONH :: JSON.ToJSON a => Status -> a -> Handler
respondJSONH status = do
    respondH status "application/json" . JSON.encode

respondH :: Status -> ByteString -> LBS.ByteString -> Handler
respondH status contentType body = do
    setResponseHeader "Content-type" contentType
    setResponseHeader "X-Powered-By" "Hubby"
    headers <- getResponseHeaders
    respond $ responseLBS status headers body


embeddedFileH :: [Text] -> (Text -> Maybe LBS.ByteString) -> Handler
embeddedFileH tpath mapping = do
    let fp = makePathX tpath
        contentMay = mapping (s fp)
    case contentMay of
        Nothing -> error404H
        Just content -> respond $
            responseLBS
                status200
                    [("Content-type", defaultMimeLookup (s fp))]
                    content

staticFileH :: [Text] -> Handler
staticFileH tpath = do
    let fp = makePath tpath
    exists <- liftIO $ fileExist fp
    if not exists
        then error404H
        else respond $
            responseFile
                status200
                [("Content-type", defaultMimeLookup (s fp))]
                fp
                Nothing

makePath :: [Text] -> FilePath
-- kludge to allow absolute paths
makePath ("":xs) = "/" <> (foldl' (</>) "" . filter isValidPathPart . map s $ xs)
makePath xs = foldl' (</>) "." . filter isValidPathPart . map s $ xs

makePathX :: [Text] -> FilePath
makePathX xs =
    if null xs' then "" else foldl1' (</>) xs'
    where
        xs' :: [FilePath]
        xs' = filter isValidPathPart . map s $ xs

isValidPathPart :: FilePath -> Bool
isValidPathPart "" = False
isValidPathPart ('.':_) = False
isValidPathPart xs = all (`elem` ['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9'] ++ ".-_ +") xs
