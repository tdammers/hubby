module Hubby.Pandoc
( PandocReaderFormat (..)
, PandocWriterFormat (..)
, pandoc
)
where

import Text.Pandoc as Pandoc
import Text.Pandoc.Walk
import qualified Data.ByteString.Lazy as LBS
import Text.StringConvert
import Control.Applicative
import Data.MaybeFail
import Data.Default
import qualified Data.Set as Set

readerOptions :: ReaderOptions
readerOptions = def
    { readerExtensions =
        Set.delete Ext_raw_html .
        Set.delete Ext_literate_haskell .
        Set.fromList $ [minBound..maxBound]
    }

data PandocReaderFormat =
    ReadMarkdown |
    ReadTextile |
    ReadRST |
    ReadMediaWiki |
    ReadLaTeX |
    ReadHtml |
    ReadDocBook |
    ReadOPML |
    ReadHaddock

instance Read PandocReaderFormat where
    readsPrec _ "markdown" = [(ReadMarkdown, "")]
    readsPrec _ "textile" = [(ReadTextile, "")]
    readsPrec _ "rst" = [(ReadRST, "")]
    readsPrec _ "mediawiki" = [(ReadMediaWiki, "")]
    readsPrec _ "latex" = [(ReadLaTeX, "")]
    readsPrec _ "html" = [(ReadHtml, "")]
    readsPrec _ "docbook" = [(ReadDocBook, "")]
    readsPrec _ "opml" = [(ReadOPML, "")]
    readsPrec _ "haddock" = [(ReadHaddock, "")]
    readsPrec _ _ = []

instance Show PandocReaderFormat where
    show ReadMarkdown = "markdown"
    show ReadTextile = "textile"
    show ReadRST = "rst"
    show ReadMediaWiki = "mediawiki"
    show ReadLaTeX = "latex"
    show ReadHtml = "html"
    show ReadDocBook = "docbook"
    show ReadOPML = "opml"
    show ReadHaddock = "haddock"

pandocReader :: PandocReaderFormat -> String -> Pandoc
pandocReader ReadMarkdown = readMarkdown readerOptions
pandocReader ReadTextile = readTextile readerOptions
pandocReader ReadRST = readRST readerOptions
pandocReader ReadMediaWiki = readMediaWiki readerOptions
pandocReader ReadLaTeX = readLaTeX readerOptions
pandocReader ReadHtml = readHtml readerOptions
pandocReader ReadDocBook = readDocBook readerOptions
pandocReader ReadOPML = readOPML readerOptions
pandocReader ReadHaddock = readHaddock readerOptions

data PandocWriterFormat =
    WriteMarkdown |
    WritePlain |
    WriteTextile |
    WriteRST |
    WriteMediaWiki |
    WriteLaTeX |
    WriteHtml |
    WriteDocBook |
    WriteOPML |
    WriteHaddock

instance Read PandocWriterFormat where
    readsPrec _ "markdown" = [(WriteMarkdown, "")]
    readsPrec _ "plain" = [(WritePlain, "")]
    readsPrec _ "textile" = [(WriteTextile, "")]
    readsPrec _ "rst" = [(WriteRST, "")]
    readsPrec _ "mediawiki" = [(WriteMediaWiki, "")]
    readsPrec _ "latex" = [(WriteLaTeX, "")]
    readsPrec _ "html" = [(WriteHtml, "")]
    readsPrec _ "docbook" = [(WriteDocBook, "")]
    readsPrec _ "opml" = [(WriteOPML, "")]
    readsPrec _ "haddock" = [(WriteHaddock, "")]
    readsPrec _ _ = []

instance Show PandocWriterFormat where
    show WriteMarkdown = "markdown"
    show WritePlain = "plain"
    show WriteTextile = "textile"
    show WriteRST = "rst"
    show WriteMediaWiki = "mediawiki"
    show WriteLaTeX = "latex"
    show WriteHtml = "html"
    show WriteDocBook = "docbook"
    show WriteOPML = "opml"
    show WriteHaddock = "haddock"

pandocWriter :: PandocWriterFormat -> WriterOptions -> Pandoc -> String
pandocWriter WriteMarkdown = writeMarkdown
pandocWriter WritePlain = writePlain
pandocWriter WriteTextile = writeTextile
pandocWriter WriteRST = writeRST
pandocWriter WriteMediaWiki = writeMediaWiki
pandocWriter WriteLaTeX = writeLaTeX
pandocWriter WriteHtml = writeHtmlString
pandocWriter WriteDocBook = writeDocbook
pandocWriter WriteOPML = writeOPML
pandocWriter WriteHaddock = writeHaddock

pandoc :: PandocReaderFormat -> PandocWriterFormat -> String -> String
pandoc readerFormat writerFormat body =
    let reader = pandocReader readerFormat
        writer = pandocWriter writerFormat
        writerOptions = def { writerEmailObfuscation = NoObfuscation }
    in writer writerOptions . safePandoc . reader $ body

safeInline :: Inline -> Inline
safeInline (RawInline _ s) = Code nullAttr s
safeInline x = x

safeBlock :: Block -> Block
safeBlock (RawBlock _ s) = CodeBlock nullAttr s
safeBlock x = x

safePandoc :: Pandoc -> Pandoc
safePandoc = walk safeInline . walk safeBlock
