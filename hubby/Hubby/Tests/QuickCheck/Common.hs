module Hubby.Tests.QuickCheck.Common
where

import Test.QuickCheck

data SomeTestable =
    SomeTestable
        { someProperty :: Property
        , someExhaustive :: Bool
        }

instance Testable SomeTestable where
    property = someProperty
    exhaustive = someExhaustive

wrapTestable :: Testable a => a -> SomeTestable
wrapTestable p = SomeTestable (property p) (exhaustive p)

data Test =
    Test
        { testName :: String
        , testProp :: SomeTestable
        }

mkTest :: Testable a => String -> a -> Test
mkTest name prop = Test name (wrapTestable prop)
