{-#LANGUAGE OverloadedStrings #-}
module Hubby.Tests.QuickCheck.Model.RecordQuery
( tests
)
where

import Test.QuickCheck
import Hubby.Record
import Hubby.RecordQuery
import Hubby.RecordQuery.Parse
import Hubby.RecordQuery.Print
import Hubby.RecordQuery.JSON
import Text.StringConvert
import Control.Applicative
import Data.Aeson as JSON
import qualified Data.Vector as Vector
import Hubby.Tests.QuickCheck.Common
import Data.Monoid

isRight :: Either a b -> Bool
isRight (Left _) = False
isRight (Right _) = True

-- tests :: [Property]
tests =
    [ mkTest
        "RecordQuery: round-trip through string (parse . print)"
        prop_roundTripString
    , mkTest
        "RecordQuery: accept query strings as JSON fragments"
        prop_stringAsJSON
    , mkTest
        "RecordQuery.Limit JSON round-tripping"
        prop_roundTripLimitJSON
    , mkTest
        "RecordQuery.Order JSON round-tripping"
        prop_roundTripOrderJSON
    , mkTest
        "RecordQuery.Match JSON round-tripping"
        prop_roundTripMatchJSON
    , mkTest
        "RecordQuery: round-trip through JSON (decode . encode)"
        prop_roundTripJSON
    ]


prop_roundTripString :: RecordQuery -> Bool
prop_roundTripString q =
    let result = (parseQuery . s . printQuery $ q)
        Right q' = result
    in isRight result && q == q'

prop_stringAsJSON :: RecordQuery -> Bool
prop_stringAsJSON q =
    let printed = "[" <> JSON.encode (printQuery q) <> "]"
        decoded = JSON.decode $ printed :: Maybe [RecordQuery]
    in decoded == Just [q]

prop_roundTripLimitJSON :: Limit -> Bool
prop_roundTripLimitJSON x =
    let result = JSON.decode . JSON.encode $ [x]
    in result == Just [x]

prop_roundTripOrderJSON :: Order -> Bool
prop_roundTripOrderJSON x =
    let result = JSON.decode . JSON.encode $ [x]
    in result == Just [x]

prop_roundTripMatchJSON :: Match -> Bool
prop_roundTripMatchJSON x =
    let result = JSON.decode . JSON.encode $ [x]
    in result == Just [x]

prop_roundTripJSON :: RecordQuery -> Bool
prop_roundTripJSON q =
    let result = JSON.decode . JSON.encode $ q
    in result == Just q


arbitraryIdentifier = s <$> arbitraryIdentifierString
arbitraryIdentifierString =
    (:) <$> arbitraryLetter <*> (listOf arbitraryIdentifierChar)

arbitraryLetter = oneof . map return $ ['a'..'z'] ++ ['A'..'Z']
arbitraryIdentifierChar = oneof . map return $ [ 'a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9'] ++ ['_']

arbitraryScalar :: Gen Value
arbitraryScalar = oneof
    [ String . s <$> (arbitrary :: Gen String)
    , Number . fromIntegral <$> (arbitrary :: Gen Int)
    , Bool <$> arbitrary
    , return Null
    ]

arbitraryValue = frequency
    [ (4, arbitraryScalar)
    , (1, Array . Vector.fromList <$> listOf arbitraryScalar)
    -- TODO: Object
    ]

instance Arbitrary Operand where
    arbitrary = oneof
        [ LiteralOperand <$> arbitraryValue
        , FieldOperand <$> arbitraryIdentifier
        , ParamOperand <$> arbitraryIdentifier
        ]

instance Arbitrary BinaryOperator where
    arbitrary = arbitraryBoundedEnum

instance Arbitrary TernaryOperator where
    arbitrary = arbitraryBoundedEnum

instance Arbitrary Match where
    arbitrary = oneof
        [ return MatchAll
        , return MatchNone
        , MatchID <$> arbitraryIdentifier
        , MatchBinary <$> arbitrary <*> arbitrary <*> arbitrary
        , MatchTernary <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
        , MatchEither <$> arbitrary <*> arbitrary
        , MatchBoth <$> arbitrary <*> arbitrary
        , MatchNot <$> arbitrary
        ] `suchThat` ((<= 3) . matchDepth)

matchDepth :: Match -> Int
matchDepth (MatchEither a b) = 1 + max (matchDepth a) (matchDepth b)
matchDepth (MatchBoth a b) = 1 + max (matchDepth a) (matchDepth b)
matchDepth (MatchNot a) = 1 + matchDepth a
matchDepth _ = 1

instance Arbitrary OrderAscDesc where
    arbitrary = arbitraryBoundedEnum

instance Arbitrary OrderTarget where
    arbitrary = oneof
        [ return OrderID
        , OrderFieldNum <$> arbitraryIdentifier
        , OrderFieldAlpha <$> arbitraryIdentifier
        ]

instance Arbitrary Order where
    arbitrary = Order <$> arbitrary <*> arbitrary

instance Arbitrary Limit where
    arbitrary = oneof
        [ return Unlimited -- | Unlimited
        , LimitCount <$> arbitrary
        , LimitOffsetCount <$> arbitrary <*> arbitrary
        ]

instance Arbitrary RecordQuery where
    arbitrary = RecordQuery <$> arbitrary <*> arbitrary <*> arbitrary
