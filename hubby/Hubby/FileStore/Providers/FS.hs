{-#LANGUAGE TemplateHaskell #-}
module Hubby.FileStore.Providers.FS
( createFSStore
, findFilesRecursively
)
where

import Hubby.FileStore.Type
import System.FilePath
import System.Directory
import System.IO
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.ByteString.Lazy as LBS
import Text.StringConvert
import Control.Monad
import Control.Applicative
import Data.Default
import Data.List

import Data.Aeson as JSON
import Data.Aeson.TH as JSON
import Data.Aeson.DeriveUtils
import Data.Aeson.ParseUtils

data FSStoreConfig =
    FSStoreConfig
        { fsconfBaseDir :: FilePath
        }
$(deriveJSON
    defaultOptions { fieldLabelModifier = camelToDashed '-' . drop (length ("fsconf" :: String)) }
    ''FSStoreConfig)

instance Default FSStoreConfig where
    def = FSStoreConfig "files"

createFSStore :: Value -> IO FileStore
createFSStore confJSON = do
    conf <- case fromJSONWithDefaults def confJSON of
                Error x -> fail x
                Success x -> return x
    let dir = fsconfBaseDir conf
    return $
        FileStore
            { fileExists = fsFileExists dir
            , putFile = fsPutFile dir
            , getFile = fsGetFile dir
            , deleteFile = fsDeleteFile dir
            , listFiles = fsListFiles dir
            }

fsFileExists :: FilePath -> FileID -> IO Bool
fsFileExists basedir fid = doesFileExist (basedir </> show fid)

fsPutFile :: FilePath -> FileID -> LBS.ByteString -> IO ()
fsPutFile basedir fid = LBS.writeFile (basedir </> show fid)

fsGetFile :: FilePath -> FileID -> IO LBS.ByteString
fsGetFile basedir fid = LBS.readFile (basedir </> show fid)

fsDeleteFile :: FilePath -> FileID -> IO ()
fsDeleteFile basedir fid =
    fail "Deleting files currently unsupported"
    -- unlink (basedir </> show fid)

fsListFiles :: FilePath -> IO [FileID]
fsListFiles basedir = findFilesRecursively basedir ""

findFilesRecursively :: FilePath -> FilePath -> IO [FileID]
findFilesRecursively basedir dir = do
    isDir <- doesDirectoryExist (basedir </> dir)
    if isDir
        then do
            -- it's a directory: list files and process recursively
            filenames <- filter (not . ("." `isPrefixOf`)) <$> getDirectoryContents (basedir </> dir)
            concat <$> forM filenames (findFilesRecursively basedir . (dir </>))
        else do
            exists <- doesFileExist (basedir </> dir)
            if exists
                then
                    -- it's a file!
                    return [read dir]
                else
                    return []
