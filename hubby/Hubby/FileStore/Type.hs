{-#LANGUAGE OverloadedStrings #-}
module Hubby.FileStore.Type
( FileID
, appendFileID
, makeFileID
, isInDir
, FileStore (..)
)
where

import Data.Text (Text)
import qualified Data.Text as Text
import Text.StringConvert
import qualified Data.ByteString.Lazy as LBS
import Data.Monoid
import Control.Applicative

data FileID = FileName Text | FileSubdir Text FileID
    deriving (Eq, Ord)

instance Show FileID where
    show = s . collapseFileID

collapseFileID :: FileID -> Text
collapseFileID (FileName fn) = escapeFileName fn
collapseFileID (FileSubdir dn sub) = escapeFileName dn <> "/" <> collapseFileID sub

makeFileID :: [Text] -> Maybe FileID
makeFileID [] = Nothing
makeFileID (x:[]) = Just $ FileName x
makeFileID (x:xs) = FileSubdir x <$> makeFileID xs

instance Read FileID where
    readsPrec _ = readFileID

appendFileID :: FileID -> FileID -> FileID
appendFileID (FileSubdir p sub) b = FileSubdir p (appendFileID sub b)
appendFileID (FileName n) b = FileSubdir n b

isInDir :: FileID -> FileID -> Bool
isInDir (FileSubdir a b) (FileSubdir c d)
    | a == c = isInDir b d
    | otherwise = False
isInDir (FileSubdir a (FileName b)) (FileName c) = a == c
isInDir _ _ = False

readFileID :: String -> [(FileID, String)]
readFileID str =
    case go parts of
        Nothing -> []
        Just fid -> [(fid, "")]
    where
        parts = Text.splitOn "/" $ s str
        go :: [Text] -> Maybe FileID
        go [] = Nothing
        go ("":xs) = go xs
        go (x:[]) = return $ FileName x
        go (x:xs) = do
            sub <- go xs
            return $ FileSubdir x sub

escapeFileName :: Text -> Text
escapeFileName = Text.replace "/" "\\/"

unescapeFileName :: Text -> Text
unescapeFileName = Text.replace "\\/" "/"

data FileStore =
    FileStore
        { fileExists :: FileID -> IO Bool
        , putFile :: FileID -> LBS.ByteString -> IO ()
        , getFile :: FileID -> IO LBS.ByteString
        , deleteFile :: FileID -> IO ()
        , listFiles :: IO [FileID]
        }
