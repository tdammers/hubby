{-#LANGUAGE OverloadedStrings #-}
module Hubby.Authentication
( createAuthProvider
, module Hubby.Authentication.Provider
)
where

import Hubby.Authentication.Provider
import Hubby.Authentication.Providers.FakeAuth
import Hubby.Authentication.Providers.FileBased
import Data.Aeson as JSON

-- | Factory function for authentication providers.
createAuthProvider :: String -> JSON.Value -> IO (Either String AuthProvider)
createAuthProvider "default" conf = createAuthProvider "file" conf
createAuthProvider "fake" conf = createFakeAuthProvider conf
createAuthProvider "file" conf = createFileBasedProvider conf
createAuthProvider name _ = return $ Left ("Invalid authentication provider: " ++ show name)
