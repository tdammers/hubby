module Hubby.Storage
( module Hubby.Storage.Type
, createStorage
, defStorageConfig
)
where

import Hubby.Storage.Type
import Hubby.Storage.Providers.InProcess
import Data.Aeson (Value, object, toJSON)
import Data.Default

-- | Factory method to create a Storage object by name.
createStorage :: String -> Value -> IO Storage
createStorage "in-process" conf = createInProcessStorage conf
createStorage _ _ = return blackHoleStorage

-- | Factory method to create a default storage configuration value
defStorageConfig :: String -> Value
defStorageConfig "in-process" = toJSON (def :: InProcessStorageConfig)
defStorageConfig _ = object []
