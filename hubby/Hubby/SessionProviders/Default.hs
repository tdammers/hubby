{-#LANGUAGE TemplateHaskell #-}
{-#LANGUAGE OverloadedStrings #-}
module Hubby.SessionProviders.Default
( mkDefaultSessionProvider
)
where

import Hubby.Session
import Hubby.SessionProviders.Type

import Control.Concurrent.STM
import System.Lock.FLock
import System.IO
import System.IO.Error
import Control.Applicative

import Data.Default
import Data.Maybe

import Data.Aeson as JSON
import Data.Aeson.TH as JSON
import Data.Aeson.DeriveUtils
import Data.Aeson.ParseUtils

import qualified Data.ByteString.Lazy as LBS
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import qualified Data.ByteString as BS

data DefaultSessionProviderConfig =
    DefaultSessionProviderConfig
        { defspcDatabaseDirname :: FilePath
        }
$(deriveJSON
    defaultOptions { fieldLabelModifier = camelToDashed '-' . drop (length ("defspc" :: String)) }
    ''DefaultSessionProviderConfig)

instance Default DefaultSessionProviderConfig where
    def = DefaultSessionProviderConfig "sessions"

type Sessions = HashMap SessionKey Session

mkDefaultSessionProvider :: JSON.Value -> IO (SessionProvider Session)
mkDefaultSessionProvider confJSON = do
    let configE = fromJSONWithDefaults def confJSON
    config <- case configE of
        Error x -> fail x
        Success x -> return x
    let dbFilename = defspcDatabaseDirname config
    sessions <- loadSessions dbFilename
    db <- atomically $ newTVar sessions
    return $ SessionProvider
                { putSession = _putSession db dbFilename
                , deleteSession = _deleteSession db dbFilename
                , moveSession = _moveSession db dbFilename
                , getSession = _getSession db
                , cleanupSessions = return ()
                , touchSession = \_ -> return ()
                }

_putSession :: TVar Sessions -> FilePath -> Session -> SessionKey -> IO ()
_putSession db fp sess key = persisted db fp $ HashMap.insert key sess

_deleteSession :: TVar Sessions -> FilePath -> SessionKey -> IO ()
_deleteSession db fp key = persisted db fp $ HashMap.delete key

_moveSession :: TVar Sessions -> FilePath -> SessionKey -> SessionKey -> IO ()
_moveSession db fp k0 k1 = persisted db fp $ \db ->
    let sMay = HashMap.lookup k0 db
    in case sMay of
        Nothing -> db
        Just s -> (HashMap.delete k0 . HashMap.insert k1 s) db

_getSession :: TVar Sessions -> SessionKey -> IO (Maybe Session)
_getSession db key = do
    sessions <- atomically $ readTVar db
    return $ HashMap.lookup key sessions

persisted :: TVar Sessions -> FilePath -> (Sessions -> Sessions) -> IO ()
persisted db fp f =
    withLock fp Exclusive Block $ do
        dbVal <- atomically $ modifyTVar db f >> readTVar db
        persist fp dbVal

persist :: FilePath -> Sessions -> IO ()
persist fn dbData = do
    BS.writeFile fn (LBS.toStrict . JSON.encode . HashMap.toList $ dbData)

loadSessions :: FilePath -> IO Sessions
loadSessions fn = do -- return HashMap.empty
   dbJSON <- catchIOError
                (BS.readFile fn)
                (const $ return "[]")
   let dbData = fromMaybe HashMap.empty $ HashMap.fromList <$> JSON.decode' (LBS.fromStrict dbJSON)
   return dbData
