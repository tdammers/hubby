{-#LANGUAGE GeneralizedNewtypeDeriving #-}
module Hubby.SessionProviders.Type
where

import Data.Text (Text)
import Data.Hashable
import Data.Aeson

newtype SessionKey = SessionKey { unSessionKey :: Text }
    deriving (Show, Read, Eq, Ord, Hashable, ToJSON, FromJSON)

data SessionProvider a =
    SessionProvider
        { putSession :: a -> SessionKey -> IO ()
        , deleteSession :: SessionKey -> IO ()
        , moveSession :: SessionKey -> SessionKey -> IO ()
        , getSession :: SessionKey -> IO (Maybe a)
        , touchSession :: SessionKey -> IO ()
        , cleanupSessions :: IO ()
        }


