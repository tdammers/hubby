{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE TemplateHaskell #-}
module Hubby.Record
where

import Control.Applicative

import Data.Default
import Data.Maybe
import Text.StringConvert
import Data.Monoid

import Data.Text (Text)
import qualified Data.Text.Lazy as LText
import qualified Data.HashMap.Strict as HashMap
import Data.HashMap.Strict (HashMap)
import Data.List (intersperse)
import Data.Scientific

import Data.Aeson as JSON
import Data.Yaml as YAML
import Data.Aeson.DeriveUtils

import Safe

type FieldName = Text
type RecordID = Text

instance ToString Value where
    toString (String t) = toString t
    toString (Number n) = show n
    toString (Bool True) = "true"
    toString (Bool False) = ""
    toString Null = ""
    toString v = toString (YAML.encode v) -- for now...

instance FromString Value where
    fromString = String . fromString

numericValue :: Value -> Scientific
numericValue (String t) = fromMaybe 0 . readMay . s $ t
numericValue (Number n) = n
numericValue (Bool True) = 1
numericValue _ = 0

data RecordField =
    RecordField
        { fieldName :: Maybe Text
        , fieldType :: Maybe Text
        , fieldValue :: Value
        }
        deriving (Show)

instance Default RecordField where
    def = RecordField Nothing Nothing Null

instance ToJSON RecordField where
    toJSON (RecordField Nothing Nothing v) = v
    toJSON (RecordField (Just name) Nothing v) =
        Object $ HashMap.fromList
                    [ ("_name", String name)
                    , ("_value", v)
                    ]
    toJSON (RecordField (Just name) (Just tag) v) =
        Object $ HashMap.fromList
                    [ ("_name", String name)
                    , ("_type", String tag)
                    , ("_value", v)
                    ]
    toJSON (RecordField Nothing (Just tag) v) =
        Object $ HashMap.fromList
                    [ ("_type", String tag)
                    , ("_value", v)
                    ]

effectiveFieldType :: RecordField -> Text
effectiveFieldType (RecordField { fieldType = Just t }) = t
effectiveFieldType f =
    case fieldValue f of
        JSON.Null -> "null"
        JSON.String _ -> "string"
        JSON.Number _ -> "number"
        JSON.Bool _ -> "bool"
        JSON.Array _ -> "list"
        JSON.Object _ -> "json"

instance FromJSON RecordField where
    parseJSON v@(Object obj) = do
        name <- obj .:? "_name"
        tag <- obj .:? "_type"
        valueMay <- obj .:? "_value"
        if isNothing name && isNothing tag && isNothing valueMay
            then
                return $ RecordField Nothing Nothing v
            else
                return $ RecordField name tag (fromMaybe Null valueMay)
    parseJSON v = return $ RecordField Nothing Nothing v

newtype RecordFields = RecordFields { unRecordFields :: [RecordField] }
    deriving (Show)

instance ToJSON RecordFields where
    toJSON = toJSON . unRecordFields

instance FromJSON RecordFields where
    parseJSON value = RecordFields <$> parseJSON value

instance Default RecordFields where
    def = RecordFields []

data RecordTableColumn =
    RecordTableColumn
        { columnName :: Maybe Text
        , columnType :: Maybe Text
        }
        deriving (Show)

instance ToJSON RecordTableColumn where
    toJSON (RecordTableColumn Nothing Nothing) =
        Object $ HashMap.empty
    toJSON (RecordTableColumn (Just name) Nothing) =
        Object $ HashMap.fromList
                    [ ("_name", String name)
                    ]
    toJSON (RecordTableColumn (Just name) (Just tag)) =
        Object $ HashMap.fromList
                    [ ("_name", String name)
                    , ("_type", String tag)
                    ]
    toJSON (RecordTableColumn Nothing (Just tag)) =
        Object $ HashMap.fromList
                    [ ("_type", String tag)
                    ]

instance FromJSON RecordTableColumn where
    parseJSON (Object obj) =
        RecordTableColumn <$> (obj .:? "name") <*> (obj .:? "type")
    parseJSON (String t) =
        return $ RecordTableColumn (Just t) Nothing
    parseJSON _ =
        return def

instance Default RecordTableColumn where
    def = RecordTableColumn Nothing Nothing

data RecordTable =
    RecordTable
        { rtableColumns :: [RecordTableColumn]
        , rtableRows :: [[Value]]
        }
        deriving (Show)
$(deriveLensJSON ''RecordTable)

instance Default RecordTable where
    def = RecordTable [] []

-- | Extract a table row and convert it to a 'RecordFields' value.
-- This has the effect of converting raw cell values into tagged 'RecordField'
-- values, according to the corresponding column specification.
recordTableRowAt :: RecordTable -> Int -> Maybe RecordFields
recordTableRowAt table@(RecordTable columns rows) ix = do
    row <- rows `atMay` ix
    return $ recordTableRow table row

recordTableRow :: RecordTable -> [Value] -> RecordFields
recordTableRow (RecordTable columns _) rowRaw =
    RecordFields $ zipWith combine columns rowRaw
    where
        combine :: RecordTableColumn -> Value -> RecordField
        combine (RecordTableColumn n t) v = RecordField n t v

data Record =
    Record
        { recordID :: RecordID
        , recordFields :: RecordFields
        }
        deriving (Show)

instance Default Record where
    def = Record "0" def

appendField :: RecordField -> RecordFields -> RecordFields
appendField field (RecordFields xs) = RecordFields (xs ++ [field])

prependField :: RecordField -> RecordFields -> RecordFields
prependField field (RecordFields xs) = RecordFields (field:xs)

appendRecordField :: RecordField -> Record -> Record
appendRecordField field = withRecordFields $ appendField field

prependRecordField :: RecordField -> Record -> Record
prependRecordField field = withRecordFields $ prependField field

withRecordFields :: (RecordFields -> RecordFields) -> Record -> Record
withRecordFields f record = record { recordFields = f (recordFields record) }

lookupFieldsBy :: (RecordField -> Bool) -> RecordFields -> [RecordField]
lookupFieldsBy f (RecordFields xs) = filter f xs

lookupFieldBy :: (RecordField -> Bool) -> RecordFields -> Maybe RecordField
lookupFieldBy isMatch fields =
    case lookupFieldsBy isMatch fields of
        [] -> Nothing
        x:_ -> Just x

lookupField :: FieldName -> RecordFields -> Maybe RecordField
lookupField n = lookupFieldBy ((== Just n) . fieldName)

recordTitle :: Record -> Maybe Value
recordTitle rec =
    s . fieldValue <$> lookupField "title" fields
    where
        fields = recordFields rec

recordTeaser :: Integral i => i -> Record -> LText.Text
recordTeaser len rec =
    if LText.length t == l
        then LText.take (l - 1) t <> "…"
        else t
    where
        l = fromIntegral len
        usefulFields :: [LText.Text]
        usefulFields = map (s . fieldValue) . lookupFieldsBy ((/= Just "title") . fieldName) . recordFields $ rec
        t = LText.take l . LText.concat . intersperse " " $ usefulFields

$(deriveLensJSON ''Record)
