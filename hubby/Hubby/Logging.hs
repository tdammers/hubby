module Hubby.Logging
( writeLog
, warn
)
where

import System.IO

writeLog :: String -> IO ()
writeLog = hPutStrLn stderr

warn :: String -> IO ()
warn = writeLog . ("Warning: " ++)
