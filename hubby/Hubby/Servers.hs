module Hubby.Servers
where

import Network.Wai
import Network.Wai.Handler.Warp as Warp
import Network.Wai.Handler.CGI as CGI
import Network.Wai.Handler.SCGI as SCGI
import Network.Wai.Middleware.RequestLogger

runWarp :: Int -> Application -> IO ()
runWarp port app = do
    putStrLn $ "Running Warp server on port 5000"
    Warp.run port . logStdout $ app

runCGI :: Application -> IO ()
runCGI = CGI.run

runSCGI :: Application -> IO ()
runSCGI = SCGI.run
