{-#LANGUAGE OverloadedStrings #-}
module Hubby.Cookies
( Cookie (..)
, CookieExpiry (..)
, CookieHttpOnly (..)
, CookieSecure (..)
, CookieDomain
, CookiePath
, makeCookie
, makeDefCookie
, cookieToValue
, parseCookies
)
where

import Data.Text (Text)
import qualified Data.Text as Text
import Data.ByteString (ByteString)
import qualified Data.ByteString as ByteString
import Data.Default
import Data.Maybe
import Data.Time
import qualified Data.HashMap.Strict as HashMap
import Data.HashMap.Strict (HashMap)
import Control.Applicative
import Data.String
import Text.StringConvert
import Data.Time
import Data.Time.Format
import System.Locale
import Data.Monoid
import Data.Char
import Data.List as List

data Cookie =
    Cookie
        { cookieName :: ByteString
        , cookieValue :: ByteString
        , cookieExpiry :: CookieExpiry
        , cookieHttpOnly :: CookieHttpOnly
        , cookieSecure :: CookieSecure
        , cookieDomain :: CookieDomain
        , cookiePath :: CookiePath
        }

data CookieExpiry =
    SessionCookie |
    AbsoluteCookieExpiry UTCTime |
    RelativeCookieExpiry Integer
    deriving (Show, Eq)

data CookieHttpOnly = CookieHttpOnly | CookieNotHttpOnly
    deriving (Show, Eq, Enum, Ord, Bounded)

data CookieSecure = CookieSecure | CookieNotSecure
    deriving (Show, Eq, Enum, Ord, Bounded)

data CookieDomain = CookieDomainAuto | CookieDomain [ByteString]
    deriving (Show, Eq, Ord)

makeCookieDomain :: [ByteString] -> CookieDomain
makeCookieDomain xs =
    if null xs' then CookieDomainAuto else CookieDomain xs'
    where
        xs' = filter (not . ByteString.null) xs

instance IsString CookieDomain where
    fromString "" = CookieDomainAuto
    fromString str = makeCookieDomain . splitBS '.' . s $ str

cookieDomainToValue :: CookieDomain -> Maybe ByteString
cookieDomainToValue (CookieDomain xs) = Just . mconcat . fmap ("." <>) $ xs
cookieDomainToValue CookieDomainAuto = Nothing

data CookiePath = CookiePathAuto | CookiePath [ByteString]
    deriving (Show, Eq, Ord)

instance IsString CookiePath where
    fromString "" = CookiePathAuto
    fromString str = makeCookiePath . splitBS '/' . s $ str

makeCookiePath :: [ByteString] -> CookiePath
makeCookiePath xs =
    if null xs' then CookiePathAuto else CookiePath xs'
    where
        xs' = filter (not . ByteString.null) xs

cookiePathToValue :: CookiePath -> Maybe ByteString
cookiePathToValue (CookiePath []) = Just "/"
cookiePathToValue (CookiePath xs) = Just . mconcat . fmap ("/" <>) $ xs
cookiePathToValue CookiePathAuto = Nothing

makeCookie :: ByteString -> ByteString -> CookieExpiry -> CookieHttpOnly -> CookieSecure -> CookieDomain -> CookiePath -> Either String Cookie
makeCookie "" _ _ _ _ _ _ = Left "Empty cookie name not allowed"
makeCookie name value expiry httpOnly secure domain path = Right $ Cookie name value expiry httpOnly secure domain path

makeDefCookie :: ByteString -> ByteString -> Either String Cookie
makeDefCookie name value = makeCookie name value SessionCookie CookieHttpOnly CookieNotSecure CookieDomainAuto CookiePathAuto

cookieToValue :: Cookie -> ByteString
cookieToValue cookie = mconcat . List.intersperse "; " . catMaybes $
    [ Just (cookieName cookie <> "=" <> cookieValue cookie)
    , ("Path=" <>) <$> cookiePathToValue (cookiePath cookie)
    , ("Domain=" <>) <$> cookieDomainToValue (cookieDomain cookie)
    , if cookieSecure cookie == CookieSecure then Just "Secure" else Nothing
    , if cookieHttpOnly cookie == CookieHttpOnly then Just "HttpOnly" else Nothing
    , case cookieExpiry cookie of
        SessionCookie -> Nothing
        RelativeCookieExpiry seconds -> Just ("Max-Age=" <> s (show seconds))
        AbsoluteCookieExpiry date -> Just ("Expires=" <> s (formatCookieDate date))
    ]

formatCookieDate :: UTCTime -> String
formatCookieDate = formatTime defaultTimeLocale "%a, %d-%b-%Y %H:%M:%S %Z"

splitBS :: Char -> ByteString -> [ByteString]
splitBS c = ByteString.split (fromIntegral . ord $ c)

parseCookies :: ByteString -> [(ByteString, ByteString)]
parseCookies raw =
    [ (k,mconcat xs) | (k:xs) <- pairs ]
    where
        pairs = map (splitBS '=') . (splitBS ';') $ raw
