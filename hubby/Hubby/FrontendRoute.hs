{-#LANGUAGE TemplateHaskell #-}
{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE FlexibleInstances #-}
module Hubby.FrontendRoute where

import Data.Text (Text)
import qualified Data.Text as Text
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.Aeson as JSON
import Data.Aeson.DeriveUtils
import Data.List
import Control.Applicative
import Control.Monad
import Data.Monoid
import Data.Maybe (fromMaybe)

import Hubby.RecordQuery
import Hubby.RecordQuery.JSON

data ContextMode =
    MaybeOne | -- ^ Zero or one
    RequireOne | -- ^ Exactly one
    Some | -- ^ Zero or more
    Many -- ^ One or more
    deriving (Show)

instance ToJSON ContextMode where
    toJSON MaybeOne = String "single"
    toJSON RequireOne = String "require-single"
    toJSON Some = String "some"
    toJSON Many = String "many"

instance FromJSON ContextMode where
    parseJSON (String "single") = return MaybeOne
    parseJSON (String "require-single") = return RequireOne
    parseJSON (String "some") = return Some
    parseJSON (String "many") = return Many
    parseJSON Null = return Some
    parseJSON (String "") = return Some
    parseJSON _ = mzero

type RouteCaptures = HashMap Text Value

type RoutePattern = [ RoutePart ]

printRoutePattern :: RoutePattern -> Text
printRoutePattern = mconcat . intersperse "/" . map printRoutePart

parseRoutePattern :: Text -> RoutePattern
parseRoutePattern "" = []
parseRoutePattern str = map parseRoutePart . Text.splitOn "/" $ str

instance ToJSON [RoutePart] where
    toJSON = String . printRoutePattern

instance FromJSON [RoutePart] where
    parseJSON (String str) = return $ parseRoutePattern str
    parseJSON v@(Array xs) = parseJSON v >>= mapM parseJSON
    parseJSON _ = mzero

data RoutePart =
    RoutePart (Maybe Text) RouteMatch
    deriving (Show)

printRoutePart :: RoutePart -> Text
printRoutePart (RoutePart Nothing (MatchLiteral n)) = n
printRoutePart (RoutePart Nothing MatchString) = "*"
printRoutePart (RoutePart Nothing MatchStrings) = "**"
printRoutePart (RoutePart (Just k) (MatchLiteral n)) = "{" <> k <> "=" <> n <> "}"
printRoutePart (RoutePart (Just k) (MatchString)) = "{" <> k <> "}"
printRoutePart (RoutePart (Just k) (MatchStrings)) = "{" <> k <> "*}"

dropLast :: Text -> Text
dropLast "" = ""
dropLast t = Text.take (Text.length t - 1) t

parseRoutePart :: Text -> RoutePart
parseRoutePart str =
    if Text.head str == '{' && Text.last str == '}'
        then
            let (left, right) = Text.breakOn "=" . Text.drop 1 . dropLast $ str
            in if right == ""
                then
                    if Text.last left == '*'
                        then RoutePart (Just $ dropLast left) MatchStrings
                        else RoutePart (Just left) MatchString
                else
                    RoutePart (Just left) (MatchLiteral $ Text.drop 1 right)
        else
            case str of
                "**" -> RoutePart Nothing MatchStrings
                "*" -> RoutePart Nothing MatchString
                n -> RoutePart Nothing (MatchLiteral n)

instance ToJSON RoutePart where
    toJSON = String . printRoutePart

instance FromJSON RoutePart where
    parseJSON (String str) = return $ parseRoutePart str
    parseJSON _ = mzero

data RouteMatch =
    MatchLiteral Text |
    MatchString |
    MatchStrings
    deriving (Show)

data ContextQuery =
    ContextQuery
        { cqQuery :: RecordQuery
        , cqMode :: ContextMode
        }
        deriving (Show)
$(deriveLensJSON ''ContextQuery)

data ContextElem =
    QueryContextElem ContextQuery |
    StaticContextElem JSON.Value |
    StaticFileContextElem FilePath |
    UploadedFileContextElem Text
    deriving (Show)


data ContextItem =
    ContextItem
        { cqKey :: Text
        , cqElem :: ContextElem
        }
    deriving (Show)

instance ToJSON ContextItem where
    toJSON i =
        object $ ("key" .= cqKey i):items0
        where
            items0 = case cqElem i of
                        QueryContextElem (ContextQuery query mode) ->
                            [ "query" .= query
                            , "mode" .= mode
                            ]
                        StaticContextElem value ->
                            [ "value" .= value ]
                        StaticFileContextElem path ->
                            [ "static-file" .= path ]
                        UploadedFileContextElem path ->
                            [ "file" .= show path ]

instance FromJSON ContextItem where
    parseJSON (Object o) = do
        key <- o .: "key"
        valueMay <- (o .:? "value")
        queryMay <- (o .:? "query")
        modeMay <- (o .:? "mode")
        staticFileMay <- (o .:? "static-file")
        uploadedFileMay <- (o .:? "file")
        let elem = fromMaybe (StaticContextElem Null) $ msum
                [ goQuery modeMay <$> queryMay
                , goStaticFile <$> staticFileMay
                , goFile <$> uploadedFileMay
                , StaticContextElem <$> valueMay
                ]
        return $ ContextItem key elem
        where
            goQuery :: Maybe ContextMode -> RecordQuery -> ContextElem
            goQuery modeMay query = QueryContextElem (ContextQuery query $ fromMaybe Some modeMay)

            goFile :: Text -> ContextElem
            goFile = UploadedFileContextElem

            goStaticFile :: FilePath -> ContextElem
            goStaticFile = StaticFileContextElem

data FrontendRoute =
    FrontendRoute
        { ferPattern :: RoutePattern
        , ferContext :: [ContextItem]
        , ferTemplate :: Text
        }
        deriving (Show)
$(deriveLensJSON ''FrontendRoute)

-- | Check if a RoutePattern matches against a path (given as a list of path
-- elements). Returns a hash map of matched captures if the route matches, or
-- Nothing if the matching fails.
matchRoutePattern :: RoutePattern -> [Text] -> Maybe (HashMap Text Value)
matchRoutePattern [] [] = Just HashMap.empty
matchRoutePattern _ [] = Nothing -- unmatched excess
matchRoutePattern [] _ = Nothing
matchRoutePattern (RoutePart rpnMay MatchStrings:ms) ps = do
    (match, rest) <- foldl' (<|>) Nothing $ map go (breaks ps)
    return $ hashMapAddMaybe rpnMay (toJSON . map String $ match) rest
    where
        go (lps, rps) = do
            rest <- matchRoutePattern ms rps
            return (lps, rest)

matchRoutePattern (RoutePart rpnMay matcher:xs) (p:ps) = do
    match <- case matcher of
                MatchLiteral txt ->
                    if p == txt
                        then return (String p)
                        else Nothing
                MatchString ->
                    return (String p)
    rest <- matchRoutePattern xs ps
    return $ hashMapAddMaybe rpnMay match rest

breaks :: [a] -> [([a],[a])]
breaks xs = map (flip splitAt xs) [0..length xs]

-- hashMapAddMaybe :: Maybe a -> b -> HashMap a b -> HashMap a b
hashMapAddMaybe Nothing _ c = c
hashMapAddMaybe (Just k) v c = HashMap.insert k v c

matchFrontendRoute :: FrontendRoute -> [Text] -> Maybe (HashMap Text Value)
matchFrontendRoute fer path = matchRoutePattern (ferPattern fer) path

matchFrontendRoutes :: [FrontendRoute] -> [Text] -> Maybe (HashMap Text Value, FrontendRoute)
matchFrontendRoutes [] path = Nothing
matchFrontendRoutes (x:xs) path =
    let match = matchFrontendRoute x path
    in case match of
        Nothing -> matchFrontendRoutes xs path
        Just m -> return (m, x)
