module Hubby.Thumbnails
( makeThumbnail
, getThumbnail
, ThumbGeometry (..)
, parseGeometry
)
where

import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString as BS
import System.Process (runInteractiveProcess)
import Text.StringConvert
import Data.Cache (Cache)
import Data.Hashable
import qualified Data.Cache as Cache
import System.FilePath
import Data.List
import Data.Char
import Safe

data ThumbResizeMode = ThumbDefault
    deriving (Read, Show, Eq, Ord, Enum, Bounded)

magickResizeMarker :: ThumbResizeMode -> String
magickResizeMarker ThumbDefault = "^"

data ThumbGeometry =
    ThumbGeometry
        { tgeoWidth :: Int
        , tgeoHeight :: Int
        , tgeoMode :: ThumbResizeMode
        }
        deriving (Show, Read, Eq, Ord)

instance Hashable ThumbGeometry where
    hash (ThumbGeometry w h m) = hash (w, h, fromEnum m)
    hashWithSalt s (ThumbGeometry w h m) = hashWithSalt s (w, h, fromEnum m)

geoToMagickResizeStr :: ThumbGeometry -> String
geoToMagickResizeStr geo =
    show (tgeoWidth geo) ++
    "x" ++
    show (tgeoHeight geo) ++
    magickResizeMarker (tgeoMode geo)

geoToMagickCropStr :: ThumbGeometry -> String
geoToMagickCropStr geo =
    show (tgeoWidth geo) ++
    "x" ++
    show (tgeoHeight geo) ++
    "+0+0"

parseGeometry :: String -> Maybe ThumbGeometry
parseGeometry str = do
    let (widthStr, 'x':str') = break (not . isDigit) str
    width <- readMay widthStr
    let (heightStr, marker) = break (not . isDigit) str'
    height <- readMay heightStr
    mode <- case marker of
                "^" -> return ThumbDefault
                "" -> return ThumbDefault
                _ -> Nothing
    return $ ThumbGeometry width height mode

getThumbnail :: Cache (FilePath, ThumbGeometry) BS.ByteString -> IO LBS.ByteString -> FilePath -> ThumbGeometry -> IO LBS.ByteString
getThumbnail cache loader filename geometry = do
    contentMay <- Cache.get cache (filename, geometry)
    case contentMay of
        Just contents -> return . LBS.fromStrict $ contents
        Nothing -> do
            origContents <- loader
            let extension = drop 1 $ takeExtension filename
            contents <- makeThumbnail extension geometry origContents
            Cache.set cache (filename, geometry) (LBS.toStrict contents)
            return contents

makeThumbnail :: String -> ThumbGeometry -> LBS.ByteString -> IO LBS.ByteString
makeThumbnail extension geometry contents = do
    let args = [ extension ++ ":-"
               , "-resize", geoToMagickResizeStr geometry
               , "-gravity", "center"
               , "-crop", geoToMagickCropStr geometry
               , "png:-"
               ]
    (inH, outH, errH, pH) <- runInteractiveProcess "convert" args Nothing Nothing
    LBS.hPut inH contents
    LBS.hGetContents outH
