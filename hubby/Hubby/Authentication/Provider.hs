module Hubby.Authentication.Provider
where

import Data.Text (Text)
import qualified Data.Text as Text
import Text.StringConvert

type UserID = Text
type Username = Text
type Password = Text

-- | Generic interface for authentication providers.
-- Needs some rethinking, because not all authentication methods necessarily
-- work the same way (e.g. OpenID etc.)
data AuthProvider =
    AuthProvider
        { authCheck :: Username -> Password -> IO (Maybe UserID)
        , authDispose :: IO ()
        , authCreateUser :: Username -> Password -> IO (Either String UserID)
        , authDeleteUser :: UserID -> IO (Either String ())
        }
