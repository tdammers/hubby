{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE TemplateHaskell #-}
-- | A filebased authentication provider that accepts *all* usernames and passwords,
-- and always returns a hard-coded filebased user ID. Not intended for production
-- use; the purpose of this module is purely for development.
module Hubby.Authentication.Providers.FileBased
( createFileBasedProvider
)
where

import Hubby.Authentication.Provider
import Data.Aeson as JSON
import Data.Aeson.DeriveUtils
import Data.Aeson.ParseUtils
import Data.Text (Text)
import qualified Data.Text as Text
import Crypto.PasswordStore
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Default
import Data.Token
import System.IO
import System.IO.Error
import Data.Char
import Data.Monoid
import Text.StringConvert
import System.FilePath
import System.Posix (removeLink)
import Control.Applicative

data FBPConfig =
    FBPConfig
        { fbpDirectory :: FilePath
        , fbpHashStrength :: Int
        }

$(deriveLensJSON ''FBPConfig)

instance Default FBPConfig where
    def = FBPConfig "./users" 17

createFileBasedProvider :: JSON.Value -> IO (Either String AuthProvider)
createFileBasedProvider confJSON = do
    let configMay = fromJSONWithDefaults def confJSON
    case configMay of
        Error msg -> return . Left $ "Invalid file-based auth config: " ++ msg
        Success config ->
            return . Right $
                AuthProvider
                    { authCheck = filebasedAuthCheck config
                    , authDispose = return ()
                    , authCreateUser = filebasedAuthCreateUser config
                    , authDeleteUser = filebasedAuthDeleteUser config
                    }

filebasedAuthCheck :: FBPConfig -> Username -> Password -> IO (Maybe UserID)
filebasedAuthCheck config username password = do
    catchIOError go (const $ return Nothing)
    where
        go :: IO (Maybe UserID)
        go = do
            let fpUsername = fbpDirectory config </> ("n-" <> s username)
            str <- s <$> BS.readFile fpUsername
            case Text.splitOn ":" str of
                userID:hash:[] -> do
                    let fpID = fbpDirectory config </> ("id-" <> s userID)
                    return $
                        if verifyPassword (s password) (s hash)
                            then Just userID
                            else Nothing
                _ -> return Nothing

filebasedAuthCreateUser :: FBPConfig -> Username -> Password -> IO (Either String UserID)
filebasedAuthCreateUser config username password = do
    if not (isValidUsername username)
        then
            return . Left $ "'" <> s username <> "' is not allowed as a username"
        else do
            result <- tryIOError go
            case result of
                Left err -> return . Left . ioeGetErrorString $ err
                Right userID -> return . Right $ userID
    where
        go :: IO UserID
        go = do
            userID <- s <$> mkTokenIO (['0'..'9'] ++ ['A'..'F']) 32
            let fnID = fbpDirectory config </> ("id-" <> s userID)
                fnUsername = fbpDirectory config </> ("n-" <> s username)
            hash <- makePassword (s password) (fbpHashStrength config)
            BS.writeFile fnID $ (s username) <> ":" <> hash
            BS.writeFile fnUsername $ (s userID) <> ":" <> hash
            return userID

filebasedAuthDeleteUser :: FBPConfig -> UserID -> IO (Either String ())
filebasedAuthDeleteUser config userID = do
    let fpID = fbpDirectory config </> ("id-" <> s userID)
    str <- s <$> BS.readFile fpID
    case Text.splitOn ":" str of
        username:hash:[] -> do
            let fpUsername = fbpDirectory config </> ("n-" <> s username)
            removeLink fpUsername
            removeLink fpID
            return . Right $ ()
        _ -> return . Left $ "Malformed user record in '" ++ fpID ++ "'"

isValidUsername :: Text -> Bool
isValidUsername u = not (Text.null u) && Text.all (\c -> isDigit c || isAlpha c || c `elem` "_@.-") u
