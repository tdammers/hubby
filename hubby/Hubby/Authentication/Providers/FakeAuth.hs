{-#LANGUAGE OverloadedStrings #-}
-- | A fake authentication provider that accepts *all* usernames and passwords,
-- and always returns a hard-coded fake user ID. Not intended for production
-- use; the purpose of this module is purely for development.
module Hubby.Authentication.Providers.FakeAuth
( createFakeAuthProvider
)
where

import Hubby.Authentication.Provider
import Data.Aeson as JSON
import Data.Text (Text)

createFakeAuthProvider :: JSON.Value -> IO (Either String AuthProvider)
createFakeAuthProvider confJSON =
    return . Right $
        AuthProvider
            { authCheck = fakeAuthCheck
            , authDispose = return ()
            , authCreateUser = fakeAuthCreateUser
            , authDeleteUser = fakeAuthDeleteUser
            }

fakeAuthCheck :: Username -> Password -> IO (Maybe UserID)
fakeAuthCheck _ _ = return . Just $ "00000000000000000000000000000000"

fakeAuthCreateUser :: Username -> Password -> IO (Either String UserID)
fakeAuthCreateUser _ _ = return . Left $ "The fake auth provider does not support creating users."

fakeAuthDeleteUser :: UserID -> IO (Either String ())
fakeAuthDeleteUser _ = return . Left $ "The fake auth provider does not support deleting users."
