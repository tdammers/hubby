{-#LANGUAGE OverloadedStrings #-}
module Hubby.Handlers
( mainH
, runHandler
)
where

import Data.Text (Text)
import Data.List
import Data.Monoid
import Text.StringConvert

import Network.Wai
import Network.HTTP.Types

import Hubby.AppContext
import Hubby.AppConfig
import Hubby.Handlers.Common
import Hubby.Handlers.Admin
import Hubby.Handlers.Frontend

mainH :: Handler
mainH = do
    config <- getConfig
    let mounts :: [(Text, [Text] -> Handler)]
        mounts =
            [ (adminMountPoint config, adminH)
            ]
    path <- fromRequest pathInfo
    if not (null path) && (last path == "")
        then redirect302 (s . ("/" <>) . mconcat . intersperse "/" . dropWhileEnd (== "") $ path)
        else
            case path of
                [] -> frontendH []
                mountpoint:tail ->
                    case lookup mountpoint mounts of
                        Just handler -> handler tail
                        Nothing -> frontendH (mountpoint:tail)
