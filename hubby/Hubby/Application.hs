{-#LANGUAGE OverloadedStrings #-}
module Hubby.Application
where

import Data.Text (Text)

import Network.Wai
import Network.HTTP.Types

import Network.Wai.Middleware.Autohead
import Network.Wai.Middleware.Gzip
import Network.Wai.Middleware.MethodOverride

import Hubby.AppContext
import Hubby.Handlers

application :: AppContext -> Application
application app = gzip def . methodOverride . autohead $ runHandler mainH app
