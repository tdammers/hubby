{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE TemplateHaskell #-}
{-#LANGUAGE FlexibleContexts #-}
{-#LANGUAGE FlexibleInstances #-}
module Hubby.AppConfig
( AppConfig (..)
, StorageConfig (..)
, SessionsConfig (..)
, AuthConfig (..)
, FileStoreConfig (..)
, Blueprint (..)
, MonadConfig
, getConfig, getsConfig
, mkAdminURL
, AdminAssetsSource (..)
)
where

import Data.Default
import qualified Data.ByteString as BS
import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Aeson
import Data.Aeson.TH
import Data.Aeson.DeriveUtils
import qualified Data.Vector as Vector
import Control.Applicative
import Control.Monad
import Text.StringConvert
import Data.Monoid

import Hubby.Storage
import Hubby.FileStore
import Hubby.FrontendRoute
import Hubby.Record

data StorageConfig =
    StorageConfig
        { storageProviderName :: String
        , storageProviderConfig :: Value
        }
$(deriveJSON
    defaultOptions { fieldLabelModifier = camelToDashed '-' . drop (length ("storageProvider" :: String)) }
    ''StorageConfig)

data FileStoreConfig =
    FileStoreConfig
        { fileStoreName :: String
        , fileStoreConfig :: Value
        }
$(deriveJSON
    defaultOptions { fieldLabelModifier = camelToDashed '-' . drop (length ("fileStore" :: String)) }
    ''FileStoreConfig)

data SessionsConfig =
    SessionsConfig
        { sessionsProviderName :: String
        , sessionsProviderConfig :: Value
        }
$(deriveJSON
    defaultOptions { fieldLabelModifier = camelToDashed '-' . drop (length ("sessionsProvider" :: String)) }
    ''SessionsConfig)

data AuthConfig =
    AuthConfig
        { authProviderName :: String
        , authProviderConfig :: Value
        }
$(deriveJSON
    defaultOptions { fieldLabelModifier = camelToDashed '-' . drop (length ("authProvider" :: String)) }
    ''AuthConfig)

data Blueprint =
    Blueprint
        { blueprintName :: Text
        , blueprintSlug :: Text
        , blueprintFields :: RecordFields
        }
$(deriveJSON
    defaultOptions { fieldLabelModifier = camelToDashed '-' . drop (length ("blueprint" :: String)) }
    ''Blueprint)

data AdminAssetsSource = AdminAssetsBuiltin | AdminAssetsDynamic FilePath
    deriving (Show, Eq)

instance ToJSON AdminAssetsSource where
    toJSON AdminAssetsBuiltin = String "builtin"
    toJSON (AdminAssetsDynamic fp) = toJSON [ "file", fp ]

instance FromJSON AdminAssetsSource where
    parseJSON (String "builtin") = return AdminAssetsBuiltin
    parseJSON (Array v) =
        case Vector.toList v of
            String "file":String fpt:[] -> return (AdminAssetsDynamic (s fpt))
            _ -> fail "Not a valid AdminAssetsSource"

data AppConfig =
    AppConfig
        { adminMountPoint :: Text
        , staticMountPoint :: Text
        , adminAssets :: AdminAssetsSource
        , storage :: StorageConfig
        , fileStore :: FileStoreConfig
        , sessions :: SessionsConfig
        , auth :: AuthConfig
        , blueprints :: [Blueprint]
        , routes :: [FrontendRoute]
        , e404Template :: Text
        }

$(deriveJSON
    defaultOptions { fieldLabelModifier = camelToDashed '-' }
    ''AppConfig)

instance Default AppConfig where
    def =
        AppConfig
            { adminMountPoint = "admin"
            , staticMountPoint = "static"
            , adminAssets = AdminAssetsDynamic "admin-static"
            , storage = StorageConfig "in-process" (defStorageConfig "in-process")
            , fileStore = FileStoreConfig "fs" Null
            , sessions = SessionsConfig "default" Null
            , auth = AuthConfig "default" Null
            , blueprints = []
            , routes = []
            , e404Template = "error404.ginger.html"
            }

class Monad m => MonadConfig m where
    getConfig :: m AppConfig

getsConfig :: MonadConfig m => (AppConfig -> a) -> m a
getsConfig p = p `liftM` getConfig

mkAdminURL :: (MonadConfig m, StringConvert Text s) => [Text] -> m s
mkAdminURL path =
    (s . mconcat . fmap ("/" <>) . (:path)) `liftM` getsConfig adminMountPoint
