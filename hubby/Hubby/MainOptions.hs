module Hubby.MainOptions
( MainOptions (..)
, defMainOptions
, Command (..)
, WarpOptions (..)
, CGIOptions (..)
, SCGIOptions (..)
, UserCommand (..)
, TestSpec (..)
, parseMainOptions
, printHelp, hPrintHelp
)
where

import Text.Parsec
import Text.Parsec.Error
import Control.Applicative ( (<$>), (<*>) )
import Data.List
import Data.Char
import System.IO
import System.Environment
import System.FilePath
import Control.Monad

printHelp :: IO ()
printHelp = do
    progname <- takeFileName <$> getEnv "_"
    hPrintHelp progname stdout

unzipl :: [[a]] -> [[a]]
unzipl [] = []
unzipl (x:xs) =
    go (x:xs)
    where
        go :: [[a]] -> [[a]]
        go [] = repeat []
        go (x:xs) = zipWith (:) x (go xs)

padTo :: Int -> String -> String
padTo l = take l . (++ repeat ' ')

padColumns :: [[String]] -> [[String]]
padColumns rows =
    let columns = unzipl rows
        colw = maximum . map length
        columnWidths = map colw columns
    in [ zipWith padTo columnWidths row
       | row <- rows
       ]


hPrintHelp :: String -> Handle -> IO ()
hPrintHelp progname h = do
    hPutStrLn h $ "Usage: " ++ progname ++ " [OPTION]... COMMAND [OPTION]..."
    hPutStrLn h $ "Perform an action on a hubby website in the CWD. The action " ++
                "to perform depends on the specified COMMAND."
    hPutStrLn h ""
    hPutStrLn h "COMMANDS"
    let rows =
            [ ["  ", command ++ " " ++ (concat . intersperse " " $ arguments), description]
            | (command, arguments, description, _)
            <- commandSpecs
            ]
        rowsPadded = padColumns rows
    forM_ rowsPadded $
        hPutStrLn h . concat . intersperse " "

commandSpecs =
    [ ( "warp"
      , ["PORT"]
      , "Start a standalone server (using Warp), running on localhost:PORT"
      , serveWarpP
      )
    , ( "cgi"
      , []
      , "Serve one request using the CGI protocol"
      , serveCGIP
      )
    , ( "scgi"
      , []
      , "Start an SCGI process"
      , serveSCGIP
      )
    , ( "user"
      , ["SUBCOMMAND", "ARG..."]
      , "Run a SUBCOMMAND from the 'user' command namespace with given ARGs"
      , userCommandP
      )
    , ( "test"
      , []
      , "Run the test suite"
      , runTestsP
      )
    , ( "make-config"
      , []
      , "Output a default configuration file on stdout"
      , makeConfigP
      )
    , ( "help"
      , []
      , "Print this help"
      , return PrintHelp
      )
    ]


data Command = RunWarp WarpOptions
             | RunCGI CGIOptions
             | RunSCGI SCGIOptions
             | RunUserCommand UserCommand
             | MakeConfig
             | RunTests TestSpec
             | PrintHelp
             deriving (Show, Read, Eq)

data WarpOptions =
    WarpOptions
        { warpPort :: Int }
        deriving (Show, Read, Eq)

data CGIOptions = CGIOptions
        deriving (Show, Read, Eq)

defCGIOptions = CGIOptions

data SCGIOptions = SCGIOptions
        deriving (Show, Read, Eq)

defSCGIOptions = SCGIOptions

data UserCommand =
    UserCreate
        { userCreateUsername :: Maybe String
        , userCreatePassword :: Maybe String
        }
    deriving (Show, Read, Eq)

data TestSpec = AllTests
        deriving (Show, Read, Eq)

data MainOptions =
    MainOptions
        { moCommand :: Command
        , moConfigFiles :: [ FilePath ]
        , moWarnings :: [ String ]
        }
        deriving (Show, Read, Eq)

defMainOptions :: MainOptions
defMainOptions =
    MainOptions
        { moCommand = RunCGI defCGIOptions
        , moConfigFiles = ["config.yml"]
        , moWarnings = []
        }

warn :: String -> MainOptions -> MainOptions
warn msg mo = mo { moWarnings = msg:moWarnings mo }

parseMainOptions :: [String] -> Either String MainOptions
parseMainOptions args =
    case go of
        Left pe -> Left (show pe)
        Right opts -> Right (opts defMainOptions)
    where
        go = runParser mainOptionsP () "command line options" args

type ArgParser = Parsec [String] ()

-- | Variadic function composition
comp :: [a -> a] -> a -> a
comp [] = id
comp xs = foldr1 (.) xs

-- | Match a token exactly
match :: (Show t, Eq t, Stream s m t, Monad m) => t -> ParsecT s u m t
match x = matchWhere (== x)

matchWhere :: (Show t, Stream s m t, Monad m) => (t -> Bool) -> ParsecT s u m t
matchWhere f = do
    c <- anyToken
    if f c
        then return c
        else unexpected (show c)

mainOptionsP :: ArgParser (MainOptions -> MainOptions)
mainOptionsP = do
    preOptions <- comp <$> many mainOptionP
    command <- commandP
    postOptions <- comp <$> many mainOptionP
    eof
    return $ postOptions . preOptions . (\options -> options { moCommand = command })

mainOptionP =
    unrecognizedOptionP

commandP =
    choice
        [ try (match keyword) >> p
        | (keyword, _, _, p)
        <- commandSpecs
        ]

serveWarpP = do
    RunWarp <$> warpOptionsP

warpOptionsP = do
    WarpOptions . read <$> matchWhere (all isDigit)

serveCGIP = return (RunCGI defCGIOptions)
serveSCGIP = return (RunSCGI defSCGIOptions)

userCommandP = do
    RunUserCommand <$> userSubcommandP

userSubcommandP = userCreateP

userCreateP = do
    try $ match "create"
    username <- optionMaybe (matchWhere $ not . ("-" `isPrefixOf`))
    password <- optionMaybe (matchWhere $ not . ("-" `isPrefixOf`))
    return $ UserCreate username password

makeConfigP = return MakeConfig

runTestsP = return (RunTests AllTests)

unrecognizedOptionP = do
    c <- lookAhead anyToken
    case c of
        '-':[] -> unexpected c
        '-':_ -> return . warn $ "Unknown option '" ++ c ++ "', ignoring"
        _ -> unexpected c
