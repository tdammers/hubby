{-#LANGUAGE GeneralizedNewtypeDeriving #-}
module Hubby.SessionProvider
( module Hubby.SessionProviders.Type
, generateSessionKey
, createSessionProvider
, regenerateSessionKey
, newSession
)
where

import Data.Text (Text)
import qualified Data.Text as Text
import Data.Token
import Text.StringConvert
import Control.Applicative
import Hubby.SessionProviders.Type
import Hubby.SessionProviders.Default
import Hubby.Session
import qualified Data.Aeson as JSON

generateSessionKey :: IO SessionKey
generateSessionKey =
    SessionKey . s <$> mkTokenIO alphabet 32
    where
        alphabet = ['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9']

regenerateSessionKey :: SessionProvider a -> SessionKey -> IO SessionKey
regenerateSessionKey sp k = do
    k' <- generateSessionKey
    moveSession sp k k'
    return k'

newSession :: SessionProvider a -> a -> IO SessionKey
newSession sp v = do
    k <- generateSessionKey
    putSession sp v k
    return k

createSessionProvider :: String -> JSON.Value -> IO (Either String (SessionProvider Session))
createSessionProvider "default" val = Right <$> mkDefaultSessionProvider val
