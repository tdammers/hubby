{-#LANGUAGE OverloadedStrings #-}
module Hubby.RecordQuery.Parse
( parseQuery
)
where

import Control.Applicative ( (<$>), (<*>) )
import Safe

import Text.Parsec
import Text.Parsec.Char

import Data.Text (Text)
import Text.StringConvert
import Data.Char
import Data.Monoid
import Data.List
import Data.Scientific
import Data.Char
import Data.Default
import qualified Data.Vector as Vector

import qualified Data.Aeson as JSON

import Hubby.RecordQuery

type P = Parsec String ()

parseQuery :: Text -> Either ParseError RecordQuery
parseQuery = parse queryP "query" . s

-- | One or more whitespace characters
spaces' :: P ()
spaces' = space >> spaces

ignore :: P a -> P ()
ignore = (>> return ())

operatorChars :: [Char]
operatorChars =
     filter (not . isAlpha)
   . filter (not . isDigit)
   . filter (/= '_')
   . nub
   . sort
   . concat
   $ map show (listAll :: [BinaryOperator]) ++ map show (listAll :: [TernaryOperator])

endOfWord :: P ()
endOfWord = eof <|> notFollowedBy (alphaNum <|> char '_')

endOfOperator :: P ()
endOfOperator = eof <|> notFollowedBy (oneOf operatorChars)

word :: String -> P String
word "" = return ""
word w@(c:cs) = try $ do
    string w
    if c `elem` operatorChars
        then
            endOfOperator
        else if isAlpha c then
            endOfWord
        else
            return ()
    spaces
    return w

queryP :: P RecordQuery
queryP =
    between spaces eof $ do
        optional . word $ "SELECT"
        RecordQuery <$> matchClauseP <*> orderClauseP <*> limitP

matchClauseP :: P Match
matchClauseP = option MatchAll $
             try (word "ALL" >> return MatchAll)
             <|> try ((optional . try . word $ "WHERE") >> matchP)

matchP :: P Match
matchP = try matchOrP

matchOrP = do
    lhs <- try matchAndP
    spaces
    try (orTail lhs) <|> return lhs
    where
        orTail lhs = do
            word "OR"
            MatchEither lhs <$> matchOrP

matchAndP = do
    lhs <- try matchNotP <|> try matchCondP
    spaces
    try (andTail lhs) <|> return lhs
    where
        andTail lhs = do
            word "AND"
            MatchBoth lhs <$> matchAndP

matchNotP = do
    between
        (word "NOT" >> spaces)
        (return ())
        (MatchNot <$> (matchCondP <|> matchNotP))

matchCondP =
    try parenthesizedMatchP
    <|> try (word "NO" >> return MatchNone)
    <|> try (word "YES" >> return MatchAll)
    <|> (postProcessCondP <$> try condP)

parenthesizedMatchP :: P Match
parenthesizedMatchP =
    between
        (char '(' >> spaces)
        (char ')' >> spaces)
        matchP

listAll :: (Enum a, Bounded a) => [a]
listAll = [ minBound .. maxBound ]

postProcessCondP :: Match -> Match
postProcessCondP
    (MatchBinary OpEq (FieldOperand "id") (LiteralOperand v)) =
        MatchID (s v)
postProcessCondP
    (MatchBinary OpNEq (FieldOperand "id") (LiteralOperand v)) =
        MatchNot $ MatchID (s v)
postProcessCondP
    (MatchBinary OpEq (LiteralOperand v) (FieldOperand "id")) =
        MatchID (s v)
postProcessCondP
    (MatchBinary OpNEq (LiteralOperand v) (FieldOperand "id")) =
        MatchNot $ MatchID (s v)
postProcessCondP m = m

condP :: P Match
condP = do
    lhs <- operandP
    spaces
    try (ternaryTailP lhs) <|> try (binaryTailP lhs)
    where
        ternaryTailP :: Operand -> P Match
        ternaryTailP lhs = foldl1' (<|>) $ map (try . ternaryOpTailP lhs) listAll

        ternaryOpTailP :: Operand -> TernaryOperator -> P Match
        ternaryOpTailP lhs op = do
            let (op1, op2) = ternaryOperatorToStrings op
            word op1
            mhs <- operandP
            spaces
            word op2
            rhs <- operandP
            spaces
            return $ MatchTernary op lhs mhs rhs

        binaryTailP :: Operand -> P Match
        binaryTailP lhs = foldl1' (<|>) $ map (try . binaryOpTailP lhs) listAll

        binaryOpTailP :: Operand -> BinaryOperator -> P Match
        binaryOpTailP lhs op = do
            let op1 = binaryOperatorToString op
            word . s $ op1
            rhs <- operandP
            spaces
            return $ MatchBinary op lhs rhs

operandP :: P Operand
operandP = try paramOperandP
         <|> try literalOperandP
         <|> try fieldOperandP
         <?> "operand"

paramOperandP :: P Operand
paramOperandP = do
    try $ char '$'
    x <- letter
    xs <- many (alphaNum <|> char '_')
    return . ParamOperand . s $ x:xs

fieldOperandP :: P Operand
fieldOperandP = FieldOperand <$> fieldNameP

fieldNameP :: P Text
fieldNameP = try quotedFieldNameP <|> try plainFieldNameP <?> "field name"

quotedFieldNameP :: P Text
quotedFieldNameP =
    s <$>
        between
            (ignore $ char '`')
            (char '`' >> spaces)
            (many1 $ noneOf "`")

plainFieldNameP :: P Text
plainFieldNameP = do
    x <- letter <|> char '_'
    xs <- many $ letter <|> digit <|> char '_'

    return . s $ x:xs

literalOperandP = LiteralOperand <$> jsonP

jsonP :: P JSON.Value
jsonP = jsonBarewordP <|> jsonArrayP <|> jsonNumberP <|> jsonStringP <?> "JSON value"

jsonArrayP :: P JSON.Value
jsonArrayP = do
    items <- between
        (ignore $ char '[' >> spaces)
        (ignore $ char ']' >> spaces)
        (jsonP `sepBy` ignore (char ',' >> spaces))
    return . JSON.Array . Vector.fromList $ items

jsonNumberP :: P JSON.Value
jsonNumberP = do
    sign <- option "" $ (:[]) <$> char '-'
    intPart <- try (string "0") <|> (:) <$> oneOf ['1'..'9'] <*> many digit
    fracPart <- option "" $ (:) <$> char '.' <*> many digit
    return . JSON.Number . read $ sign ++ intPart ++ fracPart

jsonStringP :: P JSON.Value
jsonStringP =
    JSON.String . s <$> between
        (char '"')
        (char '"' >> spaces)
        (many stringCharP)
    where
        stringCharP :: P Char
        stringCharP = escapedCharP <|> plainCharP

        plainCharP :: P Char
        plainCharP = noneOf "\"\\"

        escapedCharP :: P Char
        escapedCharP = do
            ignore $ char '\\'
            c <- anyChar
            case c of
                '"' -> return '"'
                '\\' -> return '\\'
                '/' -> return '/'
                'b' -> return '\b'
                'f' -> return '\f'
                'n' -> return '\n'
                'r' -> return '\r'
                't' -> return '\t'
                'u' -> do
                    nMay <- readMay . ("0x" ++) <$> count 4 hexDigit
                    case nMay of
                        Nothing -> fail "This should never happen :P"
                        Just n -> return $ chr n
                x -> unexpected (show x)

jsonBarewordP :: P JSON.Value
jsonBarewordP =
    try (word "true" >> return (JSON.Bool True))
    <|> try (word "false" >> return (JSON.Bool False))
    <|> try (word "null" >> return JSON.Null)

orderClauseP :: P [Order]
orderClauseP = option [] $ do
    word "ORDER"
    optional . try $ word "BY"
    sepBy1 (try orderP) (ignore $ char ',' >> spaces)

orderP :: P Order
orderP = do
    fieldName <- try fieldNameP
    target <- if fieldName == "id"
        then return OrderID
        else option (OrderFieldAlpha fieldName) $ do
                (try (word "NUM") <|> try (word "NUMERICALLY"))
                return (OrderFieldNum fieldName)
    ascDesc <- option Ascending $
                    try (word "ASC" >> return Ascending)
                    <|> try (word "DESC" >> return Descending)
    return $ Order target ascDesc

intP :: P Int
intP = do
    negative <- option False (try (char '-') >> return True)
    intStr <- string "0" <|> (:) <$> oneOf ['1'..'9'] <*> many (try digit)
    case readMay intStr of
        Nothing -> fail "invalid integer literal"
        Just i -> return $ if negative then -i else i

limitP :: P Limit
limitP = option Unlimited $ do
    word "LIMIT"
    num1 <- intP
    spaces
    let t = do
            char ','
            spaces
            num2 <- intP
            return $ LimitOffsetCount num1 num2
    try t <|> return (LimitCount num1)
