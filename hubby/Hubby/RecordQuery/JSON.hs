{-#LANGUAGE OverloadedStrings #-}
module Hubby.RecordQuery.JSON where

import Data.Text (Text)
import Data.Maybe
import Control.Applicative
import Control.Monad
import Text.StringConvert
import Data.Default

import Data.Aeson

import Hubby.RecordQuery
import Hubby.RecordQuery.Parse

instance ToJSON Operand where
    toJSON (LiteralOperand v) = toJSON [ String "lit", v ]
    toJSON (FieldOperand fn) = toJSON [ String "field", String fn ]
    toJSON (ParamOperand pn) = toJSON [ String "param", String pn ]

instance FromJSON Operand where
    parseJSON v@(Array _) = do
        items <- parseJSON v
        case items of
            String "lit":v:[] -> return $ LiteralOperand v
            String "field":String fn:[] -> return $ FieldOperand fn
            String "param":String pn:[] -> return $ ParamOperand pn
            _ -> mzero
    parseJSON _ = mzero

instance ToJSON Match where
    toJSON MatchAll = toJSON [ String "YES" ]
    toJSON MatchNone = toJSON [ String "NO" ]
    toJSON (MatchID id) = toJSON [ String "ID", String id ]
    toJSON (MatchBinary op a b) = toJSON [ String (s $ binaryOperatorToString op), toJSON a, toJSON b ]
    toJSON (MatchTernary op a b c) = toJSON [ String (s . fst $ ternaryOperatorToStrings op), toJSON a, toJSON b, toJSON c ]
    toJSON (MatchEither a b) = toJSON [ String "OR", toJSON a, toJSON b ]
    toJSON (MatchBoth a b) = toJSON [ String "AND", toJSON a, toJSON b ]
    toJSON (MatchNot a) = toJSON [ String "NOT", toJSON a ]

instance FromJSON Match where
    parseJSON v@(Array _) = do
        items <- parseJSON v
        case items of
            String "ALL":[] -> return MatchAll
            String "YES":[] -> return MatchAll
            String "NO":[] -> return MatchNone
            String "ID":v:[] -> MatchID <$> parseJSON v
            String "NOT":v:[] -> MatchNot <$> parseJSON v
            String "OR":va:vb:[] -> MatchEither <$> parseJSON va <*> parseJSON vb
            String "AND":va:vb:[] -> MatchBoth <$> parseJSON va <*> parseJSON vb
            String opStr:a:b:[] -> do
                maybe mzero (\op -> MatchBinary op <$> parseJSON a <*> parseJSON b) $ binaryOperatorFromString (s opStr)
            String opStr:a:b:c:[] -> do
                maybe mzero (\op -> MatchTernary op <$> parseJSON a <*> parseJSON b <*> parseJSON c) $ ternaryOperatorFromString (s opStr)
    parseJSON _ = mzero

instance ToJSON OrderAscDesc where
    toJSON Ascending = String "ASC"
    toJSON Descending = String "DESC"

instance FromJSON OrderAscDesc where
    parseJSON (String "ASC") = return Ascending
    parseJSON (String "DESC") = return Descending
    parseJSON Null = return def
    parseJSON (String "") = return def
    parseJSON _ = mzero

instance ToJSON OrderTarget where
    toJSON OrderID = toJSON [ String "id" ]
    toJSON (OrderFieldNum fn) = toJSON [ String "num", toJSON fn ]
    toJSON (OrderFieldAlpha fn) = toJSON [ String "alpha", toJSON fn ]

instance FromJSON OrderTarget where
    parseJSON a@(Array _) = do
        items <- parseJSON a
        case items of
            String "id":[] -> return OrderID
            String "num":String fn:[] -> return $ OrderFieldNum fn
            String "alpha":String fn:[] -> return $ OrderFieldAlpha fn
            _ -> mzero
    parseJSON _ = mzero

instance ToJSON Order where
    toJSON (Order OrderID ad) = toJSON [ String "id", toJSON ad ]
    toJSON (Order (OrderFieldNum fn) ad) = toJSON [ String "num", toJSON ad, toJSON fn ]
    toJSON (Order (OrderFieldAlpha fn) ad) = toJSON [ String "alpha", toJSON ad, toJSON fn ]

instance FromJSON Order where
    parseJSON a@(Array _) = do
        items <- parseJSON a
        case items of
            String "id":v:[] -> Order OrderID <$> parseJSON v
            String "num":v:String fn:[] -> Order (OrderFieldNum fn) <$> parseJSON v
            String "alpha":v:String fn:[] -> Order (OrderFieldAlpha fn) <$> parseJSON v
            _ -> mzero
    parseJSON _ = mzero

instance ToJSON Limit where
    toJSON Unlimited = Null
    toJSON (LimitCount c) = object [ "count" .= c ]
    toJSON (LimitOffsetCount o c) = object [ "offset" .= o, "count" .= c ]

instance FromJSON Limit where
    parseJSON Null = return Unlimited
    parseJSON (Object o) = do
        offsetMay <- o .:? "offset"
        countMay <- o .:? "count"
        case (offsetMay, countMay) of
            (Nothing, Nothing) -> return Unlimited
            (Nothing, Just count) -> return (LimitCount count)
            (Just offset, Just count) -> return (LimitOffsetCount offset count)
            (Just offset, Nothing) -> mzero
    parseJSON _ = mzero

instance ToJSON RecordQuery where
    toJSON (RecordQuery m o l) =
        object
            [ "match" .= m
            , "order" .= o
            , "limit" .= l
            ]

instance FromJSON RecordQuery where
    parseJSON (Object o) = do
        match <- fromMaybe def <$> o .:? "match"
        order <- fromMaybe def <$> o .:? "order"
        limit <- fromMaybe def <$> o .:? "limit"
        return $ RecordQuery match order limit
    parseJSON (String str) =
        case parseQuery str of
            Left _ -> mzero
            Right x -> return x
    parseJSON _ = mzero

