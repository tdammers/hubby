{-#LANGUAGE OverloadedStrings #-}
module Hubby.RecordQuery.Print
( printQuery
)
where

import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as LText
import Data.Monoid
import Data.List
import Text.StringConvert

import Hubby.RecordQuery

import Data.Aeson as JSON

printQuery :: RecordQuery -> Text
printQuery q =
    mconcat . intersperse " " . filter (not . LText.null) $
        [ "SELECT"
        , printMatch $ rqMatch q
        , printOrders $ rqOrder q
        , printLimit $ rqLimit q
        ]

printMatch :: Match -> Text
printMatch MatchAll = "ALL"
printMatch m = "WHERE " <> printMatch' m

printMatch' :: Match -> Text
printMatch' MatchAll = "YES"
printMatch' MatchNone = "NO"
printMatch' (MatchID rid) = "`id` = " <> s (show rid)
printMatch' (MatchBinary op a b) =
    mconcat
        [ printOperand a
        , " "
        , s (binaryOperatorToString op)
        , " "
        , printOperand b
        ]
printMatch' (MatchTernary op a b c) =
    let (op1, op2) = ternaryOperatorToStrings op
    in mconcat
        [ printOperand a
        , " "
        , s op1
        , " "
        , printOperand b
        , " "
        , s op2
        , " "
        , printOperand c
        ]
printMatch' (MatchEither a b) =
    mconcat
        [ "("
        , printMatch' a
        , " OR "
        , printMatch' b
        , ")"
        ]
printMatch' (MatchBoth a b) =
    mconcat
        [ "("
        , printMatch' a
        , " AND "
        , printMatch' b
        , ")"
        ]
printMatch' (MatchNot a) =
    mconcat
        [ "NOT "
        , printMatch' a
        ]

printOperand :: Operand -> Text
printOperand (LiteralOperand v) = s $ JSON.encode v
printOperand (FieldOperand f) = "`" <> s f <>"`"
printOperand (ParamOperand p) = "$" <> s p

printOrders :: [Order] -> Text
printOrders [] = ""
printOrders xs = mconcat $ "ORDER BY ":(intersperse ", " . map printOrder $ xs)

printOrder :: Order -> Text
printOrder (Order OrderID ad) = "`id` " <> printAscDesc ad
printOrder (Order (OrderFieldNum fn) ad) = "`" <> s fn <> "` NUMERICALLY " <> printAscDesc ad
printOrder (Order (OrderFieldAlpha fn) ad) = "`" <> s fn <> "` " <> printAscDesc ad

printAscDesc Ascending = "ASC"
printAscDesc Descending = "DESC"

printLimit Unlimited = ""
printLimit (LimitCount c) = "LIMIT " <> (s $ show c)
printLimit (LimitOffsetCount o c) = "LIMIT " <> (s $ show o) <> ", " <> (s $ show c)
