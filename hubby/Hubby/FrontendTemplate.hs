{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE FlexibleInstances #-}
{-#LANGUAGE MultiParamTypeClasses #-}
{-#LANGUAGE TemplateHaskell #-}
{-#LANGUAGE ScopedTypeVariables #-}
module Hubby.FrontendTemplate
( applyFrontendTemplate
, TemplateValue (..)
, FileInfo (..)
, FrontendTemplateContext (..)
)
where

import Text.Ginger
import qualified Text.Ginger.GVal as GVal
import Text.Ginger.Html

import Data.Text as Text
import qualified Data.Text.Lazy as LText
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS

import Data.Maybe
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.Cache (Cache)
import Data.MaybeFail

import Text.StringConvert
import System.LoadFile
import qualified Data.Aeson as JSON
import Data.Aeson.DeriveUtils (deriveLensJSON)

import System.Environment ( getArgs )
import System.IO
import System.IO.Error
import System.FilePath (takeExtension)

import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Maybe
import Control.Monad.Writer
import Control.Monad
import Control.Applicative
import Safe (headDef)
import Data.Monoid
import Data.Default
import Data.List as List

import Hubby.Logging
import Hubby.Record
import Hubby.Record.GValInstance
import Hubby.Thumbnails

data FileInfo =
    FileInfo
        { fileName :: FilePath
        , fileMimeType :: ByteString
        , fileContents :: LBS.ByteString
        }

fileInfoToJSON :: (Maybe Int) -> FileInfo -> JSON.Value
fileInfoToJSON contentLengthMay fi = JSON.Object
                  . HashMap.fromList
                  $ [ ("name", JSON.String . s . fileName $ fi)
                      , ("type", JSON.String . s . fileMimeType $ fi)
                      , ("contents"
                        , JSON.String
                        . s
                        . (fromMaybe id (LBS.take . fromIntegral <$> contentLengthMay))
                        . fileContents
                        $ fi
                        )
                      ]

instance ToGVal m FileInfo where
    toGVal = fiAsGVal

fiAsGVal :: forall m. FileInfo -> GVal m
fiAsGVal fi = def
    { asLookup = Just l
    , asBoolean = True
    , isNull = False
    }
    where
        l :: Text -> Maybe (GVal m)
        l "name" = Just . toGVal . (s :: String -> Text) . fileName $ fi
        l "type" = Just . toGVal . (s :: ByteString -> Text) . fileMimeType $ fi
        l "contents" = Just
                     . toGVal
                     . (s :: LBS.ByteString -> LText.Text)
                     . fileContents
                     $ fi
        l _ = Nothing

data TemplateValue = TemplateRecord Record
                   | TemplateRecords [Record]
                   | TemplateValue JSON.Value
                   | TemplateFile FileInfo

tvAsGVal :: TemplateValue -> GVal (Run (WriterT Text IO))
tvAsGVal (TemplateRecord r) = toGVal r
tvAsGVal (TemplateRecords rs) = toGVal rs
tvAsGVal (TemplateValue v) = toGVal v
tvAsGVal (TemplateFile fi) = toGVal fi

tvAsJSON :: TemplateValue -> JSON.Value
tvAsJSON (TemplateRecord r) = JSON.toJSON r
tvAsJSON (TemplateRecords rs) = JSON.toJSON rs
tvAsJSON (TemplateValue v) = v
tvAsJSON (TemplateFile fi) = fileInfoToJSON Nothing fi

class ToTemplateValue a where
    toTemplateValue :: a -> TemplateValue

data FrontendTemplateContext =
    FrontendTemplateContext
        { ftcThumbCache :: Cache (FilePath, ThumbGeometry) ByteString
        }

applyFrontendTemplate :: FrontendTemplateContext -> Text -> [(Text, TemplateValue)] -> IO (ByteString, LBS.ByteString)
applyFrontendTemplate ftc templateName context =
    tpl templateName context
    where
        tpls :: [((Text -> Bool), Text -> [(Text, TemplateValue)] -> IO (ByteString, LBS.ByteString))]
        tpls =
            [ ((".ginger.html" `Text.isSuffixOf`), applyGingerTemplate)
            , ((== "file"), serveFile ftc)
            ]
        errorTpl :: Text -> [(Text, TemplateValue)] -> IO (ByteString, LBS.ByteString)
        errorTpl templateName _ =
            fail . s $ "Template name does not match any template handler: " <> templateName

        tpl :: Text -> [(Text, TemplateValue)] -> IO (ByteString, LBS.ByteString)
        -- Find first template handler whose predicate matches the template
        -- name, defaulting to the error handler.
        tpl = headDef errorTpl . Prelude.map snd . Prelude.filter (\(p, t) -> p templateName) $ tpls

applyGingerTemplate :: Text -> [(Text, TemplateValue)] -> IO (ByteString, LBS.ByteString)
applyGingerTemplate templateName context =
    applyGingerTemplate' templateName context'
    where
        context' = [(k, tvAsGVal v) | (k, v) <- context]

applyGingerTemplate' :: Text -> [(Text, GVal (Run (WriterT Text IO)))] -> IO (ByteString, LBS.ByteString)
applyGingerTemplate' templateName context = do
    let fn = s $ "templates/" <> templateName
        scope = HashMap.fromList context
        scopeLookup key =
            case key of
                _ -> return . fromMaybe def $ HashMap.lookup key scope
    let loader fn = fmap s <$> (loadFileMay fn :: IO (Maybe LBS.ByteString))
    tpl <- parseGingerFile loader fn
    txt <- case tpl of
        Left err -> do
            writeLog (show err)
            fail "template error"
        Right t -> execWriterT $ runGingerT (makeContextM scopeLookup (tell . htmlSource)) t
    return ("text/html;charset=utf8", s txt)

serveFile :: FrontendTemplateContext -> Text -> [(Text, TemplateValue)] -> IO (ByteString, LBS.ByteString)
serveFile ftc templateName context = do
    let fileTVMay = Prelude.lookup "file" context
        geometryMay = Prelude.lookup "geometry" context
    case fileTVMay of
        Nothing -> fail "'file' not found in context"
        Just (TemplateFile fi) ->
            case geometryMay of
                Nothing -> goServe fi
                Just (TemplateValue (JSON.String geometry)) -> goThumb geometry fi
                Just _ -> fail "wrong type of value in context['geometry']"
        Just _ -> fail "wrong type of value in context['file']"
    where
        goServe :: FileInfo -> IO (ByteString, LBS.ByteString)
        goServe fi = return (fileMimeType fi, fileContents fi)
        goThumb :: Text -> FileInfo -> IO (ByteString, LBS.ByteString)
        goThumb geometryStr fi = do
            let thumbCache :: Cache (FilePath, ThumbGeometry) ByteString
                thumbCache = ftcThumbCache ftc
                loader = return (fileContents fi)
            geometry <- maybeFail
                            ("failed to parse thumbnail geometry from '" ++ s geometryStr ++ "'")
                            (parseGeometry . s $ geometryStr)
            body <- getThumbnail thumbCache loader (s . fileName $ fi) geometry
            return (fileMimeType fi, body)
