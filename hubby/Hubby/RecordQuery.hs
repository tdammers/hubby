{-#LANGUAGE OverloadedStrings #-}
module Hubby.RecordQuery
where

import Data.Default
import Data.Text (Text)
import Text.StringConvert
import Data.Maybe
import Control.Applicative
import Control.Monad
import Data.Aeson

import Hubby.Record

data Operand =
    LiteralOperand Value | FieldOperand FieldName | ParamOperand Text
    deriving (Show, Eq)

data BinaryOperator =
    -- Strict equality
    OpEq |
    OpNEq |
    -- Compare alphabetically
    OpLTStr |
    OpGTStr |
    OpLTEStr |
    OpGTEStr |
    -- Compare numerically
    OpLT |
    OpGT |
    OpLTE |
    OpGTE |
    -- List membership
    OpInList
    deriving (Show, Eq, Enum, Ord, Bounded)

binaryOperatorToString :: BinaryOperator -> String
binaryOperatorToString OpEq = "="
binaryOperatorToString OpNEq = "<>"
binaryOperatorToString OpLTStr = "BEFORE"
binaryOperatorToString OpGTStr = "AFTER"
binaryOperatorToString OpLTEStr = "BEFORE_EQ"
binaryOperatorToString OpGTEStr = "AFTER_EQ"
binaryOperatorToString OpLT = "<"
binaryOperatorToString OpGT = ">"
binaryOperatorToString OpLTE = "<="
binaryOperatorToString OpGTE = ">="
binaryOperatorToString OpInList = "IN"

binaryOperatorFromString :: String -> Maybe BinaryOperator
binaryOperatorFromString "=" = Just OpEq
binaryOperatorFromString "<>" = Just OpNEq
binaryOperatorFromString "BEFORE" = Just OpLTStr
binaryOperatorFromString "AFTER" = Just OpGTStr
binaryOperatorFromString "BEFORE_EQ" = Just OpLTEStr
binaryOperatorFromString "AFTER_EQ" = Just OpGTEStr
binaryOperatorFromString "<" = Just OpLT
binaryOperatorFromString ">" = Just OpGT
binaryOperatorFromString "<=" = Just OpLTE
binaryOperatorFromString ">=" = Just OpGTE
binaryOperatorFromString "IN" = Just OpInList
binaryOperatorFromString _ = Nothing

data TernaryOperator =
    OpBetween
    deriving (Show, Eq, Enum, Ord, Bounded)

ternaryOperatorToStrings :: TernaryOperator -> (String, String)
ternaryOperatorToStrings OpBetween = ("BETWEEN", "AND")

ternaryOperatorFromString :: String -> Maybe TernaryOperator
ternaryOperatorFromString "BETWEEN" = Just OpBetween
ternaryOperatorFromString _ = Nothing

-- | A matching condition (filter), similar to the @WHERE@ clause in SQL.
data Match
    = MatchAll -- | Match all records
    | MatchNone -- | Match no records
    | MatchID RecordID -- | Match by record ID (matches 0 or 1 record; SQL @=@)
    | MatchBinary BinaryOperator Operand Operand
    | MatchTernary TernaryOperator Operand Operand Operand
    | MatchEither Match Match -- | Match one of two sub-filters (SQL @OR@)
    | MatchBoth Match Match -- | Match both of two sub-filters (SQL @AND@)
    | MatchNot Match -- | Negate match (SQL @NOT@)
    deriving (Show, Eq)

instance Default Match where
    def = MatchAll

-- | Ascending or descending ordering.
data OrderAscDesc = Ascending | Descending
    deriving (Show, Eq, Bounded, Enum)

instance Default OrderAscDesc where
    def = Ascending

data OrderTarget =
        OrderID |
        OrderFieldNum FieldName |
        OrderFieldAlpha FieldName
    deriving (Show, Eq)

-- | Ordering (sort). SQL: @ORDER BY@
data Order = Order OrderTarget OrderAscDesc
    deriving (Show, Eq)

-- | Limit the number of results. SQL: @LIMIT@
data Limit
    = Unlimited -- | Unlimited
    | LimitCount Int -- | Get the first @n@ records from the result set
    | LimitOffsetCount Int Int -- | Get @m@ records from the result set, starting at the @n@th record.
    deriving (Show, Eq)

instance Default Limit where
    def = Unlimited

-- | Abstract representation of a backend-agnostic record query.
data RecordQuery
    = RecordQuery
        { rqMatch :: Match -- | Filter
        , rqOrder :: [Order] -- | Ordering specifiers, in descending order of precedence.
        , rqLimit :: Limit -- | Limit number of results
        }
        deriving (Show, Eq)

instance Default RecordQuery where
    -- | The default is "Match all rows, unordered, unlimited"
    def = RecordQuery def [] def

paginate :: Int -> Int -> RecordQuery -> RecordQuery
paginate pageNum pageSize query =
    let newOffset = pageSize * (pageNum - 1)
        newCount = pageSize
        newLimit =
            case rqLimit query of
                Unlimited -> LimitOffsetCount newOffset newCount
                LimitCount c -> LimitOffsetCount newOffset (min (c - newOffset) newCount)
                LimitOffsetCount o c -> LimitOffsetCount (newOffset + o) (min (c - newOffset) newCount)
    in query { rqLimit = newLimit }
