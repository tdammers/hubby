{-#LANGUAGE FlexibleInstances #-}
-- | Various function that are useful in HTTP contexts.
module Hubby.HTTP
where

import Network.Wai
import Network.HTTP.Types
import Data.ByteString (ByteString)
import Data.Text (Text)
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text.Lazy as LText
import Text.StringConvert
import Safe
import Data.Maybe
import Control.Monad (join)

class FromParam a where
    fromParam :: ByteString -> Maybe a

param :: FromParam a => ByteString -> Query -> Maybe a
param = paramBy fromParam

paramBy :: (ByteString -> Maybe a) -> ByteString -> Query -> Maybe a
paramBy f key qs = join (lookup key qs) >>= f

paramDef :: FromParam a => a -> ByteString -> Query -> a
paramDef d key = fromMaybe d . param key

paramByDef :: a -> (ByteString -> Maybe a) -> ByteString -> Query -> a
paramByDef d f key = fromMaybe d . paramBy f key

instance FromParam ByteString where
    fromParam = Just

instance FromParam LBS.ByteString where
    fromParam = Just . s

instance FromParam [Char] where
    fromParam = Just . s

instance FromParam Text where
    fromParam = Just . s

instance FromParam LText.Text where
    fromParam = Just . s

instance FromParam Int where
    fromParam = readMay . s

instance FromParam Integer where
    fromParam = readMay . s
