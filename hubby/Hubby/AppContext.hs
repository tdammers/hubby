{-#LANGUAGE OverloadedStrings #-}
module Hubby.AppContext
( AppContext (..)
)
where

import Data.Default
import qualified Data.ByteString as BS
import Data.ByteString (ByteString)
import Data.Cache (Cache)

import Hubby.AppConfig
import Hubby.Storage
import Hubby.FileStore
import Hubby.Session
import Hubby.SessionProvider
import Hubby.Authentication
import Hubby.Thumbnails (ThumbGeometry)

data AppContext =
    AppContext
        { appConfig :: AppConfig
        , appStorage :: Storage
        , appSessions :: SessionProvider Session
        , appAuth :: AuthProvider
        , appFileStore :: FileStore
        , appThumbCache :: Cache (FilePath, ThumbGeometry) ByteString
        }
