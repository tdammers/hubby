module Hubby.FileStore
( FileStore
, fileExists
, putFile
, getFile
, deleteFile
, listFiles
, createFileStore
, appendFileID
, makeFileID
, isInDir
)
where

import Hubby.FileStore.Type
import Hubby.FileStore.Providers.FS
import Data.Aeson (Value, object, toJSON)
import Data.Default

-- | Factory method to create a FileStore object by name.
createFileStore :: String -> Value -> IO FileStore
createFileStore "fs" conf = createFSStore conf
