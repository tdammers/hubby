module Hubby.CLI
where

import Hubby.AppContext
import Hubby.Authentication
import Text.StringConvert

type CLI = AppContext -> IO ()

createUser :: String -> String -> CLI
createUser username password app = do
    let provider = appAuth app
    result <- authCreateUser provider (s username) (s password)
    case result of
        Left err -> fail err
        Right _ -> return ()
