{-#LANGUAGE OverloadedStrings #-}
module Hubby.FieldType
( FieldType (..)
, FieldTypeRegistry
, fieldToText
, valueToText
)
where

import Data.Default
import Data.Monoid
import Data.Maybe
import Data.List
import Text.StringConvert

import Data.Aeson as JSON

import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vector
import qualified Data.HashMap.Strict as HashMap

import Hubby.Record

-- | Describes how to convert values for a field type to various
-- representations.
-- The @FieldTypeRegistry@ is passed to conversion functions in case we
-- want/need to convert recursively.
data FieldType =
    FieldType
        { ftKey :: Text
        , ftToText :: FieldTypeRegistry -> Value -> Text
        }

instance Default FieldType where
    def =
        FieldType
            { ftKey = ""
            , ftToText = defToText
            }

-- | Dummy implementation; to be extended later.
data FieldTypeRegistry = FieldTypeRegistry

instance Default FieldTypeRegistry where
    def = FieldTypeRegistry

-- | Dummy implementation; to be extended later.
lookupFieldType :: FieldTypeRegistry -> Text -> FieldType
lookupFieldType _ _ = def

-- | Convert a record field to a plain text representation.
fieldToText :: FieldTypeRegistry -> RecordField -> Text
fieldToText ftr f =
    let ftName = fromMaybe "" $ fieldType f
        ft = lookupFieldType ftr ftName
        val = fieldValue f
    in ftToText ft ftr val

-- | Convert a raw JSON value to a plain text representation by first parsing
-- it into a @RecordField@, and then applying @fieldToText@. If the conversion
-- from JSON fails, the value is calculated as if a record field with no name,
-- an implicit type, and the given value itself as its value, were given.
valueToText :: FieldTypeRegistry -> Value -> Text
valueToText ftr val =
    let rec = case fromJSON val of
                Success rec -> rec
                Error _ -> def { fieldValue = val }
    in fieldToText ftr rec

-- | Default implementation for ftToText; converts arbitrary JSON values to a
-- generic representation, treating nested values (array and object elements)
-- as fields, applying @valueToText@ recursively.
defToText :: FieldTypeRegistry -> Value -> Text
defToText ftr (Array items) =
    foldl' (<>) "" .
    intersperse " " .
    fmap (valueToText ftr) .
    Vector.toList $
        items
defToText ftr (Object dict) =
    mconcat . intersperse ", " $
        [ k <> ": " <> valueToText ftr v
        | (k,v)
        <- HashMap.toList dict
        ]
defToText _ v = s . toString $ v
