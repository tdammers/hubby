{-#LANGUAGE OverloadedStrings #-}
module Hubby.Templates.Master
where

import Prelude hiding (head)
import Data.Text (Text)
import Text.Tamper
import Text.Tamper.DOM as DOM
import Text.Tamper.Tags.Html as H
import Text.Tamper.Tags.Html.Attributes as A
import Hubby.Templates.Type
import Data.String

masterT :: Text -> Template -> Template
masterT heading inner = nodeWithContent DOM.documentNode $ do
    html $ do
        head $ do
            H.title $ do
                text heading
        body $ do
            h1 $ do
                text heading
            inner
