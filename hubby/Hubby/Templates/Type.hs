{-#LANGUAGE TypeSynonymInstances #-}
{-#LANGUAGE FlexibleInstances #-}
module Hubby.Templates.Type
( runTemplate
, runTemplateWithSession
, runTemplate'
, TemplateM
, Template
, getSession
)
where

import Text.Tamper
import Text.Tamper.Render
import Text.Tamper.Render.Html
import Data.Text (Text)
import Control.Monad.Identity
import Control.Monad.Reader
import Control.Applicative
import Hubby.AppConfig
import Hubby.Session

data TemplateContext =
    TemplateContext
        { tcConfig :: AppConfig
        , tcSession :: Maybe Session
        }
type TemplateM = TamperT Text (Reader TemplateContext)
type Template = TemplateM ()

instance MonadConfig TemplateM where
    getConfig = asks tcConfig

getSession :: TemplateM (Maybe Session)
getSession = asks tcSession

runTemplate :: AppConfig -> Template -> Text
runTemplate = runTemplate' Nothing

runTemplateWithSession :: Session -> AppConfig -> Template -> Text
runTemplateWithSession session = runTemplate' (Just session)

runTemplate' :: Maybe Session -> AppConfig -> Template -> Text
runTemplate' sessionMay config dom =
    runReader
        (renderTamperT htmlReducer dom)
        (TemplateContext { tcConfig = config, tcSession = sessionMay })
