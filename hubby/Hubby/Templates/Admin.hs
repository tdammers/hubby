{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE FlexibleContexts #-}
module Hubby.Templates.Admin
where

import Prelude hiding (head, div)
import Data.Text (Text)
import Text.StringConvert (StringConvert, s)
import Data.Monoid
import Control.Monad
import Data.List
import Control.Applicative
import qualified Data.ByteString.Lazy as LBS
import Data.Default
import Data.String
import Data.Maybe (fromMaybe)

import Text.Tamper
import Text.Tamper.DOM as DOM
import Text.Tamper.Tags.Html as H hiding (s)
import Text.Tamper.Tags.Html.Attributes as A

import Hubby.AppConfig
import Hubby.Session
import Hubby.Record
import Hubby.RecordQuery
import Hubby.RecordQuery.JSON
import Hubby.RecordQuery.Print
import Hubby.Templates.Type

import Data.Aeson as JSON
import Data.Yaml as YAML
import Network.HTTP.Types
import qualified Text.Parsec as Parsec

adminMasterT' :: Bool -> Text -> Text -> Template -> Template
adminMasterT' withMenu bodyClass heading inner = nodeWithContent DOM.documentNode $ do
    html $ do
        H.head $ do
            H.title $ do
                text heading
            url <- mkAdminURL ["static", "css", "admin.css" ]
            link
                ! rel "stylesheet"
                ! A.type_ "text/css"
                ! href url
            dataMainUrl <- mkAdminURL ["static", "js", "main.js" ]
            requireJsUrl <- mkAdminURL ["static", "js", "lib", "require.js"]
            H.script
                ! A.type_ "text/javascript"
                ! A.src requireJsUrl
                ! A.dataAttrib "main" dataMainUrl
                $ return ()
        body
            ! class_ bodyClass
            $ do
                when withMenu $ do
                    div
                        ! class_ "sidebar"
                        $ sidebarMenuT
                div
                    ! class_ "main"
                    $ do
                        h1 $ text heading
                        inner

adminMasterT :: Text -> Template -> Template
adminMasterT = adminMasterT' True ""

adminLoginT :: Maybe Text -> [Text] -> Template
adminLoginT usernameMay errorMessages = adminMasterT' False "login-body" "Login" $ do
    loginUrl <- mkAdminURL [ "login" ]
    H.form
        ! method "POST"
        ! action loginUrl
        ! class_ "login-form"
        $ do
            when (not . null $ errorMessages) $
                div ! class_ "errors box" $
                    forM_ errorMessages $ \error ->
                        p $ text error
            div ! class_ "form-row" $ do
                H.label
                    ! for "username"
                    $ "username"
                input
                    ! type_ "text"
                    ! name "username"
                    ! value (fromMaybe "" usernameMay)
            div ! class_ "form-row" $ do
                H.label
                    ! for "password"
                    $ "password"
                input
                    ! type_ "password"
                    ! name "password"
            div ! class_ "form-row" $ do
                input
                    ! type_ "submit"
                    ! value "login"

sidebarMenuT :: Template
sidebarMenuT = do
    blueprints <- getsConfig blueprints
    menuT
        Nothing
        [ ("Visit front page", return "/")
        , ("Dashboard", mkAdminURL [])
        , ("Log out", mkAdminURL [ "logout" ])
        ]
    menuT
        (Just "Nodes")
        [ ("All nodes", mkAdminURL ["nodes"])
        , ("Search", mkAdminURL ["nodes", "search"])
        ]
    menuT
        (Just "New Node")
        (
            [ ("Blank", mkAdminURL ["node", "new"])
            ] ++
            [ (text (blueprintName bp), mkAdminURL [ "node", "new", blueprintSlug bp ])
            | bp <- blueprints
            ]
        )

menuT :: Maybe Text -> [(Template, TemplateM Text)] -> Template
menuT title items =
    menu
        ! class_ "sidebar-menu"
        $ do
            maybe (return ()) (li . H.h1 . text) title
            sequence_
                [ li $ do
                    url <- urlT
                    a ! href url $ title
                | (title, urlT)
                <- items
                ]

adminLogoutT :: Template
adminLogoutT = adminMasterT "Logout" $ do
    logoutUrl <- mkAdminURL [ "logout" ]
    H.form
        ! A.action logoutUrl 
        ! A.method "POST"
        $ do
            formTokenT
            H.input
                ! A.type_ "submit"
                ! A.value "Log out"

formTokenT :: Template
formTokenT = do
    sessionMay <- getSession
    case sessionMay of
        Nothing -> return ()
        Just session ->
            H.input
                ! A.type_ "hidden"
                ! A.name "_form_token"
                ! A.value (sessionCSRFToken session)

adminDashboardT :: Template
adminDashboardT = adminMasterT "Admin Dashboard" $ do
    sessionMay <- getSession
    H.section $ do
        H.h1 "Session"
        case sessionMay of
            Nothing -> H.p ! class_ "disabled" $ "Not logged in"
            Just session -> H.p . text . s . show $ session

nodeFormT :: Text -> Record -> Template
nodeFormT formAction rec =
    adminMasterT "Edit node" $ do
        H.div
            ! class_ "record-editor"
            $ do
                H.form
                    ! method "POST"
                    ! action formAction
                    $ do
                        formTokenT
                        textarea
                            ! class_ "record-json hidden"
                            ! name "node"
                            $ text (s (JSON.encode $ recordFields rec) :: Text)
                        input
                            ! type_ "submit"
                            ! value "Save"
                H.form
                    ! method "POST"
                    ! action (formAction <> "?_method=DELETE")
                    ! class_ "confirm-submit"
                    $ do
                        formTokenT
                        input
                            ! type_ "submit"
                            ! value "Delete"

querySyntaxErrorT :: Parsec.ParseError -> Text -> Template
querySyntaxErrorT err qString =
    adminMasterT "Syntax error in query" $ do
        h2
            ! class_ "node-query-string"
            $ text (s qString)
        pre
            ! class_ "error"
            $ text (s . show $ err)
        queryEditFormT qString

queryJSONErrorT :: String -> LBS.ByteString -> Template
queryJSONErrorT err qJSON =
    adminMasterT "JSON error in query" $ do
        h2
            ! class_ "node-query-json"
            $ text (s qJSON)
        pre
            ! class_ "error"
            $ text (s err)

queryEditFormT :: Text -> Template
queryEditFormT qString = do
    ac <- mkAdminURL [ "nodes", "search" ]
    H.form
        ! A.action ac
        ! A.method "GET"
        $ do
            formTokenT
            input
                ! class_ "node-query-string"
                ! name "q"
                ! type_ "text"
                ! value (s qString)
            input ! type_ "submit" ! value "Go"

queryJSONFormT :: RecordQuery -> Template
queryJSONFormT q = do
    ac <- mkAdminURL [ "nodes", "search" ]
    H.form
        ! A.action ac
        ! A.method "GET"
        $ do
            formTokenT
            textarea
                ! class_ "node-query-json"
                ! name "qj"
                $ text (s . JSON.encode $ q)
            input ! type_ "submit" ! value "Go"

queryEditorFormT :: RecordQuery -> Template
queryEditorFormT q = do
    ac <- mkAdminURL [ "nodes", "search" ]
    H.form
        ! class_ "node-query-editor"
        ! A.action ac
        ! A.method "GET"
        $ do
            formTokenT
            textarea
                ! class_ "node-query-json"
                ! name "qj"
                $ text (s . JSON.encode $ q)
            input ! type_ "submit" ! value "Go"


data RecordQueryOriginFormat = RecordQueryFromJSON | RecordQueryFromString
    deriving (Eq, Enum, Ord)

instance Show RecordQueryOriginFormat where
    show RecordQueryFromJSON = "json"
    show RecordQueryFromString = "string"

instance Default RecordQueryOriginFormat where
    def = RecordQueryFromString

tabsT :: Text -> [(Text, Text, Template)] -> Template
tabsT selectedKey items = do
    div
        ! class_ "tab-buttons"
        $ forM_ items $ \(key, title, _) ->
            a ! href "#"
              ! class_ (if key == selectedKey then "tab-selector tab-selected" else "tab-selector")
              ! dataAttrib "tab-panel" ("tab_panel_" <> key)
              $ text title
    div
        ! class_ "tab-container"
        $ forM_ items $ \(key, _, contents) ->
            div
                ! A.id ("tab_panel_" <> key)
                ! class_ (if key == selectedKey then "tab-panel tab-selected" else "tab-panel")
                $ contents

nodeListT :: Text -> RecordQueryOriginFormat -> RecordQuery -> [Record] -> Int -> Int -> Template
nodeListT queryName qFormat q nodes page numPages =
    adminMasterT (s queryName) $ do
        tabsT (s . show $ qFormat)
            [ (s . show $ RecordQueryFromJSON
              , "Search"
              , queryEditorFormT q
              )
            , (s . show $ RecordQueryFromString
              , "Query"
              , queryEditFormT (s . printQuery $ q))
            , (s . show $ RecordQueryFromJSON
              , "JSON"
              , queryJSONFormT q
              )
            ]
        if null nodes
            then
                ul
                    ! class_ "node-list empty"
                    $ text "Nothing found"
            else do
                pager
                ul
                    ! class_ "node-list"
                    $ do
                        sequence_ (Prelude.map nodeListItemT nodes)
                pager
        ul
            ! class_ "node-list" $
                li $ do
                    url <- mkAdminURL [ "node", "new" ]
                    a
                        ! href url
                        $ h1 "Add a node..."
    where
        pager :: Template
        pager = when (numPages > 1) $ do
            div
                ! class_ "pager"
                $ do
                    let itemsRaw = [ 1 ] ++ [ page - 2 .. page + 2 ] ++ [ numPages ]
                        items = nub . sort . filter (> 0) . filter (<= numPages) $ itemsRaw
                    forM_ (zip (0:items) items) $ \(prev, p) -> do
                        when (succ prev /= p) $
                            H.span
                                ! class_ "pager-item"
                                $ "…" 
                        a
                            ! (class_ $ "pager-item" <> (if p == page then " current" else ""))
                            ! (href . s $ "?page=" <> show p)
                            $ text (s $ show p)

        nodeListItemT :: Record -> Template
        nodeListItemT rec = do
            li $ do
                h1 $ do
                    url <- mkAdminURL ["node", (recordID rec)]
                    a
                        ! href url
                        $ do
                            maybe
                                (H.em ! class_ "unnamed" $ "(unnamed)")
                                (\title -> text (s title)) $ recordTitle rec
                            text " "
                            small $ do
                                text "(#"
                                text (recordID rec)
                                text ")"
                p $ text (s . recordTeaser 100 $ rec)
