module Hubby.Tests
where

import qualified Hubby.Tests.QuickCheck.Model.RecordQuery as RecordQuery
import Hubby.Tests.QuickCheck.Common
import Test.QuickCheck
import Control.Monad

allTests =
    RecordQuery.tests

runAllTests = runTests allTests

runTests = mapM_ go
    where
        go (Test name test) = do
            putStrLn $ name ++ ":"
            quickCheck test
